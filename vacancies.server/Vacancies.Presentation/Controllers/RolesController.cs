﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Roles;
using Vacancies.Application.Services.Roles.DTOs;
using Vacancies.Presentation.Filters;

namespace Vacancies.Presentation.Controllers
{
    public class RolesController : BaseController
    {
        private readonly IRoleService _roleService;

        public RolesController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        /// <summary>
        /// Default: {
        ///     Name: null,
        ///     Description: null,
        ///     CurrentPage: 1,
        ///     PerPage: 10,
        ///     SortBy: "ID",
        ///     SortDirection: "ASC"
        /// }
        /// </summary>
        /// <param name="roleSearchDTO"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult FindList([FromQuery] RoleSearchDTO roleSearchDTO)
        {
            (IEnumerable<RoleDTO>, int) findList = _roleService.FindList(roleSearchDTO);
            return Ok(new { Data = findList.Item1, Count = findList.Item2 });
        }

        [HttpGet]
        public IActionResult GetList()
        {
            IEnumerable<SelectItemDTO> selectItemDTOs = _roleService.GetList(); 
            return Ok(selectItemDTOs);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            RoleUpdateDTO roleUpdateDTO = _roleService.GetById(id);
            return Ok(roleUpdateDTO);
        }

        [HttpPost]
        [ServiceFilter(typeof(InsertFilter))]
        public IActionResult Insert([FromBody] RoleInsertDTO roleInsertDTO)
        {
            bool result = _roleService.Insert(roleInsertDTO);
            return Ok(result);
        }

        [HttpPut]
        [ServiceFilter(typeof(UpdateFilter))]
        public IActionResult Update([FromBody] RoleUpdateDTO roleUpdateDTO)
        {
            bool result = _roleService.Update(roleUpdateDTO);
            return Ok(result);
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            bool result = _roleService.Delete(id);
            return Ok(result);
        }
    }
}