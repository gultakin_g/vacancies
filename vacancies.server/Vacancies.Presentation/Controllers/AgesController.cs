﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Ages;
using Vacancies.Application.Services.Ages.DTOs;
using Vacancies.Presentation.Filters;

namespace Vacancies.Presentation.Controllers
{
    public class AgesController :BaseController
    {
        private readonly IAgeService _ageService;

        public AgesController(IAgeService ageService)
        {
            _ageService = ageService;
        }

        /// <summary>
        /// Default: {
        ///     Name: null,
        ///     Description: null,
        ///     CurrentPage: 1,
        ///     PerPage: 10,
        ///     SortBy: "ID",
        ///     SortDirection: "ASC"
        /// }
        /// </summary>
        /// <param name="ageSearchDTO"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult FindList([FromQuery] AgeSearchDTO ageSearchDTO)
        {
            (IEnumerable<AgeDTO> ages, int count) = _ageService.FindList(ageSearchDTO);
            return Ok(new { Ages = ages, Count = count });
        }

        [HttpGet]
        public IActionResult GetList()
        {
            IEnumerable<SelectItemDTO> selectItemDTOs = _ageService.GetList();
            return Ok(selectItemDTOs);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            AgeUpdateDTO ageUpdateDTO = _ageService.GetById(id);
            return Ok(ageUpdateDTO);
        }

        [HttpPost]
        [ServiceFilter(typeof(InsertFilter))]
        public IActionResult Insert([FromBody] AgeInsertDTO ageInsertDTO)
        {
            bool result = _ageService.Insert(ageInsertDTO);
            return Ok(result);
        }

        [HttpPut]
        [ServiceFilter(typeof(UpdateFilter))]
        public IActionResult Update([FromBody] AgeUpdateDTO ageUpdateDTO)
        {
            bool result = _ageService.Update(ageUpdateDTO);
            return Ok(result);
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            bool result = _ageService.Delete(id);
            return Ok(result);
        }
    }
}
