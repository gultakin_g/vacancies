﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Experiences;
using Vacancies.Application.Services.Experiences.DTOs;
using Vacancies.Presentation.Filters;

namespace Vacancies.Presentation.Controllers
{
    public class ExperiencesController : BaseController
    {
        private readonly IExperienceService _experienceService;

        public ExperiencesController(IExperienceService experienceService)
        {
            _experienceService = experienceService;
        }

        /// <summary>
        /// Default: {
        ///     Name: null,
        ///     Description: null,
        ///     CurrentPage: 1,
        ///     PerPage: 10,
        ///     SortBy: "ID",
        ///     SortDirection: "ASC"
        /// }
        /// </summary>
        /// <param name="experienceSearchDTO"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult FindList([FromQuery]ExperienceSearchDTO experienceSearchDTO)
        {
            (IEnumerable<ExperienceDTO> experiences, int count) = _experienceService.FindList(experienceSearchDTO);
            return Ok(new { Experiences = experiences, Count = count });
        }

        [HttpGet]
        public IActionResult GetList()
        {
            IEnumerable<SelectItemDTO> selectItemDTOs = _experienceService.GetList();
            return Ok(selectItemDTOs);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            ExperienceUpdateDTO experienceUpdateDTO = _experienceService.GetById(id);
            return Ok(experienceUpdateDTO);
        }

        [HttpPost]
        [ServiceFilter(typeof(InsertFilter))]
        public IActionResult Insert(ExperienceInsertDTO experienceInsertDTO)
        {
            bool result = _experienceService.Insert(experienceInsertDTO);
            return Ok(result);
        }

        [HttpPut]
        [ServiceFilter(typeof(UpdateFilter))]
        public IActionResult Update(ExperienceUpdateDTO experienceUpdateDTO)
        {
            bool result = _experienceService.Update(experienceUpdateDTO);
            return Ok(result);
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            bool result = _experienceService.Delete(id);
            return Ok(result);
        }
    }
}
