﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Educations;
using Vacancies.Application.Services.Educations.DTOs;
using Vacancies.Presentation.Filters;

namespace Vacancies.Presentation.Controllers
{
    public class EducationsController : BaseController
    {
        private readonly IEducationService _educationService;

        public EducationsController(IEducationService educationService)
        {
            _educationService = educationService;
        }

        /// <summary>
        /// Default: {
        ///     Name: null,
        ///     Description: null,
        ///     CurrentPage: 1,
        ///     PerPage: 10,
        ///     SortBy: "ID",
        ///     SortDirection: "ASC"
        /// }
        /// </summary>
        /// <param name="educationSearchDTO"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult FindList([FromQuery] EducationSearchDTO educationSearchDTO)
        {
            (IEnumerable<EducationDTO> educations, int count) = _educationService.FindList(educationSearchDTO);
            return Ok(new { Educations = educations, Count = count });
        }

        [HttpGet]
        public IActionResult GetList()
        {
            IEnumerable<SelectItemDTO> selectItemDTOs = _educationService.GetList();
            return Ok(selectItemDTOs);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            EducationUpdateDTO educationUpdateDTO = _educationService.GetById(id);
            if(educationUpdateDTO == null)
            {
                return NotFound("Education not found!");
            }
            return Ok(educationUpdateDTO);
        }

        [HttpPost]
        [ServiceFilter(typeof(InsertFilter))]
        public IActionResult Insert([FromBody]EducationInsertDTO educationInsertDTO)
        {
            bool result = _educationService.Insert(educationInsertDTO);
            return Ok(result);
        }

        [HttpPut]
        [ServiceFilter(typeof(UpdateFilter))]
        public IActionResult Update(EducationUpdateDTO educationUpdateDTO)
        {
            bool result = _educationService.Update(educationUpdateDTO);
            return Ok(result);
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            bool result = _educationService.Delete(id);
            return Ok(result);
        }
    }
}
