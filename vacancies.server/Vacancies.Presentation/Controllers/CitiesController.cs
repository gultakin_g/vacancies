﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Cities;
using Vacancies.Application.Services.Cities.DTOs;
using Vacancies.Presentation.Filters;

namespace Vacancies.Presentation.Controllers
{
    public class CitiesController : BaseController
    {
        private readonly ICityService _cityService;

        public CitiesController(ICityService cityService)
        {
            _cityService = cityService;
        }

        /// <summary>
        /// Default: {
        ///     Name: null,
        ///     Description: null,
        ///     CurrentPage: 1,
        ///     PerPage: 10,
        ///     SortBy: "ID",
        ///     SortDirection: "ASC"
        /// }
        /// </summary>
        /// <param name="citySearchDTO"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult FindList([FromQuery] CitySearchDTO citySearchDTO)
        {
            (IEnumerable<CityDTO> cities, int count) = _cityService.FindList(citySearchDTO);
            return Ok(new { Cities = cities, Count = count });
        }

        [HttpGet]
        public IActionResult GetList()
        {
            IEnumerable<SelectItemDTO> selectItemDTOs = _cityService.GetList();
            return Ok(selectItemDTOs);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            CityUpdateDTO cityUpdateDTO = _cityService.GetById(id);
            return Ok(cityUpdateDTO);
        }

        [HttpPost]
        [ServiceFilter(typeof(InsertFilter))]
        public IActionResult Insert([FromBody] CityInsertDTO cityInsertDTO)
        {
            bool result = _cityService.Insert(cityInsertDTO);
            return Ok(result);
        }

        [HttpPut]
        [ServiceFilter(typeof(UpdateFilter))]
        public IActionResult Update([FromBody] CityUpdateDTO cityUpdateDTO)
        {
            bool result = _cityService.Update(cityUpdateDTO);
            return Ok(result);
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            bool result = _cityService.Delete(id);
            return Ok(result);
        }
    }
}