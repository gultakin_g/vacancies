﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Salaries;
using Vacancies.Application.Services.Salaries.DTOs;
using Vacancies.Presentation.Filters;

namespace Vacancies.Presentation.Controllers
{
    public class SalariesController :BaseController
    {
        private readonly ISalaryService _salaryService;

        public SalariesController(ISalaryService salaryService)
        {
            _salaryService = salaryService;
        }

        /// <summary>
        /// Default: {
        ///     Name: null,
        ///     Description: null,
        ///     CurrentPage: 1,
        ///     PerPage: 10,
        ///     SortBy: "ID",
        ///     SortDirection: "ASC"
        /// }
        /// </summary>
        /// <param name="salarySearchDTO"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult FindList([FromQuery] SalarySearchDTO salarySearchDTO)
        {
            (IEnumerable<SalaryDTO> salaryDTOs, int count) = _salaryService.FindList(salarySearchDTO);
            return Ok(new {Salaries = salaryDTOs, Count = count });
        }

        [HttpGet]
        public IActionResult GetList()
        {
            IEnumerable<SelectItemDTO> selectItemDTOs = _salaryService.GetList();
            return Ok(selectItemDTOs);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            SalaryUpdateDTO salaryUpdateDTO = _salaryService.GetById(id);
            return Ok(salaryUpdateDTO);
        }

        [HttpPost]
        [ServiceFilter(typeof(InsertFilter))]
        public IActionResult Insert([FromBody] SalaryInsertDTO salaryInsertDTO)
        {
            bool result = _salaryService.Insert(salaryInsertDTO);
            return Ok(result);
        }

        [HttpPut]
        [ServiceFilter(typeof(UpdateFilter))]
        public IActionResult Update([FromBody] SalaryUpdateDTO salaryUpdateDTO)
        {
            bool result = _salaryService.Update(salaryUpdateDTO);
            return Ok(result);
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            bool result = _salaryService.Delete(id);
            return Ok(result);
        }
    }
}
