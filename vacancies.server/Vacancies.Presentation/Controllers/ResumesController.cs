﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Vacancies.Application.Services.Resumes;
using Vacancies.Application.Services.Resumes.DTOs;
using Vacancies.Domain.Models.Bases;
using Vacancies.Presentation.Filters;

namespace Vacancies.Presentation.Controllers
{
    public class ResumesController : BaseController
    {
        private readonly IResumeService _resumeService;

        public ResumesController(IResumeService resumeService)
        {
            _resumeService = resumeService;
        }

        /// <summary>
        /// Default: {
        ///     Name: null,
        ///     Description: null,
        ///     CurrentPage: 1,
        ///     PerPage: 10,
        ///     SortBy: "ID",
        ///     SortDirection: "ASC"
        /// }
        /// </summary>
        /// <param name="resumeSearchDTO"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult FindList([FromQuery] ResumeSearchDTO resumeSearchDTO)
        {
            (IEnumerable<ResumeDTO> resumeDTOs, int count) = _resumeService.FindList(resumeSearchDTO);
            return Ok(new { Resumes = resumeDTOs, Count = count });
        }

        [HttpGet]
        public IActionResult GetList()
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            ResumeUpdateDTO resumeUpdateDTO = _resumeService.GetById(id);
            return Ok(resumeUpdateDTO);
        }

        [HttpPost]
        [ServiceFilter(typeof(InsertFilter))]
        public IActionResult Insert([FromBody] ResumeInsertDTO resumeInsertDTO)
        {
            bool result = _resumeService.Insert(resumeInsertDTO);
            return Ok(result);
        }

        [HttpPut]
        [ServiceFilter(typeof(UpdateFilter))]
        public IActionResult Update([FromBody] ResumeUpdateDTO resumeUpdateDTO)
        {
            bool result = _resumeService.Update(resumeUpdateDTO);
            return Ok(result);
        }

        [HttpDelete]
        public IActionResult Delete(DeleteEntity entity)
        {
            bool result = _resumeService.Delete(entity);
            return Ok(result);
        }
    }
}
