﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Vacancies.Application.Services.Vacancies;
using Vacancies.Application.Services.Vacancies.DTOs;
using Vacancies.Domain.Models.Bases;
using Vacancies.Presentation.Filters;

namespace Vacancies.Presentation.Controllers
{
    public class VacanciesController : BaseController
    {
        private readonly IVacanciesService _vacanciesService;

        public VacanciesController(IVacanciesService vacanciesService)
        {
            _vacanciesService = vacanciesService;
        }

        /// <summary>
        /// Default: {
        ///     Name: null,
        ///     Description: null,
        ///     CurrentPage: 1,
        ///     PerPage: 10,
        ///     SortBy: "ID",
        ///     SortDirection: "ASC"
        /// }
        /// </summary>
        /// <param name="vacancySearchDTO"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult FindList([FromQuery] VacancySearchDTO vacancySearchDTO)
        {
            (IEnumerable<VacancyDTO> vacancyDTOs, int count) = _vacanciesService.FindList(vacancySearchDTO);
            return Ok(new { Vacancies = vacancyDTOs, Count = count });
        }

        [HttpGet]
        public IActionResult GetList()
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            VacancyUpdateDTO vacancyUpdateDTO = _vacanciesService.GetById(id);
            return Ok(vacancyUpdateDTO);
        }

        [HttpPost]
        [ServiceFilter(typeof(InsertFilter))]
        public IActionResult Insert([FromBody] VacancyInsertDTO vacancyInsertDTO)
        {
            bool result = _vacanciesService.Insert(vacancyInsertDTO);
            return Ok(result);
        }

        [HttpPut]
        [ServiceFilter(typeof(UpdateFilter))]
        public IActionResult Update([FromBody] VacancyUpdateDTO vacancyUpdateDTO)
        {
            bool result = _vacanciesService.Update(vacancyUpdateDTO);
            return Ok(result);
        }

        [HttpDelete]
        public IActionResult Delete(DeleteEntity entity)
        {
            bool result = _vacanciesService.Delete(entity);
            return Ok(result);
        }
    }
}
