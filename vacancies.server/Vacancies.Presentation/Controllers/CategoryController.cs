﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Categories;
using Vacancies.Application.Services.Categories.DTOs;
using Vacancies.Presentation.Filters;

namespace Vacancies.Presentation.Controllers
{
    public class CategoryController : BaseController
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        /// <summary>
        /// Default: {
        ///     Name: null,
        ///     Description: null,
        ///     CurrentPage: 1,
        ///     PerPage: 10,
        ///     SortBy: "ID",
        ///     SortDirection: "ASC"
        /// }
        /// </summary>
        /// <param name="categorySearchDTO"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult FindList([FromQuery] CategorySearchDTO categorySearchDTO)
        {
            (IEnumerable<CategoryDTO> categories, int count) = _categoryService.FindList(categorySearchDTO);
            return Ok(new { Categories = categories, Count = count });
        }

        [HttpGet]
        public IActionResult GetList()
        {
            IEnumerable<SelectItemDTO> selectItemDTOs = _categoryService.GetList();
            return Ok(selectItemDTOs);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            CategoryUpdateDTO categoryUpdateDTO = _categoryService.GetById(id);
            return Ok(categoryUpdateDTO);
        }

        [HttpPost]
        [ServiceFilter(typeof(InsertFilter))]
        public IActionResult Insert([FromBody] CategoryInsertDTO categoryInsertDTO)
        {
            bool result = _categoryService.Insert(categoryInsertDTO);
            return Ok(result);
        }

        [HttpPut]
        [ServiceFilter(typeof(UpdateFilter))]
        public IActionResult Update([FromBody] CategoryUpdateDTO categoryUpdateDTO)
        {
            bool result = _categoryService.Update(categoryUpdateDTO);
            return Ok(result);
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            bool result = _categoryService.Delete(id);
            return Ok(result);
        }
    }
}
