﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Users;
using Vacancies.Application.Services.Users.DTOs;
using Vacancies.Presentation.Filters;

namespace Vacancies.Presentation.Controllers
{
    public class UsersController :  BaseController
    {
        private readonly IUserService _userService;
        private readonly IConfiguration _configuration;

        public UsersController(IUserService userService,
            IConfiguration configuration)
        {
            _userService = userService;
            _configuration = configuration;
        }

        #region Auth
        [HttpPost]
        [AllowAnonymous]
        public IActionResult Authenticate([FromBody] UserAuthDTO authDTO)
        {
            (UserUpdateDTO authenticatedUser, string errorMessage) = _userService.Authenticate(authDTO);
            if(errorMessage != null)
            {
                return NotFound(errorMessage);
            }
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            byte[] keyword = Encoding.ASCII.GetBytes(_configuration.GetValue<string>("JwtSettings:Keyword"));
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Sid, authenticatedUser.Id.ToString()),
                    new Claim(ClaimTypes.NameIdentifier, authenticatedUser.UserName.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(keyword), SecurityAlgorithms.HmacSha256Signature)
            };
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            string userToken = tokenHandler.WriteToken(token);

            return Ok(new { Token = userToken, User = authenticatedUser});
        }
        #endregion

        #region Users

        /// <summary>
        /// Default: {
        ///     Name: null,
        ///     Description: null,
        ///     CurrentPage: 1,
        ///     PerPage: 10,
        ///     SortBy: "ID",
        ///     SortDirection: "ASC"
        /// }
        /// </summary>
        /// <param name="userSearchDTO"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult FindList([FromQuery] UserSearchDTO userSearchDTO)
        {
            (IEnumerable<UserDTO> userDTOs, int count) = _userService.FindList(userSearchDTO);
            return Ok(new { Users = userDTOs, Count = count });
        }

        [HttpGet]
        public IActionResult GetList()
        {
            IEnumerable<SelectItemDTO> selectItemDTOs = _userService.GetList();
            return Ok(selectItemDTOs);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            UserUpdateDTO userUpdateDTO = _userService.GetById(id);
            return Ok(userUpdateDTO);
        }

        [HttpPost]
        [ServiceFilter(typeof(InsertFilter))]
        public IActionResult Insert(UserInsertDTO userInsertDTO)
        {
            bool result = _userService.Insert(userInsertDTO);
            return Ok(result);
        }

        [HttpPut]
        [ServiceFilter(typeof(UpdateFilter))]
        public IActionResult Update([FromBody] UserUpdateDTO userUpdateDTO)
        {
            bool result = _userService.Update(userUpdateDTO);
            return Ok(result);
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            bool result = _userService.Delete(id);
            return Ok(result);
        }

        #endregion
    }
}
