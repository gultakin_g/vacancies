﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Permissions;
using Vacancies.Application.Services.Permissions.DTOs;
using Vacancies.Presentation.Filters;

namespace Vacancies.Presentation.Controllers
{
    public class PermissionsController :BaseController
    {
        private readonly IPermissionService _permissionService;

        public PermissionsController(IPermissionService permissionService)
        {
            _permissionService = permissionService;
        }

        /// <summary>
        /// Default: {
        ///     Name: null,
        ///     Description: null,
        ///     CurrentPage: 1,
        ///     PerPage: 10,
        ///     SortBy: "ID",
        ///     SortDirection: "ASC"
        /// }
        /// </summary>
        /// <param name="permissionSearchDTO"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult FindList([FromQuery] PermissionSearchDTO permissionSearchDTO)
        {
            (IEnumerable<PermissionDTO> permissions, int count) = _permissionService.FindList(permissionSearchDTO);
            return Ok(new { Permissions = permissions, Count = count });
        }

        [HttpGet]
        public IActionResult GetList()
        {
            IEnumerable<SelectItemDTO> selectItemDTOs = _permissionService.GetList();
            return Ok(selectItemDTOs);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            PermissionUpdateDTO permissionUpdateDTO = _permissionService.GetById(id);
            return Ok(permissionUpdateDTO);
        }

        [HttpPost]
        [ServiceFilter(typeof(InsertFilter))]
        public IActionResult Insert([FromBody] PermissionInsertDTO permissionInsertDTO)
        {
            bool result = _permissionService.Insert(permissionInsertDTO);
            return Ok(result);
        }

        [HttpPut]
        [ServiceFilter(typeof(UpdateFilter))]
        public IActionResult Update([FromBody] PermissionUpdateDTO permissionUpdateDTO)
        {
            bool result = _permissionService.Update(permissionUpdateDTO);
            return Ok(result);
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            bool result = _permissionService.Delete(id);
            return Ok(result);
        }
    }
}
