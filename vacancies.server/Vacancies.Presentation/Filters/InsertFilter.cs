﻿using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using Vacancies.Application.BaseDTOs;

namespace Vacancies.Presentation.Filters
{
    public class InsertFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
            
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ActionArguments.FirstOrDefault().Value is IInsertDTO)
            {
                (context.ActionArguments.FirstOrDefault().Value as IInsertDTO).CreatedBy = 1;
            }
        }
    }
}
