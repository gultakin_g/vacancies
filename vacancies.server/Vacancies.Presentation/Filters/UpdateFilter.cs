﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using Vacancies.Application.BaseDTOs;

namespace Vacancies.Presentation.Filters
{
    public class UpdateFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
            
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if(context.ActionArguments.FirstOrDefault().Value is IUpdateDTO)
            {
                (context.ActionArguments.FirstOrDefault().Value as IUpdateDTO).UpdatedBy = 1;
                (context.ActionArguments.FirstOrDefault().Value as IUpdateDTO).UpdatedDate = DateTime.Now;
            }
        }
    }
}
