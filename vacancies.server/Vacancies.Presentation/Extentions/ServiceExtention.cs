﻿using AutoMapper;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Vacancies.Application;
using Vacancies.Application.Services.Ages;
using Vacancies.Application.Services.Ages.DTOs;
using Vacancies.Application.Services.Categories;
using Vacancies.Application.Services.Categories.DTOs;
using Vacancies.Application.Services.Cities;
using Vacancies.Application.Services.Cities.DTOs;
using Vacancies.Application.Services.Educations;
using Vacancies.Application.Services.Educations.DTOs;
using Vacancies.Application.Services.Experiences;
using Vacancies.Application.Services.Experiences.DTOs;
using Vacancies.Application.Services.Permissions;
using Vacancies.Application.Services.Permissions.DTOs;
using Vacancies.Application.Services.Resumes;
using Vacancies.Application.Services.Resumes.DTOs;
using Vacancies.Application.Services.Roles;
using Vacancies.Application.Services.Roles.DTOs;
using Vacancies.Application.Services.Salaries;
using Vacancies.Application.Services.Salaries.DTOs;
using Vacancies.Application.Services.Users;
using Vacancies.Application.Services.Users.DTOs;
using Vacancies.Application.Services.Vacancies;
using Vacancies.Application.Services.Vacancies.DTOs;
using Vacancies.Application.Validators.Ages;
using Vacancies.Application.Validators.Categories;
using Vacancies.Application.Validators.Cities;
using Vacancies.Application.Validators.Educations;
using Vacancies.Application.Validators.Experiences;
using Vacancies.Application.Validators.Permissions;
using Vacancies.Application.Validators.Resumes;
using Vacancies.Application.Validators.Roles;
using Vacancies.Application.Validators.Salaries;
using Vacancies.Application.Validators.Users;
using Vacancies.Application.Validators.Vacancies;
using Vacancies.Infrastructure.Handlers;
using Vacancies.Infrastructure.Repositories.Ages;
using Vacancies.Infrastructure.Repositories.Categories;
using Vacancies.Infrastructure.Repositories.Cities;
using Vacancies.Infrastructure.Repositories.Educations;
using Vacancies.Infrastructure.Repositories.Experiences;
using Vacancies.Infrastructure.Repositories.Permissions;
using Vacancies.Infrastructure.Repositories.Resumes;
using Vacancies.Infrastructure.Repositories.Roles;
using Vacancies.Infrastructure.Repositories.Salaries;
using Vacancies.Infrastructure.Repositories.Users;
using Vacancies.Infrastructure.Repositories.Vacancies;
using Vacancies.Presentation.Filters;

namespace Vacancies.Presentation.Extentions
{
    public static class ServiceExtention
    {
        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddSingleton<IDbHandler, DbHandler>();
            RegisterServices(services);
            RegisterRepositories(services);
            RegisterMappers(services);
            RegisterValidators(services);
            RegisterFilters(services);
        }

        private static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IPermissionService, PermissionService>();
            services.AddScoped<IAgeService, AgeService>();
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<IEducationService, EducationService>();
            services.AddScoped<ISalaryService, SalaryService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IResumeService, ResumeService>();
            services.AddScoped<IExperienceService, ExperienceService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IVacanciesService, VacanciesService>();
        }

        private static void RegisterRepositories(IServiceCollection services)
        {
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IPermissionRepository, PermissionRepository>();
            services.AddScoped<IAgeRepository, AgeRepository>();
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<IEducationRepository, EducationRepository>();
            services.AddScoped<IExperienceRepository, ExperienceRepository>();
            services.AddScoped<ISalaryRepository, SalaryRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IResumeRepository, ResumeRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IVacancyRepository, VacancyRepository>();
        }

        private static void RegisterMappers(IServiceCollection services)
        {
            MapperConfiguration mapperConfiguration = new MapperConfiguration(config =>
            {
                config.AddProfile(new MapperProfile());
            });
            IMapper mapper = mapperConfiguration.CreateMapper();
            services.AddSingleton(mapper);
        }

        private static void RegisterValidators(IServiceCollection services)
        {
            #region Ages
            services.AddTransient<IValidator<AgeInsertDTO>, AgeInsertValidator>();
            services.AddTransient<IValidator<AgeUpdateDTO>, AgeUpdateValidator>();
            #endregion

            #region Categories
            services.AddTransient<IValidator<CategoryInsertDTO>, CategoryInsertValidator>();
            services.AddTransient<IValidator<CategoryUpdateDTO>, CategoryUpdateValidator>();
            #endregion

            #region Vacancies
            services.AddTransient<IValidator<VacancyInsertDTO>, VacancyInsertValidator>();
            services.AddTransient<IValidator<VacancyUpdateDTO>, VacancyUpdateValidator>();
            #endregion

            #region Cities
            services.AddTransient<IValidator<CityInsertDTO>, CityInsertValidator>();
            services.AddTransient<IValidator<CityUpdateDTO>, CityUpdateValidator>();
            #endregion

            #region Educations
            services.AddTransient<IValidator<EducationInsertDTO>, EducationInsertValidator>();
            services.AddTransient<IValidator<EducationUpdateDTO>, EducationUpdateValidator>();
            #endregion

            #region Experiences
            services.AddTransient<IValidator<ExperienceInsertDTO>, ExperienceInsertValidator>();
            services.AddTransient<IValidator<ExperienceUpdateDTO>, ExperienceUpdateValidator>();
            #endregion

            #region Permissions
            services.AddTransient<IValidator<PermissionInsertDTO>, PermissionInsertValidator>();
            services.AddTransient<IValidator<PermissionUpdateDTO>, PermissionUpdateValidator>();
            #endregion

            #region Resumes
            services.AddTransient<IValidator<ResumeInsertDTO>, ResumeInsertValidator>();
            services.AddTransient<IValidator<ResumeUpdateDTO>, ResumeUpdateValidator>();
            #endregion

            #region Roles
            services.AddTransient<IValidator<RoleInsertDTO>, RoleInsertValidator>();
            services.AddTransient<IValidator<RoleUpdateDTO>, RoleUpdateValidator>();
            #endregion

            #region Salaries
            services.AddTransient<IValidator<SalaryInsertDTO>, SalaryInsertValidator>();
            services.AddTransient<IValidator<SalaryUpdateDTO>, SalaryUpdateValidator>();
            #endregion

            #region Users
            services.AddTransient<IValidator<UserInsertDTO>, UserInsertValidator>();
            services.AddTransient<IValidator<UserUpdateDTO>, UserUpdateValidator>();
            services.AddTransient<IValidator<UserAuthDTO>, UserAuthValidator>();
            #endregion
        }

        private static void RegisterFilters(IServiceCollection services)
        {
            services.AddTransient<UpdateFilter>();
            services.AddTransient<InsertFilter>();
        }
    }
}
