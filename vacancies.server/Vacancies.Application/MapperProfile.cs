﻿using AutoMapper;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Ages.DTOs;
using Vacancies.Application.Services.Categories.DTOs;
using Vacancies.Application.Services.Cities.DTOs;
using Vacancies.Application.Services.Educations.DTOs;
using Vacancies.Application.Services.Experiences.DTOs;
using Vacancies.Application.Services.Permissions.DTOs;
using Vacancies.Application.Services.Resumes.DTOs;
using Vacancies.Application.Services.Roles.DTOs;
using Vacancies.Application.Services.Salaries.DTOs;
using Vacancies.Application.Services.Users.DTOs;
using Vacancies.Application.Services.Vacancies.DTOs;
using Vacancies.Domain.Models.Ages;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Categories;
using Vacancies.Domain.Models.Cities;
using Vacancies.Domain.Models.Educations;
using Vacancies.Domain.Models.Experiences;
using Vacancies.Domain.Models.Permissions;
using Vacancies.Domain.Models.Resumes;
using Vacancies.Domain.Models.Roles;
using Vacancies.Domain.Models.Salaries;
using Vacancies.Domain.Models.Users;
using Vacancies.Domain.Models.Vacancies;

namespace Vacancies.Application
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            MapBases();
            MapRoles();
            MapPermissions();
            MapAges();
            MapCities();
            MapEducations();
            MapExperience();
            MapSalaries();
            MapUsers();
            MapResumes();
            MapCategories();
            MapVacancies();
        }

        private void MapBases()
        {
            CreateMap<SelectItem, SelectItemDTO>();
        }

        private void MapRoles()
        {
            CreateMap<Role, RoleDTO>();
            CreateMap<Role, RoleUpdateDTO>().ReverseMap();
            CreateMap<RoleInsertDTO, Role>();
            CreateMap<RoleCheckExistenceDTO, Role>();
            CreateMap<RolePermission, RolePermissionDTO>().ReverseMap();
            CreateMap<RoleSearchDTO, RoleSearch>();
            CreateMap<RoleInsertDTO, RoleCheckExistenceDTO>();
            CreateMap<RoleUpdateDTO, RoleCheckExistenceDTO>();
        }

        private void MapPermissions()
        {
            CreateMap<Permission, PermissionDTO>();
            CreateMap<Permission, PermissionUpdateDTO>().ReverseMap();
            CreateMap<PermissionInsertDTO, Permission>();
            CreateMap<PermissionCheckExistenceDTO, Permission>();
            CreateMap<PermissionSearchDTO, PermissionSearch>();
            CreateMap<PermissionInsertDTO, PermissionCheckExistenceDTO>();
            CreateMap<PermissionUpdateDTO, PermissionCheckExistenceDTO>();
        }

        private void MapAges()
        {
            CreateMap<Age, AgeDTO>();
            CreateMap<Age, AgeUpdateDTO>().ReverseMap();
            CreateMap<AgeInsertDTO, Age>();
            CreateMap<AgeCheckExistenceDTO, Age>();
            CreateMap<AgeSearchDTO, AgeSearch>();
            CreateMap<AgeInsertDTO, AgeCheckExistenceDTO>();
            CreateMap<AgeUpdateDTO, AgeCheckExistenceDTO>();
        }

        private void MapCities()
        {
            CreateMap<City, CityDTO>();
            CreateMap<City, CityUpdateDTO>().ReverseMap();
            CreateMap<CityInsertDTO, City>();
            CreateMap<CityCheckExistenceDTO, City>();
            CreateMap<CitySearchDTO, CitySearch>();
            CreateMap<CityInsertDTO, CityCheckExistenceDTO>();
            CreateMap<CityUpdateDTO, CityCheckExistenceDTO>();
        }

        private void MapEducations()
        {
            CreateMap<Education, EducationDTO>();
            CreateMap<Education, EducationUpdateDTO>().ReverseMap();
            CreateMap<EducationInsertDTO, Education>();
            CreateMap<EducationCheckExistenceDTO, Education>();
            CreateMap<EducationSearchDTO, EducationSearch>();
            CreateMap<EducationInsertDTO, EducationCheckExistenceDTO>();
            CreateMap<EducationUpdateDTO, EducationCheckExistenceDTO>();
        }

        private void MapExperience()
        {
            CreateMap<Experience, ExperienceDTO>();
            CreateMap<Experience, ExperienceUpdateDTO>().ReverseMap();
            CreateMap<ExperienceInsertDTO, Experience>();
            CreateMap<ExperienceCheckExistenceDTO, Experience>();
            CreateMap<ExperienceSearchDTO, ExperienceSearch>();
            CreateMap<ExperienceInsertDTO, ExperienceCheckExistenceDTO>();
            CreateMap<ExperienceUpdateDTO, ExperienceCheckExistenceDTO>();
        }

        private void MapSalaries()
        {
            CreateMap<Salary, SalaryDTO>();
            CreateMap<Salary, SalaryUpdateDTO>().ReverseMap();
            CreateMap<SalaryInsertDTO, Salary>();
            CreateMap<SalaryCheckExistenceDTO, Salary>();
            CreateMap<SalarySearchDTO, SalarySearch>();
            CreateMap<SalaryInsertDTO, SalaryCheckExistenceDTO>();
            CreateMap<SalaryUpdateDTO, SalaryCheckExistenceDTO>();
        }

        private void MapUsers()
        {
            CreateMap<User, UserDTO>();
            CreateMap<User, UserUpdateDTO>().ReverseMap();
            CreateMap<UserInsertDTO, User>();
            CreateMap<UserCheckExistenceUserNameDTO, User>();
            CreateMap<UserCheckExistenceEmailDTO, User>();
            CreateMap<UserRole, UserRoleDTO>().ReverseMap();
            CreateMap<UserSearchDTO, UserSearch>();
            CreateMap<UserInsertDTO, UserCheckExistenceUserNameDTO>();
            CreateMap<UserInsertDTO, UserCheckExistenceEmailDTO>();
            CreateMap<UserUpdateDTO, UserCheckExistenceUserNameDTO>();
            CreateMap<UserUpdateDTO, UserCheckExistenceEmailDTO>();
        }

        private void MapCategories()
        {
            CreateMap<Category, CategoryDTO>();
            CreateMap<Category, CategoryUpdateDTO>().ReverseMap();
            CreateMap<CategoryInsertDTO, Category>();
            CreateMap<CategoryCheckExistenceDTO, Category>();
            CreateMap<CategoryItem, CategoryItemDTO>().ReverseMap();
            CreateMap<CategorySearchDTO, CategorySearch>();
            CreateMap<CategoryInsertDTO , CategoryCheckExistenceDTO >();
            CreateMap<CategoryUpdateDTO, CategoryCheckExistenceDTO>();
        }

        private void MapResumes()
        {
            CreateMap<Resume, ResumeDTO>();

            CreateMap<ResumeInsertDTO, Resume>()
                .ForMember(s => s.CategoryItem, d => d.MapFrom(e => new CategoryItem() { Id = e.CategoryItemId }))
                .ForMember(s => s.Age, d => d.MapFrom(e => new Age() { Id = e.AgeId }))
                .ForMember(s => s.Salary, d => d.MapFrom(e => new Salary() { Id = e.SalaryId }))
                .ForMember(s => s.City, d => d.MapFrom(e => new City() { Id = e.CityId }))
                .ForMember(s => s.Education, d => d.MapFrom(e => new Education() { Id = e.EducationId }))
                .ForMember(s => s.Experience, d => d.MapFrom(e => new Experience() { Id = e.ExperienceId }));

            CreateMap<ResumeUpdateDTO, Resume>()
                .ForMember(s => s.CategoryItem, d => d.MapFrom(e => new CategoryItem() { Id = e.CategoryItemId }))
                .ForMember(s => s.Age, d => d.MapFrom(e => new Age() { Id = e.AgeId }))
                .ForMember(s => s.Salary, d => d.MapFrom(e => new Salary() { Id = e.SalaryId }))
                .ForMember(s => s.City, d => d.MapFrom(e => new City() { Id = e.CityId }))
                .ForMember(s => s.Education, d => d.MapFrom(e => new Education() { Id = e.EducationId }))
                .ForMember(s => s.Experience, d => d.MapFrom(e => new Experience() { Id = e.ExperienceId }))

                .ReverseMap()
                .ForMember(s => s.CategoryItemId, d => d.MapFrom(e => e.CategoryItem != null ? e.CategoryItem.Id : default(int)))
                .ForMember(s => s.CityId, d => d.MapFrom(e => e.City != null ? e.City.Id : default(int)))
                .ForMember(s => s.EducationId, d => d.MapFrom(e => e.Education != null ? e.Education.Id : default(int)))
                .ForMember(s => s.ExperienceId, d => d.MapFrom(e => e.Experience != null ? e.Education.Id : default(int)))
                .ForMember(s => s.SalaryId, d => d.MapFrom(e => e.Experience != null ? e.Salary.Id : default(int)))
                .ForMember(s => s.AgeId, d => d.MapFrom(e => e.Experience != null ? e.Age.Id : default(int)));

            CreateMap<ResumeCheckExistenceDTO, Resume>();
            CreateMap<ResumePhone, Resume>().ReverseMap();
            CreateMap<ResumeSearchDTO, ResumeSearch>();
        }
        private void MapVacancies()
        {
            CreateMap<Vacancy, VacancyDTO>();

            CreateMap<VacancyInsertDTO, Vacancy>()
                .ForMember(s => s.CategoryItem, d => d.MapFrom(e => new CategoryItem() { Id = e.CategoryItemId }))
                .ForMember(s => s.City, d => d.MapFrom(e => new City() { Id = e.CityId }))
                .ForMember(s => s.MinSalary, d => d.MapFrom(e => new Salary() { Id = e.MinSalaryId }))
                .ForMember(s => s.MaxSalary, d => d.MapFrom(e => new Salary() { Id = e.MaxSalaryId }))
                .ForMember(s => s.MinAge, d => d.MapFrom(e => new Age() { Id = e.MinAgeId }))
                .ForMember(s => s.MaxAge, d => d.MapFrom(e => new Age() { Id = e.MaxAgeId }))
                .ForMember(s => s.Education, d => d.MapFrom(e => new Education() { Id = e.EducationId }))
                .ForMember(s => s.Experience, d => d.MapFrom(e => new Experience() { Id = e.ExperienceId }));

            CreateMap<VacancyUpdateDTO, Vacancy>()
                .ForMember(s => s.CategoryItem, d => d.MapFrom(e => new CategoryItem() { Id = e.CategoryItemId }))
                .ForMember(s => s.City, d => d.MapFrom(e => new City() { Id = e.CityId }))
                .ForMember(s => s.MinSalary, d => d.MapFrom(e => new Salary() { Id = e.MinSalaryId }))
                .ForMember(s => s.MaxSalary, d => d.MapFrom(e => new Salary() { Id = e.MaxSalaryId }))
                .ForMember(s => s.MinAge, d => d.MapFrom(e => new Age() { Id = e.MinAgeId }))
                .ForMember(s => s.MaxAge, d => d.MapFrom(e => new Age() { Id = e.MaxAgeId }))
                .ForMember(s => s.Education, d => d.MapFrom(e => new Education() { Id = e.EducationId }))
                .ForMember(s => s.Experience, d => d.MapFrom(e => new Experience() { Id = e.ExperienceId }))
                .ReverseMap()
                .ForMember(s => s.CategoryItemId, d => d.MapFrom(e => e.CategoryItem != null ? e.CategoryItem.Id : default(int)))
                .ForMember(s => s.CityId, d => d.MapFrom(e => e.City != null ? e.City.Id : default(int)))
                .ForMember(s => s.MinSalaryId, d => d.MapFrom(e => e.MinSalary != null ? e.MinSalary.Id : default(int)))
                .ForMember(s => s.MaxSalaryId, d => d.MapFrom(e => e.MaxSalary != null ? e.MaxSalary.Id : default(int)))
                .ForMember(s => s.MinAgeId, d => d.MapFrom(e => e.MinAge != null ? e.MinAge.Id : default(int)))
                .ForMember(s => s.MaxAgeId, d => d.MapFrom(e => e.MaxAge != null ? e.MaxAge.Id : default(int)))
                .ForMember(s => s.EducationId, d => d.MapFrom(e => e.Education != null ? e.Education.Id : default(int)))
                .ForMember(s => s.ExperienceId, d => d.MapFrom(e => e.Experience != null ? e.Education.Id : default(int)));

            CreateMap<VacancyCheckExistenceDTO, Vacancy>();
            CreateMap<VacancyPhone, VacancyPhoneDTO>().ReverseMap();
            CreateMap<VacancySearchDTO, VacancySearch>();
        }
    }
}
