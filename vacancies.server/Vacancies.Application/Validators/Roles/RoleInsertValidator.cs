﻿using AutoMapper;
using FluentValidation;
using System.Collections.Generic;
using System.Linq;
using Vacancies.Application.Services.Roles;
using Vacancies.Application.Services.Roles.DTOs;

namespace Vacancies.Application.Validators.Roles
{
    public class RoleInsertValidator : AbstractValidator<RoleInsertDTO>
    {
        private readonly IMapper _mapper;
        private readonly IRoleService _roleService; 

        public RoleInsertValidator(IRoleService roleService,
            IMapper mapper)
        {
            _mapper = mapper;
            _roleService = roleService;

            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Name is required");
            RuleFor(e => e.Name).Must(CheckName).WithMessage("Name already exists!");
            RuleFor(e => e.RolePermissions)
                .Must(CheckRolePermissions).WithMessage("RolePermissions are required!");
        }

        private bool CheckRolePermissions(RoleInsertDTO roleInsertDTO, IEnumerable<RolePermissionDTO> rolePermissionDTOs)
        {
            if (rolePermissionDTOs == null || rolePermissionDTOs.Count() == 0)
                return false;
            return true;
        }

        private bool CheckName(RoleInsertDTO roleInsertDTO, string name)
        {
            RoleCheckExistenceDTO roleCheckExistenceDTO = _mapper.Map<RoleCheckExistenceDTO >(roleInsertDTO);
            bool checkName = _roleService.CheckExistence(roleCheckExistenceDTO);
            return !checkName;
        }
    }
}
