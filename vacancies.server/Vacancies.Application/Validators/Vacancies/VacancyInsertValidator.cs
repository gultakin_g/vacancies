﻿using FluentValidation;
using System.Linq;
using Vacancies.Application.Services.Vacancies.DTOs;

namespace Vacancies.Application.Validators.Vacancies
{
    public class VacancyInsertValidator : AbstractValidator<VacancyInsertDTO>
    {
        public VacancyInsertValidator()
        {
            RuleFor(e => e.Email).NotNull().NotEmpty().WithMessage("Email is required");
            RuleFor(e => e.Position).NotNull().NotEmpty().WithMessage("Position is required");
            RuleFor(e => e.CompanyName).NotNull().NotEmpty().WithMessage("CompanyName is required");
            RuleFor(e => e.RelevantPerson).NotNull().NotEmpty().WithMessage("RelevantPerson is required");
            RuleFor(e => e.CandidateRequirements).NotNull().NotEmpty().WithMessage("CandidateRequirements is required");
            RuleFor(e => e.AboutWork).NotNull().NotEmpty().WithMessage("AboutWork is required");
            RuleFor(e => e.CategoryItemId).GreaterThan(0).WithMessage("CategoryItemId is required");
            RuleFor(e => e.CityId).GreaterThan(0).WithMessage("CityId is required");
            RuleFor(e => e.MinSalaryId).GreaterThan(0).WithMessage("MinSalaryId is required");
            RuleFor(e => e.MaxSalaryId).GreaterThan(0).WithMessage("MaxSalaryId is required");
            RuleFor(e => e.MinAgeId).GreaterThan(0).WithMessage("MinAgeId is required");
            RuleFor(e => e.MaxAgeId).GreaterThan(0).WithMessage("MaxAgeId is required");
            RuleFor(e => e.EducationId).GreaterThan(0).WithMessage("EducationId is required");
            RuleFor(e => e.ExperienceId).GreaterThan(0).WithMessage("ExperienceId is required");
            RuleFor(e => e.VacancyPhones)
                .NotNull().WithMessage("VacancyPhones are required")
                .NotEmpty().WithMessage("VacancyPhones are required")
                .Must(e => e?.Count() > 0).WithMessage("VacancyPhones are required");
        }
    }
}
