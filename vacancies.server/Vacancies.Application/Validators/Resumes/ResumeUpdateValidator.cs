﻿using FluentValidation;
using System.Linq;
using Vacancies.Application.Services.Resumes.DTOs;
using Vacancies.Domain.Enums;

namespace Vacancies.Application.Validators.Resumes
{
    public class ResumeUpdateValidator : AbstractValidator<ResumeUpdateDTO>
    {
        public ResumeUpdateValidator()
        {
            RuleFor(e => e.Id).GreaterThan(0).WithMessage("Id is required");
            RuleFor(e => e.FirstName).NotNull().NotEmpty().WithMessage("FirstName is required");
            RuleFor(e => e.LastName).NotNull().NotEmpty().WithMessage("LastName is required");
            RuleFor(e => e.MiddleName).NotNull().NotEmpty().WithMessage("MiddleName is required");
            RuleFor(e => e.Gender).Must(e => e == Gender.Male || e == Gender.Female).WithMessage("Gender is required");
            RuleFor(e => e.AboutExperience).NotNull().NotEmpty().WithMessage("AboutExperience is required");
            RuleFor(e => e.AboutEducation).NotNull().NotEmpty().WithMessage("AboutEducation is required");
            RuleFor(e => e.MoreInformation).NotNull().NotEmpty().WithMessage("MoreInformation is required");
            RuleFor(e => e.Position).NotNull().NotEmpty().WithMessage("Position is required");
            RuleFor(e => e.Skills).NotNull().NotEmpty().WithMessage("Skills is required");
            RuleFor(e => e.Email).NotNull().NotEmpty().WithMessage("Email is required");
            RuleFor(e => e.ExpiryDate).NotNull().NotEmpty().WithMessage("ExpiryDate is required");
            RuleFor(e => e.CityId).GreaterThan(0).WithMessage("CityId are requeired");
            RuleFor(e => e.SalaryId).GreaterThan(0).WithMessage("SalaryId are requeired");
            RuleFor(e => e.AgeId).GreaterThan(0).WithMessage("AgeId are requeired");
            RuleFor(e => e.CategoryItemId).GreaterThan(0).WithMessage("CategoryItemId are requeired");
            RuleFor(e => e.EducationId).GreaterThan(0).WithMessage("EducationId are requeired");
            RuleFor(e => e.ExperienceId).GreaterThan(0).WithMessage("ExperienceId are requeired");
            RuleFor(e => e.ResumePhones)
               .NotNull().WithMessage("ResumePhones are required")
               .NotEmpty().WithMessage("ResumePhones are required")
               .Must(e => e?.Count() > 0).WithMessage("ResumePhones are required");
        }
    }
}
