﻿using AutoMapper;
using FluentValidation;
using Vacancies.Application.Services.Experiences;
using Vacancies.Application.Services.Experiences.DTOs;

namespace Vacancies.Application.Validators.Experiences
{
    public class ExperienceInsertValidator : AbstractValidator<ExperienceInsertDTO>
    {
        private readonly IExperienceService _experienceService;
        private readonly IMapper _mapper;

        public ExperienceInsertValidator(IExperienceService experienceService,
            IMapper mapper)
        {
            _experienceService = experienceService;
            _mapper = mapper;

            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Name is required!");
            RuleFor(e => e.Name).Must(CheckName).WithMessage("Name already exists!");
        }

        private bool CheckName(ExperienceInsertDTO experienceInsertDTO, string name)
        {
            ExperienceCheckExistenceDTO experienceCheckExistenceDTO = _mapper.Map<ExperienceCheckExistenceDTO>(experienceInsertDTO);
            bool checkName = _experienceService.CheckExistence(experienceCheckExistenceDTO);
            return !checkName;
        }
    }
}
