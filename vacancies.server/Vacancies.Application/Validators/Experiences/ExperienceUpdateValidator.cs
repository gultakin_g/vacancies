﻿using AutoMapper;
using FluentValidation;
using Vacancies.Application.Services.Experiences;
using Vacancies.Application.Services.Experiences.DTOs;

namespace Vacancies.Application.Validators.Experiences
{
    public class ExperienceUpdateValidator : AbstractValidator<ExperienceUpdateDTO>
    {
        private readonly IExperienceService _experienceService;
        private readonly IMapper _mapper;
        public ExperienceUpdateValidator(IExperienceService experienceService,
            IMapper mapper)
        {
            _experienceService = experienceService;
            _mapper = mapper;

            RuleFor(e => e.Id).GreaterThan(0).WithMessage("Id is required!");
            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Name is required!");
            RuleFor(e => e.Name).Must(CheckName).WithMessage("Name already exists!");
        }

        private bool CheckName(ExperienceUpdateDTO experienceUpdateDTO, string name)
        {
            ExperienceCheckExistenceDTO experienceCheckExistenceDTO = _mapper.Map<ExperienceCheckExistenceDTO>(experienceUpdateDTO);
            bool checkName = _experienceService.CheckExistence(experienceCheckExistenceDTO);
            return !checkName;
        }
    }
}
