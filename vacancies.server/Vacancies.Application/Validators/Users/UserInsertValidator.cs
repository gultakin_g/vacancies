﻿using AutoMapper;
using FluentValidation;
using System.Collections.Generic;
using System.Linq;
using Vacancies.Application.Services.Users;
using Vacancies.Application.Services.Users.DTOs;

namespace Vacancies.Application.Validators.Users
{
    public class UserInsertValidator : AbstractValidator<UserInsertDTO>
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        public UserInsertValidator(IMapper mapper, IUserService userService)
        {
            _userService = userService;
            _mapper = mapper;

            RuleFor(e => e.FirstName).NotNull().NotEmpty().WithMessage("FirstName is required");
            RuleFor(e => e.LastName).NotNull().NotEmpty().WithMessage("LastName is required");
            RuleFor(e => e.MiddleName).NotNull().NotEmpty().WithMessage("MiddleName is required");
            RuleFor(e => e.BirthDate).NotNull().NotEmpty().WithMessage("BirthDate is required");
            RuleFor(e => e.Email).NotNull().NotEmpty().WithMessage("Email is required");
            RuleFor(e => e.PhoneNumber).NotNull().NotEmpty().WithMessage("PhoneNumber is required");
            RuleFor(e => e.UserName).NotNull().NotEmpty().WithMessage("UserName is required");
            RuleFor(e => e.Password).NotNull().NotEmpty().WithMessage("Password is required");
            RuleFor(e => e.UserRoles).Must(CheckUserRoles).WithMessage("UserRoles are required!");
            RuleFor(e => e.Email).Must(CheckEmail).WithMessage("Email are required!");
            RuleFor(e => e.UserName).Must(CheckUserName).WithMessage("UserName are required!");
        }

        private bool CheckUserRoles(UserInsertDTO userInsertDTO, IEnumerable<UserRoleDTO> userRoleDTOs)
        {
            if (userRoleDTOs == null || userRoleDTOs.Count() == 0)
                return false;
            return true;
        }

        private bool CheckEmail(UserInsertDTO userInsertDTO, string name)
        {
            UserCheckExistenceEmailDTO userCheckExistenceEmailDTO = _mapper.Map<UserCheckExistenceEmailDTO>(userInsertDTO);
            bool checkEmail = _userService.CheckExistenceEmail(userCheckExistenceEmailDTO);
            return !checkEmail;
        }

        private bool CheckUserName(UserInsertDTO userInsertDTO, string name)
        {
            UserCheckExistenceUserNameDTO userCheckExistenceUser = _mapper.Map<UserCheckExistenceUserNameDTO>(userInsertDTO);
            bool checkUserName = _userService.CheckExistenceUserName(userCheckExistenceUser);
            return !checkUserName;
        }
    }
}
