﻿using FluentValidation;
using Vacancies.Application.Services.Users.DTOs;

namespace Vacancies.Application.Validators.Users
{
    public class UserAuthValidator : AbstractValidator<UserAuthDTO>
    {
        public UserAuthValidator()
        {
            RuleFor(e => e.UserName).NotNull().WithMessage("UserName is required!");
            RuleFor(e => e.Password).NotNull().WithMessage("Password is required!");
        }
    }
}
