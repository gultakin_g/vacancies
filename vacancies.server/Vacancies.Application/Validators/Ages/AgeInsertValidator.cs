﻿using AutoMapper;
using FluentValidation;
using Vacancies.Application.Services.Ages;
using Vacancies.Application.Services.Ages.DTOs;

namespace Vacancies.Application.Validators.Ages
{
    public class AgeInsertValidator : AbstractValidator<AgeInsertDTO>
    {
        private readonly IAgeService _ageService;
        private readonly IMapper _mapper;

        public AgeInsertValidator(IAgeService ageService,
            IMapper mapper)
        {
            _ageService = ageService;
            _mapper = mapper;
            RuleFor(e => e.Amount).GreaterThan(17).WithMessage("Amount is required!");
            RuleFor(e => e.Amount).Must(CheckAmount).WithMessage("Amount already exists!");
        }

        private bool CheckAmount(AgeInsertDTO ageInsertDTO, int amount)
        {
            AgeCheckExistenceDTO ageCheckExistenceDTO = _mapper.Map<AgeCheckExistenceDTO>(ageInsertDTO);
            bool checkAmount = _ageService.CheckExistence(ageCheckExistenceDTO);
            return !checkAmount;
        }
    }
}
