﻿using AutoMapper;
using FluentValidation;
using Vacancies.Application.Services.Ages;
using Vacancies.Application.Services.Ages.DTOs;

namespace Vacancies.Application.Validators.Ages
{
    public class AgeUpdateValidator : AbstractValidator<AgeUpdateDTO>
    {
        private readonly IMapper _mapper;
        private readonly IAgeService _ageService;

        public AgeUpdateValidator(IMapper mapper, 
            IAgeService ageService)
        {
            _mapper = mapper;
            _ageService = ageService;

            RuleFor(e => e.Id).GreaterThan(0).WithMessage("Id is required!");
            RuleFor(e => e.Amount).GreaterThan(17).WithMessage("Amount is required!");
            RuleFor(e => e.Amount).Must(CheckAmount).WithMessage("Amount already exists");
        }

        private bool CheckAmount(AgeUpdateDTO ageUpdateDTO, int amount)
        {
            AgeCheckExistenceDTO ageCheckExistenceDTO = _mapper.Map<AgeCheckExistenceDTO>(ageUpdateDTO);
            bool checkAmount = _ageService.CheckExistence(ageCheckExistenceDTO);
            return !checkAmount;
        }
    }
}
