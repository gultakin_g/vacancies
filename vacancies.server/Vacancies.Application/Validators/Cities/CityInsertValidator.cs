﻿using AutoMapper;
using FluentValidation;
using Vacancies.Application.Services.Cities;
using Vacancies.Application.Services.Cities.DTOs;

namespace Vacancies.Application.Validators.Cities
{
    public class CityInsertValidator : AbstractValidator<CityInsertDTO>
    {
        private readonly ICityService _cityService;
        private readonly IMapper _mapper;
        public CityInsertValidator(ICityService cityService,
            IMapper mapper)
        {
            _cityService = cityService;
            _mapper = mapper;

            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Name is required!");
            RuleFor(e => e.Name).Must(CheckName).WithMessage("Name already exists!");
        }

        private bool CheckName(CityInsertDTO cityInsertDTO, string name)
        {
            CityCheckExistenceDTO cityCheckExistenceDTO = _mapper.Map<CityCheckExistenceDTO>(cityInsertDTO);
            bool checkName = _cityService.CheckExistence(cityCheckExistenceDTO);
            return !checkName;
        }
    }
}
