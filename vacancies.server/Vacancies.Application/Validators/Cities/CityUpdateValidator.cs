﻿using AutoMapper;
using FluentValidation;
using Vacancies.Application.Services.Cities;
using Vacancies.Application.Services.Cities.DTOs;

namespace Vacancies.Application.Validators.Cities
{
    public class CityUpdateValidator : AbstractValidator<CityUpdateDTO>
    {
        private readonly ICityService _cityService;
        private readonly IMapper _mapper;
        public CityUpdateValidator(ICityService cityService,
            IMapper mapper)
        {
            _cityService = cityService;
            _mapper = mapper;

            RuleFor(e => e.Id).GreaterThan(0).WithMessage("Id is required!");
            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Name is required!");
            RuleFor(e => e.Name).Must(CheckName).WithMessage("Name already exists!");
        }

        private bool CheckName(CityUpdateDTO cityUpdateDTO, string name)
        {
            CityCheckExistenceDTO cityCheckExistenceDTO = _mapper.Map<CityCheckExistenceDTO>(cityUpdateDTO);
            bool checkName = _cityService.CheckExistence(cityCheckExistenceDTO);
            return !checkName;
        }
    }
}
