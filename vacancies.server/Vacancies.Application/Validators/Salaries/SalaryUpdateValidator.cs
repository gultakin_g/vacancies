﻿using AutoMapper;
using FluentValidation;
using Vacancies.Application.Services.Salaries;
using Vacancies.Application.Services.Salaries.DTOs;

namespace Vacancies.Application.Validators.Salaries
{
    public class SalaryUpdateValidator : AbstractValidator<SalaryUpdateDTO>
    {
        private readonly ISalaryService _salaryService;
        private readonly IMapper _mapper;

        public SalaryUpdateValidator(ISalaryService salaryService,
            IMapper mapper)
        {
            _salaryService = salaryService;
            _mapper = mapper;

            RuleFor(e => e.Id).GreaterThan(0).WithMessage("Id is required");
            RuleFor(e => e.Amount).GreaterThan(0).WithMessage("Amount is required");
            RuleFor(e => e.Amount).Must(CheckAmount).WithMessage("Amount is required");
        }

        private bool CheckAmount(SalaryUpdateDTO salaryUpdateDTO, double Amount)
        {
            SalaryCheckExistenceDTO salaryCheckExistenceDTO = _mapper.Map<SalaryCheckExistenceDTO>(salaryUpdateDTO);
            bool checkName = _salaryService.CheckExistence(salaryCheckExistenceDTO);
            return !checkName;
        }
    }
}
