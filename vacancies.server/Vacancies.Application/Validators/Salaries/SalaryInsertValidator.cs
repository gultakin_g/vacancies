﻿using AutoMapper;
using FluentValidation;
using Vacancies.Application.Services.Salaries;
using Vacancies.Application.Services.Salaries.DTOs;

namespace Vacancies.Application.Validators.Salaries
{
    public class SalaryInsertValidator : AbstractValidator<SalaryInsertDTO>
    {
        private readonly ISalaryService _salaryService;
        private readonly IMapper _mapper;
        public SalaryInsertValidator(ISalaryService salaryService,
            IMapper mapper)
        {
            _salaryService = salaryService;
            _mapper = mapper;

            RuleFor(e => e.Amount).GreaterThan(0).WithMessage("Amount is required");
            RuleFor(e => e.Amount).Must(CheckAmount).WithMessage("Amount is required");
        }

        private bool CheckAmount(SalaryInsertDTO salaryInsertDTO, double amount)
        {
            SalaryCheckExistenceDTO salaryCheckExistenceDTO = _mapper.Map<SalaryCheckExistenceDTO>(salaryInsertDTO);
            bool checkName = _salaryService.CheckExistence(salaryCheckExistenceDTO);
            return !checkName;
        }
    }
}
