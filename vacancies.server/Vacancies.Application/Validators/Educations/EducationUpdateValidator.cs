﻿using AutoMapper;
using FluentValidation;
using Vacancies.Application.Services.Educations;
using Vacancies.Application.Services.Educations.DTOs;

namespace Vacancies.Application.Validators.Educations
{
    public class EducationUpdateValidator : AbstractValidator<EducationUpdateDTO>
    {
        private readonly IEducationService _educationService;
        private readonly IMapper _mapper;

        public EducationUpdateValidator(IEducationService educationService,
            IMapper mapper)
        {
            _educationService = educationService;
            _mapper = mapper;

            RuleFor(e => e.Id).GreaterThan(0).WithMessage("Id is required!");
            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Name is required!");
            RuleFor(e => e.Name).Must(CheckName).WithMessage("Name already exists!");
        }

        private bool CheckName(EducationUpdateDTO educationUpdateDTO, string name)
        {
            EducationCheckExistenceDTO educationCheckExistenceDTO = _mapper.Map<EducationCheckExistenceDTO>(educationUpdateDTO);
            bool checkName = _educationService.CheckExistence(educationCheckExistenceDTO);
            return !checkName;
        }
    }
}
