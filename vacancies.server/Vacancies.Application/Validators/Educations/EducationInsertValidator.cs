﻿using AutoMapper;
using FluentValidation;
using Vacancies.Application.Services.Educations;
using Vacancies.Application.Services.Educations.DTOs;

namespace Vacancies.Application.Validators.Educations
{
    public class EducationInsertValidator : AbstractValidator<EducationInsertDTO>
    {
        private readonly IEducationService _educationService;
        private readonly IMapper _mapper;

        public EducationInsertValidator(IEducationService educationService,
            IMapper mapper)
        {
            _educationService = educationService;
            _mapper = mapper;

            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Name is required!");
            RuleFor(e => e.Name).Must(CheckName).WithMessage("Name already exists!");
        }

        private bool CheckName(EducationInsertDTO educationInsertDTO, string name)
        {
            EducationCheckExistenceDTO educationCheckExistenceDTO = _mapper.Map<EducationCheckExistenceDTO>(educationInsertDTO);
            bool checkName = _educationService.CheckExistence(educationCheckExistenceDTO);
            return !checkName;
        }
    }
}
