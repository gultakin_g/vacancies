﻿using AutoMapper;
using FluentValidation;
using Vacancies.Application.Services.Permissions;
using Vacancies.Application.Services.Permissions.DTOs;

namespace Vacancies.Application.Validators.Permissions
{
    public class PermissionInsertValidator : AbstractValidator<PermissionInsertDTO>
    {
        private readonly IPermissionService _permissionService;
        private readonly IMapper _mapper;

        public PermissionInsertValidator(IPermissionService permissionService,
            IMapper mapper)
        {
            _permissionService = permissionService;
            _mapper = mapper;

            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Name is required");
            RuleFor(e => e.Name).Must(CheckName).WithMessage("Name already exists!");
        }

        private bool CheckName(PermissionInsertDTO permissionInsertDTO, string name)
        {
            PermissionCheckExistenceDTO permissionCheckExistenceDTO = _mapper.Map<PermissionCheckExistenceDTO>(permissionInsertDTO);
            bool checkName = _permissionService.CheckExistence(permissionCheckExistenceDTO);
            return !checkName;
        }
    }
}
