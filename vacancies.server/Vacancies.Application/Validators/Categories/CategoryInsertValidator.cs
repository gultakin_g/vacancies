﻿using AutoMapper;
using FluentValidation;
using System.Collections.Generic;
using System.Linq;
using Vacancies.Application.Services.Categories;
using Vacancies.Application.Services.Categories.DTOs;

namespace Vacancies.Application.Validators.Categories
{
    public class CategoryInsertValidator : AbstractValidator<CategoryInsertDTO>
    {
        private readonly IMapper _mapper;
        private readonly ICategoryService _categoryService;

        public CategoryInsertValidator(IMapper mapper,
            ICategoryService categoryService)
        {
            _mapper = mapper;
            _categoryService = categoryService;

            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Name is required");
            RuleFor(e => e.Name).Must(CheckName).WithMessage("Name already exists!");
            RuleFor(e => e.CategoryItems)
                .Must(CheckItems).WithMessage("CategoryItems are required!");
        }

        private bool CheckName(CategoryInsertDTO categoryInsertDTO, string name)
        {
            CategoryCheckExistenceDTO categoryCheckExistenceDTO = _mapper.Map<CategoryCheckExistenceDTO>(categoryInsertDTO);
            bool checkName = _categoryService.CheckExistence(categoryCheckExistenceDTO);
            return !checkName;
        }

        private bool CheckItems(CategoryInsertDTO categoryInsertDTO, IEnumerable<CategoryItemDTO> categoryItems)
        {
            if (categoryItems == null || categoryItems.Count() == 0)
                return false;
            return true;
        }
    }
}
