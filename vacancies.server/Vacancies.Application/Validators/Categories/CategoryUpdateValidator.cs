﻿using AutoMapper;
using FluentValidation;
using System.Collections.Generic;
using System.Linq;
using Vacancies.Application.Services.Categories;
using Vacancies.Application.Services.Categories.DTOs;

namespace Vacancies.Application.Validators.Categories
{
    public class CategoryUpdateValidator : AbstractValidator<CategoryUpdateDTO>
    {
        private readonly IMapper _mapper;
        private readonly ICategoryService _categoryService;

        public CategoryUpdateValidator(IMapper mapper,
            ICategoryService categoryService)
        {
            _mapper = mapper;
            _categoryService = categoryService;

            RuleFor(e => e.Id).GreaterThan(0).WithMessage("Id is required!");
            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Id is required!");
            RuleFor(e => e.Name).Must(CheckName).WithMessage("Name already exists!");
            RuleFor(e => e.CategoryItems)
                .Must(CheckItems).WithMessage("CategoryItems are required!");
        }

        private bool CheckName(CategoryUpdateDTO categoryUpdateDTO, string name)
        {
            CategoryCheckExistenceDTO categoryCheckExistenceDTO = _mapper.Map<CategoryCheckExistenceDTO>(categoryUpdateDTO);
            bool checkName = _categoryService.CheckExistence(categoryCheckExistenceDTO);
            return !checkName;
        }

        private bool CheckItems(CategoryUpdateDTO categoryUpdateDTO, IEnumerable<CategoryItemDTO> categoryItems)
        {
            if (categoryItems == null || categoryItems.Count() == 0)
                return false;
            return true;
        }
    }
}
