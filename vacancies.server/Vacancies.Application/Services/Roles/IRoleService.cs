﻿using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Roles.DTOs;

namespace Vacancies.Application.Services.Roles
{
    public interface IRoleService
    {
        (IEnumerable<RoleDTO>, int) FindList(RoleSearchDTO roleSearchDTO);
        IEnumerable<SelectItemDTO> GetList();
        RoleUpdateDTO GetById(int id);
        bool CheckExistence(RoleCheckExistenceDTO roleCheckExistenceDTO);
        bool Insert(RoleInsertDTO roleInsertDTO);
        bool Update(RoleUpdateDTO roleUpdateDTO);
        bool Delete(int id);
    }
}