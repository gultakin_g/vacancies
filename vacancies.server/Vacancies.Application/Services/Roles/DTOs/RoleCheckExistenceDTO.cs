﻿namespace Vacancies.Application.Services.Roles.DTOs
{
    public class RoleCheckExistenceDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
