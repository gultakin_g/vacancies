﻿using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Roles.DTOs
{
    public class RoleInsertDTO : IInsertDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int CreatedBy { get; set; }
        public IEnumerable<RolePermissionDTO> RolePermissions { get; set; }
    }
}
