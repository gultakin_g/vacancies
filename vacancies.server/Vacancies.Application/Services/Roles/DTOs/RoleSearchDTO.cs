﻿using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Roles.DTOs
{
    public class RoleSearchDTO : BaseSearchDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
