﻿namespace Vacancies.Application.Services.Roles.DTOs
{
    public class RolePermissionDTO
    {
        public int RoleId { get; set; }
        public int PermissionId { get; set; }
    }
}
