﻿using AutoMapper;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Roles.DTOs;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Roles;
using Vacancies.Infrastructure.Repositories.Roles;

namespace Vacancies.Application.Services.Roles
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IMapper _mapper;

        public RoleService(
            IRoleRepository roleRepository,
            IMapper mapper)
        {
            _roleRepository = roleRepository;
            _mapper = mapper;
        }

        public (IEnumerable<RoleDTO>, int) FindList(RoleSearchDTO roleSearchDTO)
        {
            RoleSearch roleSearch = _mapper.Map<RoleSearch>(roleSearchDTO);
            (IEnumerable<Role> roles, int count) = _roleRepository.FindList(roleSearch);
            IEnumerable<RoleDTO> roleDTOs = _mapper.Map<IEnumerable<RoleDTO>>(roles);
            return (roleDTOs, count);
        }

        public IEnumerable<SelectItemDTO> GetList()
        {
            IEnumerable<SelectItem> selectItems = _roleRepository.GetList();
            IEnumerable<SelectItemDTO> selectItemDTOs = _mapper.Map<IEnumerable<SelectItemDTO>>(selectItems);
            return selectItemDTOs;
        }

        public RoleUpdateDTO GetById(int id)
        {
            Role role = _roleRepository.GetById(id);
            RoleUpdateDTO roleUpdateDTO = _mapper.Map<RoleUpdateDTO>(role);
            return roleUpdateDTO;
        }

        public bool CheckExistence(RoleCheckExistenceDTO roleCheckExistenceDTO)
        {
            Role role = _mapper.Map<Role>(roleCheckExistenceDTO);
            bool result = _roleRepository.CheckExistence(role);
            return result;
        }

        public bool Insert(RoleInsertDTO roleInsertDTO)
        {
            Role role = _mapper.Map<Role>(roleInsertDTO);
            bool result = _roleRepository.Insert(role);
            return result;
        }

        public bool Update(RoleUpdateDTO roleUpdateDTO)
        {
            Role role = _mapper.Map<Role>(roleUpdateDTO);
            bool result = _roleRepository.Update(role);
            return result;
        }

        public bool Delete(int id)
        {
            bool result = _roleRepository.Delete(id);
            return result;
        }
    }
}
