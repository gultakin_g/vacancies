﻿using System;
using Vacancies.Application.Services.Ages.DTOs;
using Vacancies.Application.Services.Categories.DTOs;
using Vacancies.Application.Services.Cities.DTOs;
using Vacancies.Application.Services.Educations.DTOs;
using Vacancies.Application.Services.Experiences.DTOs;
using Vacancies.Application.Services.Salaries.DTOs;
using Vacancies.Domain.Enums;

namespace Vacancies.Application.Services.Resumes.DTOs
{
    public class ResumeDTO
    {
        public string ImageUrl { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Gender { get; set; }
        public string AboutExperience { get; set; }
        public string AboutEducation { get; set; }
        public string MoreInformation { get; set; }
        public string Position { get; set; }
        public string Skills { get; set; }
        public string Email { get; set; }
        public DateTime ExpiryDate { get; set; }

        public AgeDTO Age { get; set; }
        public EducationDTO Education { get; set; }
        public ExperienceDTO Experience { get; set; }
        public CategoryItemDTO CategoryItem { get; set; }
        public CityDTO City { get; set; }
        public SalaryDTO Salary { get; set; }
    }
}
