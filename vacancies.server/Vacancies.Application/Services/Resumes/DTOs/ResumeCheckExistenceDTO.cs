﻿namespace Vacancies.Application.Services.Resumes.DTOs
{
    public class ResumeCheckExistenceDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
    }
}
