﻿using System;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Domain.Enums;
using Vacancies.Domain.Models.Resumes;

namespace Vacancies.Application.Services.Resumes.DTOs
{
    public class ResumeUpdateDTO : IUpdateDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string ImageUrl { get; set; }
        public Gender Gender { get; set; }
        public string AboutExperience { get; set; }
        public string AboutEducation { get; set; }
        public string MoreInformation { get; set; }
        public string Position { get; set; }
        public string Skills { get; set; }
        public string Email { get; set; }
        public DateTime ExpiryDate { get; set; }

        public int AgeId { get; set; }
        public int EducationId { get; set; }
        public int ExperienceId { get; set; }
        public int CategoryItemId { get; set; }
        public int CityId { get; set; }
        public int SalaryId { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate  { get; set; }
        public IEnumerable<ResumePhone> ResumePhones { get; set; }
    }
}
