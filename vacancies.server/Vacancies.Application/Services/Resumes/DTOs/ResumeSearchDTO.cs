﻿using Vacancies.Application.BaseDTOs;
using Vacancies.Domain.Enums;

namespace Vacancies.Application.Services.Resumes.DTOs
{
    public class ResumeSearchDTO : BaseSearchDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public Gender Gender { get; set; }
        public string AboutExperience { get; set; }
        public string AboutEducation { get; set; }
        public string MoreInformation { get; set; }
        public string Position { get; set; }
        public string Skills { get; set; }
        public string Email { get; set; }
    }
}
