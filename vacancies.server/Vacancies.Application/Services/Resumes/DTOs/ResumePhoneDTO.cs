﻿namespace Vacancies.Application.Services.Resumes.DTOs
{
    public class ResumePhoneDTO
    {
        public int ResumeId { get; set; }
        public string PhoneNumber { get; set; }
    }
}
