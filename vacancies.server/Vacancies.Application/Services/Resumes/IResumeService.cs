﻿using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Resumes.DTOs;
using Vacancies.Domain.Models.Bases;

namespace Vacancies.Application.Services.Resumes
{
    public interface IResumeService
    {
        (IEnumerable<ResumeDTO>, int) FindList(ResumeSearchDTO roleSearchDTO);
        IEnumerable<SelectItemDTO> GetList();
        ResumeUpdateDTO GetById(int id);
        bool CheckExistence(ResumeCheckExistenceDTO resumeCheckExistenceDTO);
        bool Insert(ResumeInsertDTO resumeInsertDTO);
        bool Update(ResumeUpdateDTO resumeUpdateDTO);
        bool Delete(DeleteEntity entity);
    }
}
