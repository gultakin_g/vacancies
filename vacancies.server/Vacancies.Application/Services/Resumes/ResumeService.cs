﻿using AutoMapper;
using System;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Resumes.DTOs;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Resumes;
using Vacancies.Infrastructure.Repositories.Resumes;

namespace Vacancies.Application.Services.Resumes
{
    public class ResumeService : IResumeService
    {
        private readonly IResumeRepository _resumeRepository;
        private readonly IMapper _mapper;

        public ResumeService(IResumeRepository resumeRepository, IMapper mapper)
        {
            _resumeRepository = resumeRepository;
            _mapper = mapper;
        }
        public (IEnumerable<ResumeDTO>, int) FindList(ResumeSearchDTO roleSearchDTO)
        {
            ResumeSearch resumeSearch = _mapper.Map<ResumeSearch>(roleSearchDTO);
            (IEnumerable<Resume> resumes, int count) = _resumeRepository.FindList(resumeSearch);
            IEnumerable<ResumeDTO> resumeDTOs = _mapper.Map<IEnumerable<ResumeDTO>>(resumes);
            return (resumeDTOs, count);
        }

        public IEnumerable<SelectItemDTO> GetList()
        {
            throw new NotImplementedException();
        }

        public ResumeUpdateDTO GetById(int id)
        {
            Resume resume = _resumeRepository.GetById(id);
            ResumeUpdateDTO resumeUpdateDTO = _mapper.Map<ResumeUpdateDTO>(resume);
            return resumeUpdateDTO;
        }

        public bool CheckExistence(ResumeCheckExistenceDTO resumeCheckExistenceDTO)
        {
            Resume resume = _mapper.Map<Resume>(resumeCheckExistenceDTO);
            bool result = _resumeRepository.CheckExistence(resume);
            return result;
        }

        public bool Insert(ResumeInsertDTO resumeInsertDTO)
        {
            Resume resume = _mapper.Map<Resume>(resumeInsertDTO);
            bool result = _resumeRepository.Insert(resume);
            return result;
        }

        public bool Update(ResumeUpdateDTO resumeUpdateDTO)
        {
            Resume resume = _mapper.Map<Resume>(resumeUpdateDTO);
            bool result = _resumeRepository.Update(resume);
            return result;
        }

        public bool Delete(DeleteEntity entity)
        {
            bool result = _resumeRepository.Delete(entity);
            return result;
        }
    }
}
