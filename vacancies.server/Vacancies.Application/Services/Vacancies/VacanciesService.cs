﻿using AutoMapper;
using System;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Vacancies.DTOs;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Vacancies;
using Vacancies.Infrastructure.Repositories.Vacancies;

namespace Vacancies.Application.Services.Vacancies
{
    public class VacanciesService : IVacanciesService
    {
        private readonly IVacancyRepository _vacancyRepository;
        private readonly IMapper _mapper;

        public VacanciesService(IVacancyRepository vacancyRepository, IMapper mapper)
        {
            _vacancyRepository = vacancyRepository;
            _mapper = mapper;
        }

        public (IEnumerable<VacancyDTO>, int) FindList(VacancySearchDTO vacancySearchDTO)
        {
            VacancySearch vacancySearch = _mapper.Map<VacancySearch>(vacancySearchDTO);
            (IEnumerable<Vacancy> vacancies, int count) = _vacancyRepository.FindList(vacancySearch);
            IEnumerable<VacancyDTO> vacancyDTOs = _mapper.Map<IEnumerable<VacancyDTO>>(vacancies);
            return (vacancyDTOs, count);
        }

        public IEnumerable<SelectItemDTO> GetList()
        {
            IEnumerable<SelectItem> selectItems = _vacancyRepository.GetList();
            IEnumerable<SelectItemDTO> selectItemDTOs = _mapper.Map<IEnumerable<SelectItemDTO>>(selectItems);
            return selectItemDTOs;
        }

        public VacancyUpdateDTO GetById(int id)
        {
            Vacancy vacancy = _vacancyRepository.GetById(id);
            VacancyUpdateDTO vacancyUpdateDTO = _mapper.Map<VacancyUpdateDTO>(vacancy);
            return vacancyUpdateDTO;
        }

        public bool CheckExistence(VacancyCheckExistenceDTO vacancyCheckExistenceDTO)
        {
            throw new NotImplementedException();
        }

        public bool Insert(VacancyInsertDTO vacancyInsertDTO)
        {
            Vacancy vacancy = _mapper.Map<Vacancy>(vacancyInsertDTO);
            bool result = _vacancyRepository.Insert(vacancy);
            return result;
        }

        public bool Update(VacancyUpdateDTO vacancyUpdateDTO)
        {
            Vacancy vacancy = _mapper.Map<Vacancy>(vacancyUpdateDTO);
            bool result = _vacancyRepository.Update(vacancy);
            return result;
        }

        public bool Delete(DeleteEntity entity)
        {
            bool result = _vacancyRepository.Delete(entity);
            return result;
        }
    }
}
