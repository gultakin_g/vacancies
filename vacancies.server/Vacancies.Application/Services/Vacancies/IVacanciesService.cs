﻿using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Vacancies.DTOs;
using Vacancies.Domain.Models.Bases;

namespace Vacancies.Application.Services.Vacancies
{
    public interface IVacanciesService
    {
        (IEnumerable<VacancyDTO>, int) FindList(VacancySearchDTO vacancySearchDTO);
        IEnumerable<SelectItemDTO> GetList();
        VacancyUpdateDTO GetById(int id);
        bool CheckExistence(VacancyCheckExistenceDTO vacancyCheckExistenceDTO);
        bool Insert(VacancyInsertDTO vacancyInsertDTO);
        bool Update(VacancyUpdateDTO vacancyUpdateDTO);
        bool Delete(DeleteEntity entity);
    }
}
