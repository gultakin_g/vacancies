﻿using System;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Vacancies.DTOs
{
    public class VacancyUpdateDTO : IUpdateDTO
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Position { get; set; }
        public string CompanyName { get; set; }
        public string RelevantPerson { get; set; }
        public string CandidateRequirements { get; set; }
        public string AboutWork { get; set; }
        public DateTime ExpiryDate { get; set; }

        public int CategoryItemId { get; set; }
        public int CityId { get; set; }
        public int MinSalaryId { get; set; }
        public int MaxSalaryId { get; set; }
        public int MinAgeId { get; set; }
        public int MaxAgeId { get; set; }
        public int EducationId { get; set; }
        public int ExperienceId { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public IEnumerable<VacancyPhoneDTO> VacancyPhones { get; set; }
    }
}
