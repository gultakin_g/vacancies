﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vacancies.Application.Services.Vacancies.DTOs
{
    public class VacancyPhoneDTO
    {
        public int VacancyId { get; set; }
        public string PhoneNumber { get; set; }
    }
}
