﻿using System;
using Vacancies.Application.Services.Ages.DTOs;
using Vacancies.Application.Services.Categories.DTOs;
using Vacancies.Application.Services.Cities.DTOs;
using Vacancies.Application.Services.Educations.DTOs;
using Vacancies.Application.Services.Experiences.DTOs;
using Vacancies.Application.Services.Salaries.DTOs;

namespace Vacancies.Application.Services.Vacancies.DTOs
{
    public class VacancyDTO
    {
        public string Email { get; set; }
        public string Position { get; set; }
        public string CompanyName { get; set; }
        public string RelevantPerson { get; set; }
        public string CandidateRequirements { get; set; }
        public string AboutWork { get; set; }
        public DateTime ExpiryDate { get; set; }

        public CategoryItemDTO CategoryItem { get; set; }
        public CityDTO City { get; set; }
        public SalaryDTO MinSalary { get; set; }
        public SalaryDTO MaxSalary { get; set; }
        public AgeDTO MinAge { get; set; }
        public AgeDTO MaxAge { get; set; }
        public EducationDTO Education { get; set; }
        public ExperienceDTO Experience { get; set; }
    }
}
