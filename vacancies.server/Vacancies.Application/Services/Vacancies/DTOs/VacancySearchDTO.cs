﻿using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Ages.DTOs;
using Vacancies.Application.Services.Categories.DTOs;
using Vacancies.Application.Services.Cities.DTOs;
using Vacancies.Application.Services.Educations.DTOs;
using Vacancies.Application.Services.Experiences.DTOs;
using Vacancies.Application.Services.Salaries.DTOs;

namespace Vacancies.Application.Services.Vacancies.DTOs
{
    public class VacancySearchDTO : BaseSearchDTO
    {
        public string Email { get; set; }
        public string Position { get; set; }
        public string CompanyName { get; set; }
        public string RelevantPerson { get; set; }
        public string CandidateRequirements { get; set; }
        public string AboutWork { get; set; }
    }
}
