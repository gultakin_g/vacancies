﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vacancies.Application.Services.Cities.DTOs
{
    public class CityDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
