﻿namespace Vacancies.Application.Services.Cities.DTOs
{
    public class CityCheckExistenceDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
