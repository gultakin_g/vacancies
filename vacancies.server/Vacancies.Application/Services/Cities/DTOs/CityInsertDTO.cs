﻿using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Cities.DTOs
{
    public class CityInsertDTO :IInsertDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int CreatedBy { get; set; }
    }
}
