﻿using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Cities.DTOs
{
    public class CitySearchDTO : BaseSearchDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
