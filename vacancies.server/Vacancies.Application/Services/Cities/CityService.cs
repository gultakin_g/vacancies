﻿using AutoMapper;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Cities.DTOs;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Cities;
using Vacancies.Infrastructure.Repositories.Cities;

namespace Vacancies.Application.Services.Cities
{
    public class CityService : ICityService
    {
        private readonly ICityRepository _cityRepository;
        private readonly IMapper _mapper;
        public CityService(ICityRepository cityRepository, IMapper mapper)
        {
            _cityRepository = cityRepository;
            _mapper = mapper;
        }
        public (IEnumerable<CityDTO>, int) FindList(CitySearchDTO citySearchDTO)
        {
            CitySearch citySearch = _mapper.Map<CitySearch>(citySearchDTO);
            (IEnumerable<City> cities, int count) = _cityRepository.FindList(citySearch);
            IEnumerable<CityDTO> cityDTOs = _mapper.Map<IEnumerable<CityDTO>>(cities);
            return (cityDTOs, count);
        }

        public IEnumerable<SelectItemDTO> GetList()
        {
            IEnumerable<SelectItem> selectItems = _cityRepository.GetList();
            IEnumerable<SelectItemDTO> selectItemDTOs = _mapper.Map<IEnumerable<SelectItemDTO>>(selectItems);
            return selectItemDTOs;
        }

        public CityUpdateDTO GetById(int id)
        {
            City city = _cityRepository.GetById(id);
            CityUpdateDTO cityUpdateDTO = _mapper.Map<CityUpdateDTO>(city);
            return cityUpdateDTO;
        }

        public bool CheckExistence(CityCheckExistenceDTO cityCheckExistenceDTO)
        {
            City city = _mapper.Map<City>(cityCheckExistenceDTO);
            bool result = _cityRepository.CheckExistence(city);
            return result;
        }

        public bool Insert(CityInsertDTO cityInsertDTO)
        {
            City city = _mapper.Map<City>(cityInsertDTO);
            bool result = _cityRepository.Insert(city);
            return result;
        }

        public bool Update(CityUpdateDTO cityUpdateDTO)
        {
            City city = _mapper.Map<City>(cityUpdateDTO);
            bool result = _cityRepository.Update(city);
            return result;
        }

        public bool Delete(int id)
        {
            bool result = _cityRepository.Delete(id);
            return result;
        }
    }
}
