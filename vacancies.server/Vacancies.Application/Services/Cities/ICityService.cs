﻿using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Cities.DTOs;

namespace Vacancies.Application.Services.Cities
{
    public interface ICityService
    {
        (IEnumerable<CityDTO>, int) FindList(CitySearchDTO citySearchDTO);
        IEnumerable<SelectItemDTO> GetList();
        CityUpdateDTO GetById(int id);
        bool CheckExistence(CityCheckExistenceDTO cityCheckExistenceDTO);
        bool Insert(CityInsertDTO cityInsertDTO);
        bool Update(CityUpdateDTO cityUpdateDTO);
        bool Delete(int id);
    }
}
