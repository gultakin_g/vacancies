﻿using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Users.DTOs;

namespace Vacancies.Application.Services.Users
{
    public interface IUserService
    {
        #region Auth
        (UserUpdateDTO, string) Authenticate(UserAuthDTO authDTO);
        #endregion

        #region Users
        (IEnumerable<UserDTO>, int) FindList(UserSearchDTO userSearchDTO);
        IEnumerable<SelectItemDTO> GetList();
        UserUpdateDTO GetById(int id);
        bool CheckExistenceUserName(UserCheckExistenceUserNameDTO userCheckExistenceUserName);
        bool CheckExistenceEmail(UserCheckExistenceEmailDTO userCheckExistenceEmailDTO);
        bool Insert(UserInsertDTO userInsertDTO);
        bool Update(UserUpdateDTO userUpdateDTO);
        bool Delete(int id);
        #endregion
    }
}
