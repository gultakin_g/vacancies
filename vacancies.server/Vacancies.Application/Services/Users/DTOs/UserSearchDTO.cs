﻿using System;
using Vacancies.Application.BaseDTOs;
using Vacancies.Domain.Enums;

namespace Vacancies.Application.Services.Users.DTOs
{
    public class UserSearchDTO : BaseSearchDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public Gender? Gender { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
    }
}
