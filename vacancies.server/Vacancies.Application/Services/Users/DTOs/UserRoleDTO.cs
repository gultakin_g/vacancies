﻿namespace Vacancies.Application.Services.Users.DTOs
{
    public class UserRoleDTO
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
