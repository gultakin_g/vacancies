﻿namespace Vacancies.Application.Services.Users.DTOs
{
    public class UserAuthDTO
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
