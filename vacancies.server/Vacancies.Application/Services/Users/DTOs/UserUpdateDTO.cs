﻿using System;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Domain.Enums;
using Vacancies.Domain.Models.Users;

namespace Vacancies.Application.Services.Users.DTOs
{
    public class UserUpdateDTO : IUpdateDTO
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public Gender Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public IEnumerable<UserRoleDTO> UserRoles { get; set; }
    }
}
