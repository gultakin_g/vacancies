﻿namespace Vacancies.Application.Services.Users.DTOs
{
    public class UserCheckExistenceEmailDTO
    {
        public int Id { get; set; }
        public string Email { get; set; }
    }
}
