﻿namespace Vacancies.Application.Services.Users.DTOs
{
    public class UserCheckExistenceUserNameDTO
    {
        public int Id { get; set; }
        public string UserName { get; set; }
    }
}
