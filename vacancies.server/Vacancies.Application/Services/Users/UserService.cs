﻿using AutoMapper;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Users.DTOs;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Users;
using Vacancies.Infrastructure.Repositories.Users;

namespace Vacancies.Application.Services.Users
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        #region Auth
        public (UserUpdateDTO, string) Authenticate(UserAuthDTO authDTO)
        {
            (User user, string errorMessage) = _userRepository.Authenticate(authDTO.UserName, authDTO.Password);
            UserUpdateDTO userUpdateDTO = _mapper.Map<UserUpdateDTO>(user);
            return (userUpdateDTO, errorMessage);
        }
        #endregion

        #region Users

        public (IEnumerable<UserDTO>, int) FindList(UserSearchDTO userSearchDTO)
        {
            UserSearch userSearch = _mapper.Map<UserSearch>(userSearchDTO);
            (IEnumerable<User> users, int count) = _userRepository.FindList(userSearch);
            IEnumerable<UserDTO> userDTOs = _mapper.Map<IEnumerable<UserDTO>>(users);
            return (userDTOs, count);
        }

        public IEnumerable<SelectItemDTO> GetList()
        {
            IEnumerable<SelectItem> selectItems = _userRepository.GetList();
            IEnumerable<SelectItemDTO> selectItemDTOs = _mapper.Map<IEnumerable<SelectItemDTO>>(selectItems);
            return selectItemDTOs;
        }

        public UserUpdateDTO GetById(int id)
        {
            User user = _userRepository.GetById(id);
            UserUpdateDTO userUpdateDTO = _mapper.Map<UserUpdateDTO>(user);
            return userUpdateDTO;
        }
       
        public bool CheckExistenceUserName(UserCheckExistenceUserNameDTO userCheckExistenceUserName)
        {
            User user = _mapper.Map<User>(userCheckExistenceUserName);
            bool result = _userRepository.CheckExistenceUserName(user);
            return result;
        }

        public bool CheckExistenceEmail(UserCheckExistenceEmailDTO userCheckExistenceEmailDTO)
        {
            User user = _mapper.Map<User>(userCheckExistenceEmailDTO);
            bool result = _userRepository.CheckExistenceEmail(user);
            return result;
        }

        public bool Insert(UserInsertDTO userInsertDTO)
        {
            User user = _mapper.Map<User>(userInsertDTO);
            bool result = _userRepository.Insert(user);
            return result;
        }

        public bool Update(UserUpdateDTO userUpdateDTO)
        {
            User user = _mapper.Map<User>(userUpdateDTO);
            bool result = _userRepository.Update(user);
            return result;
        }

        public bool Delete(int id)
        {
            bool result = _userRepository.Delete(id);
            return result;
        }

        #endregion
    }
}
