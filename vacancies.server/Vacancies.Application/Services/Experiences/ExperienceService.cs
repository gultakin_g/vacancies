﻿using AutoMapper;
using System;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Experiences.DTOs;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Experiences;
using Vacancies.Infrastructure.Repositories.Experiences;

namespace Vacancies.Application.Services.Experiences
{
    public class ExperienceService : IExperienceService
    {
        private readonly IExperienceRepository _experienceRepository;
        private readonly IMapper _mapper;

        public ExperienceService(IExperienceRepository experienceRepository,
                                 IMapper mapper)
        {
            _experienceRepository = experienceRepository;
            _mapper = mapper;
        }
        public (IEnumerable<ExperienceDTO>, int) FindList(ExperienceSearchDTO experienceSearchDTO)
        {
            ExperienceSearch experienceSearch = _mapper.Map<ExperienceSearch>(experienceSearchDTO);
            (IEnumerable<Experience> experiences, int count) = _experienceRepository.FindList(experienceSearch);
            IEnumerable<ExperienceDTO> experienceDTOs = _mapper.Map<IEnumerable<ExperienceDTO>>(experiences);
            return (experienceDTOs, count);
        }

        public IEnumerable<SelectItemDTO> GetList()
        {
            IEnumerable<SelectItem> selectItems = _experienceRepository.GetList();
            IEnumerable<SelectItemDTO> selectItemDTOs = _mapper.Map<IEnumerable<SelectItemDTO>>(selectItems);
            return selectItemDTOs;
        }

        public ExperienceUpdateDTO GetById(int id)
        {
            Experience experience = _experienceRepository.GetById(id);
            ExperienceUpdateDTO experienceUpdateDTO = _mapper.Map<ExperienceUpdateDTO>(experience);
            return experienceUpdateDTO;
        }

        public bool CheckExistence(ExperienceCheckExistenceDTO experienceCheckExistenceDTO)
        {
            Experience experience = _mapper.Map<Experience>(experienceCheckExistenceDTO);
            bool result = _experienceRepository.CheckExistence(experience);
            return result;
        }

        public bool Insert(ExperienceInsertDTO experienceInsertDTO)
        {
            Experience experience = _mapper.Map<Experience>(experienceInsertDTO);
            bool result = _experienceRepository.Insert(experience);
            return result;
        }

        public bool Update(ExperienceUpdateDTO experienceUpdateDTO)
        {
            Experience experience = _mapper.Map<Experience>(experienceUpdateDTO);
            bool result = _experienceRepository.Update(experience);
            return result;
        }

        public bool Delete(int id)
        {
            bool result = _experienceRepository.Delete(id);
            return result;
        }
    }
}
