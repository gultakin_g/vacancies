﻿using System;
using System.Collections.Generic;
using System.Text;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Experiences.DTOs;

namespace Vacancies.Application.Services.Experiences
{
    public interface IExperienceService
    {
        (IEnumerable<ExperienceDTO>, int) FindList(ExperienceSearchDTO experienceSearchDTO);
        IEnumerable<SelectItemDTO> GetList();
        ExperienceUpdateDTO GetById(int id);
        bool CheckExistence(ExperienceCheckExistenceDTO experienceCheckExistenceDTO);
        bool Insert(ExperienceInsertDTO experienceInsertDTO);
        bool Update(ExperienceUpdateDTO experienceUpdateDTO);
        bool Delete(int id);
    }
}
