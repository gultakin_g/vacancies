﻿using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Experiences.DTOs
{
    public class ExperienceInsertDTO : IInsertDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int CreatedBy { get; set; }
    }
}
