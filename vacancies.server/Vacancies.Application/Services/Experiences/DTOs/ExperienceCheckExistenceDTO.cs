﻿namespace Vacancies.Application.Services.Experiences.DTOs
{
    public class ExperienceCheckExistenceDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
