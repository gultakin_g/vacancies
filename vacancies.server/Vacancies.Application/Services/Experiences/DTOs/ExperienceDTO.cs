﻿namespace Vacancies.Application.Services.Experiences.DTOs
{
    public class ExperienceDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
