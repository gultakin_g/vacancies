﻿using System;
using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Experiences.DTOs
{
    public class ExperienceUpdateDTO : IUpdateDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
