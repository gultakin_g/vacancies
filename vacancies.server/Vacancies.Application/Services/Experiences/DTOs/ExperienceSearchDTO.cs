﻿using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Experiences.DTOs
{
    public class ExperienceSearchDTO : BaseSearchDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
