﻿using AutoMapper;
using System;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Educations.DTOs;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Educations;
using Vacancies.Infrastructure.Repositories.Educations;

namespace Vacancies.Application.Services.Educations
{
    public class EducationService : IEducationService
    {
        private readonly IEducationRepository _educationRepository;
        private readonly IMapper _mapper;

        public EducationService(IEducationRepository  educationRepository, IMapper mapper)
        {
            _educationRepository = educationRepository;
            _mapper = mapper;
        }
        public (IEnumerable<EducationDTO>, int) FindList(EducationSearchDTO educationSearchDTO)
        {
            EducationSearch educationSearch = _mapper.Map<EducationSearch>(educationSearchDTO);
            (IEnumerable<Education> educations, int count) = _educationRepository.FindList(educationSearch);
            IEnumerable<EducationDTO> educationDTOs = _mapper.Map<IEnumerable<EducationDTO>>(educations);
            return (educationDTOs, count);
        }

        public IEnumerable<SelectItemDTO> GetList()
        {
            IEnumerable<SelectItem> selectItems = _educationRepository.GetList();
            IEnumerable<SelectItemDTO> selectItemDTOs = _mapper.Map<IEnumerable<SelectItemDTO>>(selectItems);
            return selectItemDTOs;
        }

        public EducationUpdateDTO GetById(int id)
        {
            Education education = _educationRepository.GetById(id);
            EducationUpdateDTO educationUpdateDTO = _mapper.Map<EducationUpdateDTO>(education);
            return educationUpdateDTO;
        }

        public bool CheckExistence(EducationCheckExistenceDTO educationCheckExistenceDTO)
        {
            Education education = _mapper.Map<Education>(educationCheckExistenceDTO);
            bool result = _educationRepository.CheckExistence(education);
            return result;
        }

        public bool Insert(EducationInsertDTO educationInsertDTO)
        {
            Education education = _mapper.Map<Education>(educationInsertDTO);
            bool result = _educationRepository.Insert(education);
            return result;
        }

        public bool Update(EducationUpdateDTO educationUpdateDTO)
        {
            Education education = _mapper.Map<Education>(educationUpdateDTO);
            bool result = _educationRepository.Update(education);
            return result;
        }
        public bool Delete(int id)
        {
            bool result = _educationRepository.Delete(id);
            return result;
        }
    }
}
