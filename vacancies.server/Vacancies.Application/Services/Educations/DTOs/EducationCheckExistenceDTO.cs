﻿namespace Vacancies.Application.Services.Educations.DTOs
{
    public class EducationCheckExistenceDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
