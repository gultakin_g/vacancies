﻿using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Educations.DTOs
{
    public class EducationSearchDTO : BaseSearchDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
