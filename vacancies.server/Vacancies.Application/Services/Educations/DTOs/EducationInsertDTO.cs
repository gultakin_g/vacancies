﻿using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Educations.DTOs
{
    public class EducationInsertDTO : IInsertDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int CreatedBy { get; set; }
    }
}
