﻿using System;
using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Educations.DTOs
{
    public class EducationUpdateDTO : IUpdateDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
