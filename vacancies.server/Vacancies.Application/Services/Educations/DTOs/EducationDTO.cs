﻿namespace Vacancies.Application.Services.Educations.DTOs
{
    public class EducationDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
