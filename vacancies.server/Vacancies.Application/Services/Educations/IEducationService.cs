﻿using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Educations.DTOs;

namespace Vacancies.Application.Services.Educations
{
    public interface IEducationService
    {
        (IEnumerable<EducationDTO>, int) FindList(EducationSearchDTO educationSearchDTO);
        IEnumerable<SelectItemDTO> GetList();
        EducationUpdateDTO GetById(int id);
        bool CheckExistence(EducationCheckExistenceDTO educationCheckExistenceDTO);
        bool Insert(EducationInsertDTO educationInsertDTO);
        bool Update(EducationUpdateDTO educationUpdateDTO);
        bool Delete(int id);
    }
}
