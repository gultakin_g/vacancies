﻿using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Permissions.DTOs;

namespace Vacancies.Application.Services.Permissions
{
    public interface IPermissionService
    {
        (IEnumerable<PermissionDTO>, int) FindList(PermissionSearchDTO permissionSearchDTO);
        IEnumerable<SelectItemDTO> GetList();
        PermissionUpdateDTO GetById(int id);
        bool CheckExistence(PermissionCheckExistenceDTO permissionCheckExistenceDTO);
        bool Insert(PermissionInsertDTO permissionInsertDTO);
        bool Update(PermissionUpdateDTO permissionUpdateDTO);
        bool Delete(int id);
    }
}
