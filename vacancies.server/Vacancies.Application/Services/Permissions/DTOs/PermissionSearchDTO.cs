﻿using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Permissions.DTOs
{
    public class PermissionSearchDTO : BaseSearchDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
