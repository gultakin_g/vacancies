﻿namespace Vacancies.Application.Services.Permissions.DTOs
{
    public class PermissionCheckExistenceDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
