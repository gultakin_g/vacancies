﻿using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Permissions.DTOs
{
    public class PermissionInsertDTO : IInsertDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int CreatedBy { get; set; }
    }
}
