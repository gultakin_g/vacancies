﻿using System;
using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Permissions.DTOs
{
    public class PermissionUpdateDTO : IUpdateDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
