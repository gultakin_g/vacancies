﻿using AutoMapper;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Permissions.DTOs;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Permissions;
using Vacancies.Infrastructure.Repositories.Permissions;

namespace Vacancies.Application.Services.Permissions
{
    public class PermissionService : IPermissionService
    {
        private readonly IPermissionRepository _permissionRepository;
        private readonly IMapper _mapper;
        public PermissionService(
            IPermissionRepository permissionRepository,
            IMapper mapper)
        {
            _permissionRepository = permissionRepository;
            _mapper = mapper;
        }

        public (IEnumerable<PermissionDTO>, int) FindList(PermissionSearchDTO permissionSearchDTO)
        {
            PermissionSearch permissionSearch = _mapper.Map<PermissionSearch>(permissionSearchDTO);
            (IEnumerable<Permission> permissions, int count) = _permissionRepository.FindList(permissionSearch);
            IEnumerable<PermissionDTO> permissionDTOs = _mapper.Map<IEnumerable<PermissionDTO>>(permissions);
            return (permissionDTOs, count);
        }

        public IEnumerable<SelectItemDTO> GetList()
        {
            IEnumerable<SelectItem> selectItems = _permissionRepository.GetList();
            IEnumerable<SelectItemDTO> selectItemDTOs = _mapper.Map<IEnumerable<SelectItemDTO>>(selectItems);
            return selectItemDTOs;
        }

        public PermissionUpdateDTO GetById(int id)
        {
            Permission permission = _permissionRepository.GetById(id);
            PermissionUpdateDTO permissionUpdateDTO = _mapper.Map<PermissionUpdateDTO>(permission);
            return permissionUpdateDTO;
        }

        public bool CheckExistence(PermissionCheckExistenceDTO permissionCheckExistenceDTO)
        {
            Permission permission = _mapper.Map<Permission>(permissionCheckExistenceDTO);
            bool result = _permissionRepository.CheckExistence(permission);
            return result;
        }

        public bool Insert(PermissionInsertDTO permissionInsertDTO)
        {
            Permission permission = _mapper.Map<Permission>(permissionInsertDTO);
            bool result = _permissionRepository.Insert(permission);
            return result;
        }

        public bool Update(PermissionUpdateDTO permissionUpdateDTO)
        {
            Permission permission = _mapper.Map<Permission>(permissionUpdateDTO);
            bool result = _permissionRepository.Update(permission);
            return result;
        }

        public bool Delete(int id)
        {
            bool result = _permissionRepository.Delete(id);
            return result;
        }
    }
}
