﻿using AutoMapper;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Categories.DTOs;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Categories;
using Vacancies.Infrastructure.Repositories.Categories;

namespace Vacancies.Application.Services.Categories
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IMapper _mapper;

        public CategoryService(
            ICategoryRepository categoryRepository,
            IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }

        public (IEnumerable<CategoryDTO>, int) FindList(CategorySearchDTO categorySearchDTO)
        {
            CategorySearch categorySearch = _mapper.Map<CategorySearch>(categorySearchDTO);
            (IEnumerable<Category> categories, int count) = _categoryRepository.FindList(categorySearch);
            IEnumerable<CategoryDTO> categoryDTOs = _mapper.Map<IEnumerable<CategoryDTO>>(categories);
            return (categoryDTOs, count);
        }

        public IEnumerable<SelectItemDTO> GetList()
        {
            IEnumerable<SelectItem> selectItems = _categoryRepository.GetList();
            IEnumerable<SelectItemDTO> selectItemDTOs = _mapper.Map<IEnumerable<SelectItemDTO>>(selectItems);
            return selectItemDTOs;
        }

        public CategoryUpdateDTO GetById(int id)
        {
            Category category = _categoryRepository.GetById(id);
            CategoryUpdateDTO categoryUpdateDTO = _mapper.Map<CategoryUpdateDTO>(category);
            return categoryUpdateDTO;
        }

        public bool CheckExistence(CategoryCheckExistenceDTO categoryCheckExistenceDTO)
        {
            Category category = _mapper.Map<Category>(categoryCheckExistenceDTO);
            bool result = _categoryRepository.CheckExistence(category);
            return result;
        }

        public bool Insert(CategoryInsertDTO categoryInsertDTO)
        {
            Category category = _mapper.Map<Category>(categoryInsertDTO);
            bool result = _categoryRepository.Insert(category);
            return result;
        }

        public bool Update(CategoryUpdateDTO categoryUpdateDTO)
        {
            Category category = _mapper.Map<Category>(categoryUpdateDTO);
            bool result = _categoryRepository.Update(category);
            return result;
        }

        public bool Delete(int id)
        {
            bool result = _categoryRepository.Delete(id);
            return result;
        }
    }
}
