﻿using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Categories.DTOs;

namespace Vacancies.Application.Services.Categories
{
    public interface ICategoryService
    {
        (IEnumerable<CategoryDTO>, int) FindList(CategorySearchDTO categorySearchDTO);
        IEnumerable<SelectItemDTO> GetList();
        CategoryUpdateDTO GetById(int id);
        bool CheckExistence(CategoryCheckExistenceDTO categoryCheckExistenceDTO);
        bool Insert(CategoryInsertDTO categoryInsertDTO);
        bool Update(CategoryUpdateDTO categoryUpdateDTO);
        bool Delete(int id);
    }
}
