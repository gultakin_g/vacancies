﻿using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Categories.DTOs
{
    public class CategorySearchDTO : BaseSearchDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
