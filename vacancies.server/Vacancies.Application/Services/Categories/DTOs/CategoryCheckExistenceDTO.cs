﻿namespace Vacancies.Application.Services.Categories.DTOs
{
    public class CategoryCheckExistenceDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
