﻿using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Categories.DTOs
{
    public class CategoryInsertDTO : IInsertDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int CreatedBy { get; set; }
        public IEnumerable<CategoryItemDTO> CategoryItems { get; set; }
    }
}
