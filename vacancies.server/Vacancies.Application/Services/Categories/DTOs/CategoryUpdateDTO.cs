﻿using System;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Categories.DTOs
{
    public class CategoryUpdateDTO : IUpdateDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public IEnumerable<CategoryItemDTO> CategoryItems { get; set; }
    }
}
