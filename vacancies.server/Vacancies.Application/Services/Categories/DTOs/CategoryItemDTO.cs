﻿namespace Vacancies.Application.Services.Categories.DTOs
{
    public class CategoryItemDTO
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
