﻿using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Salaries.DTOs;

namespace Vacancies.Application.Services.Salaries
{
    public interface ISalaryService
    {
        (IEnumerable<SalaryDTO>, int) FindList(SalarySearchDTO salarySearchDTO);
        IEnumerable<SelectItemDTO> GetList();
        SalaryUpdateDTO GetById(int id);
        bool CheckExistence(SalaryCheckExistenceDTO salaryCheckExistenceDTO);
        bool Insert(SalaryInsertDTO salaryInsertDTO);
        bool Update(SalaryUpdateDTO salaryUpdateDTO);
        bool Delete(int id);
    }
}
