﻿using AutoMapper;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Salaries.DTOs;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Salaries;
using Vacancies.Infrastructure.Repositories.Salaries;

namespace Vacancies.Application.Services.Salaries
{
    public class SalaryService : ISalaryService
    {
        private readonly ISalaryRepository _salaryRepository;
        private readonly IMapper _mapper;

        public SalaryService(ISalaryRepository salaryRepository, IMapper mapper)
        {
            _salaryRepository = salaryRepository;
            _mapper = mapper;
        }
        public (IEnumerable<SalaryDTO>, int) FindList(SalarySearchDTO salarySearchDTO)
        {
            SalarySearch salarySearch = _mapper.Map<SalarySearch>(salarySearchDTO);
            (IEnumerable<Salary> salaries, int count) = _salaryRepository.FindList(salarySearch);
            IEnumerable<SalaryDTO> salaryDTOs = _mapper.Map<IEnumerable<SalaryDTO>>(salaries);
            return (salaryDTOs, count);
        }

        public IEnumerable<SelectItemDTO> GetList()
        {
            IEnumerable<SelectItem> selectItems = _salaryRepository.GetList();
            IEnumerable<SelectItemDTO> selectItemDTOs = _mapper.Map<IEnumerable<SelectItemDTO>>(selectItems);
            return selectItemDTOs;
        }

        public SalaryUpdateDTO GetById(int id)
        {
            Salary salary = _salaryRepository.GetById(id);
            SalaryUpdateDTO salaryUpdateDTO = _mapper.Map<SalaryUpdateDTO>(salary);
            return salaryUpdateDTO;
        }

        public bool CheckExistence(SalaryCheckExistenceDTO salaryCheckExistenceDTO)
        {
            Salary salary = _mapper.Map<Salary>(salaryCheckExistenceDTO);
            bool result = _salaryRepository.CheckExistence(salary);
            return result;
        }

        public bool Insert(SalaryInsertDTO salaryInsertDTO)
        {
            Salary salary = _mapper.Map<Salary>(salaryInsertDTO);
            bool result = _salaryRepository.Insert(salary);
            return result;
        }

        public bool Update(SalaryUpdateDTO salaryUpdateDTO)
        {
            Salary salary = _mapper.Map<Salary>(salaryUpdateDTO);
            bool result = _salaryRepository.Update(salary);
            return result;
        }

        public bool Delete(int id)
        {
            bool result = _salaryRepository.Delete(id);
            return result;
        }
    }
}
