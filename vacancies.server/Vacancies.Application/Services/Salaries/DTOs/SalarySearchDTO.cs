﻿using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Salaries.DTOs
{
    public class SalarySearchDTO : BaseSearchDTO
    {
        public double? Amount { get; set; }
        public string Description { get; set; }
    }
}
