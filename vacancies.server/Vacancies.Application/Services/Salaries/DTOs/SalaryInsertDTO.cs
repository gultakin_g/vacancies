﻿using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Salaries.DTOs
{
    public class SalaryInsertDTO : IInsertDTO
    {
        public double Amount { get; set; }
        public string Description { get; set; }
        public int CreatedBy { get; set; }
    }
}
