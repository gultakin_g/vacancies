﻿using System;
using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Salaries.DTOs
{
    public class SalaryUpdateDTO :IUpdateDTO
    {
        public int Id { get; set; }
        public double Amount { get; set; }
        public string Description { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
