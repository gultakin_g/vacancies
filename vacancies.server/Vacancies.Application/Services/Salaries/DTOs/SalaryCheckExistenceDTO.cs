﻿namespace Vacancies.Application.Services.Salaries.DTOs
{
    public class SalaryCheckExistenceDTO
    {
        public string Id { get; set; }
        public double Amount { get; set; }
    }
}
