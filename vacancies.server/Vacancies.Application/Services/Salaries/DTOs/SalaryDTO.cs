﻿namespace Vacancies.Application.Services.Salaries.DTOs
{
    public class SalaryDTO
    {
        public int Id { get; set; }
        public double Amount { get; set; }
        public string Description { get; set; }
    }
}
