﻿using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Ages.DTOs
{
    public class AgeInsertDTO : IInsertDTO
    {
        public int Amount { get; set; }
        public string Description { get; set; }
        public int CreatedBy { get; set; }
    }
}
