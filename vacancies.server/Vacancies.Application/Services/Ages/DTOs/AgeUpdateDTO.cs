﻿using System;
using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Ages.DTOs
{
    public class AgeUpdateDTO : IUpdateDTO
    {
        public int Id { get; set; }
        public int Amount { get; set; }
        public string Description { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
