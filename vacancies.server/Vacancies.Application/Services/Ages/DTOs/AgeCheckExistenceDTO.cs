﻿namespace Vacancies.Application.Services.Ages.DTOs
{
    public class AgeCheckExistenceDTO
    {
        public int Id { get; set; }
        public int Amount { get; set; }
    }
}
