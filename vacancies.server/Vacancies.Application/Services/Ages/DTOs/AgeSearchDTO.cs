﻿using Vacancies.Application.BaseDTOs;

namespace Vacancies.Application.Services.Ages.DTOs
{
    public class AgeSearchDTO : BaseSearchDTO
    {
        public int? Amount { get; set; }
        public string Description { get; set; }
    }
}
