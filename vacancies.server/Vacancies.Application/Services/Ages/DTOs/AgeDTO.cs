﻿namespace Vacancies.Application.Services.Ages.DTOs
{
    public class AgeDTO
    {
        public int Id { get; set; }
        public int Amount { get; set; }
        public string Description { get; set; }
    }
}
