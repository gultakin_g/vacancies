﻿using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Ages.DTOs;

namespace Vacancies.Application.Services.Ages
{
    public interface IAgeService
    {
        (IEnumerable<AgeDTO>, int) FindList(AgeSearchDTO ageSearchDTO);
        IEnumerable<SelectItemDTO> GetList();
        AgeUpdateDTO GetById(int id);
        bool CheckExistence(AgeCheckExistenceDTO ageCheckExistenceDTO);
        bool Insert(AgeInsertDTO ageInsertDTO);
        bool Update(AgeUpdateDTO ageUpdateDTO);
        bool Delete(int id);
    }
}
