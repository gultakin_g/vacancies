﻿using AutoMapper;
using System.Collections.Generic;
using Vacancies.Application.BaseDTOs;
using Vacancies.Application.Services.Ages.DTOs;
using Vacancies.Domain.Models.Ages;
using Vacancies.Domain.Models.Bases;
using Vacancies.Infrastructure.Repositories.Ages;

namespace Vacancies.Application.Services.Ages
{
    public class AgeService : IAgeService
    {
        private readonly IAgeRepository _ageRepository;
        private readonly IMapper _mapper;

        public AgeService(IAgeRepository ageRepository, IMapper mapper)
        {
            _ageRepository = ageRepository;
            _mapper = mapper;
        }

        public (IEnumerable<AgeDTO>, int) FindList(AgeSearchDTO ageSearchDTO)
        {
            AgeSearch ageSearch = _mapper.Map<AgeSearch>(ageSearchDTO);
            (IEnumerable<Age> ages, int count) = _ageRepository.FindList(ageSearch);
            IEnumerable<AgeDTO> ageDTOs = _mapper.Map<IEnumerable<AgeDTO>>(ages);
            return (ageDTOs, count);
        }

        public IEnumerable<SelectItemDTO> GetList()
        {
            IEnumerable<SelectItem> selectItems = _ageRepository.GetList();
            IEnumerable<SelectItemDTO> selectItemDTOs = _mapper.Map<IEnumerable<SelectItemDTO>>(selectItems);
            return selectItemDTOs;
        }

        public AgeUpdateDTO GetById(int id)
        {
            Age age = _ageRepository.GetById(id);
            AgeUpdateDTO ageUpdateDTO = _mapper.Map<AgeUpdateDTO>(age);
            return ageUpdateDTO;
        }

        public bool CheckExistence(AgeCheckExistenceDTO ageCheckExistenceDTO)
        {
            Age age = _mapper.Map<Age>(ageCheckExistenceDTO);
            bool result = _ageRepository.CheckExistence(age);
            return result;
        }

        public bool Insert(AgeInsertDTO ageInsertDTO)
        {
            Age age = _mapper.Map<Age>(ageInsertDTO);
            bool result = _ageRepository.Insert(age);
            return result;
        }

        public bool Update(AgeUpdateDTO ageUpdateDTO)
        {
            Age age = _mapper.Map<Age>(ageUpdateDTO);
            bool result = _ageRepository.Update(age);
            return result;
        }

        public bool Delete(int id)
        {
            bool result = _ageRepository.Delete(id);
            return result;
        }
    }
}
