﻿using System;
using System.Text.Json.Serialization;

namespace Vacancies.Application.BaseDTOs
{
    public interface IUpdateDTO
    {
        public int Id { get; set; }

        [JsonIgnore]
        public int UpdatedBy { get; set; }

        [JsonIgnore]
        public DateTime UpdatedDate { get; set; }
    }
}
