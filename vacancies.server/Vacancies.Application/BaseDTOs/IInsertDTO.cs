﻿using System.Text.Json.Serialization;

namespace Vacancies.Application.BaseDTOs
{
    public interface IInsertDTO
    {
        [JsonIgnore]
        int CreatedBy { get; set; }
    }
}
