﻿namespace Vacancies.Application.BaseDTOs
{
    public abstract class BaseSearchDTO
    {
        public int CurrentPage { get; set; } = 1;
        public int PerPage { get; set; } = 5;
        public string SortBy { get; set; } = "ID";
        public string SortDirection { get; set; } = "ASC";
    }
}
