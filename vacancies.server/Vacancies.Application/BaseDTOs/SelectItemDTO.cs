﻿namespace Vacancies.Application.BaseDTOs
{
    public class SelectItemDTO
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
