﻿using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace Vacancies.Infrastructure.Handlers
{
    public class DbHandler : IDbHandler
    {
        private readonly IConfiguration _configuration;

        public DbHandler(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public SqlConnection GetConnection()
        {
            string connectionString = _configuration.GetConnectionString("DefaultConnection");
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            return sqlConnection;
        }
    }
}
