﻿using System.Data.SqlClient;

namespace Vacancies.Infrastructure.Handlers
{
    public interface IDbHandler
    {
        SqlConnection GetConnection();
    }
}
