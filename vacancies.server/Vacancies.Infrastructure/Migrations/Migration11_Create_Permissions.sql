﻿CREATE TABLE dbo.PERMISSIONS(
	ID INT IDENTITY(1,1) NOT NULL,
	NAME NVARCHAR(200) NOT NULL,
	DESCRIPTION NVARCHAR(1000) NULL,
	CREATED_BY INT NOT NULL,
	CREATED_DATE DATETIME DEFAULT GETDATE() NOT NULL,
	UPDATED_BY INT,
	UPDATED_DATE DATETIME,
	CONSTRAINT PK_PERMISSIONS PRIMARY KEY(ID),
	CONSTRAINT UK_PERMISSIONS UNIQUE(NAME),
	CONSTRAINT FK_PERMISSIONS_CREATED_BY FOREIGN KEY(CREATED_BY) REFERENCES dbo.USERS(ID),
	CONSTRAINT FK_PERMISSIONS_UPDATED_BY FOREIGN KEY(UPDATED_BY) REFERENCES dbo.USERS(ID),
)