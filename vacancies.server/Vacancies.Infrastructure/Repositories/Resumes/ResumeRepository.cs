﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Resumes;
using Vacancies.Infrastructure.Handlers;

namespace Vacancies.Infrastructure.Repositories.Resumes
{
    public class ResumeRepository : IResumeRepository
    {
        private readonly IDbHandler _dbHandler;

        public ResumeRepository(IDbHandler dbHandler)
        {
            _dbHandler = dbHandler;
        }

        #region Resumes

        #region FindList
        public (IEnumerable<Resume>, int) FindList(ResumeSearch resumeSearch)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                IEnumerable<Resume> resumes = GetResumesList(connection, resumeSearch);
                int resumesCount = GetResumesCount(connection, resumeSearch);
                return (resumes, resumesCount);
            }
        }

        private IEnumerable<Resume> GetResumesList(
            SqlConnection connection,
            ResumeSearch resumeSearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            #region filterBy
            if (!string.IsNullOrEmpty(resumeSearch.FirstName))
            {
                filterQuery.Append(" AND UPPER(R.FIRST_NAME) LIKE UPPER('%' + @FIRST_NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@FIRST_NAME", resumeSearch.FirstName);
            }

            if (!string.IsNullOrEmpty(resumeSearch.LastName))
            {
                filterQuery.Append(" AND UPPER(R.LAST_NAME) LIKE UPPER('%' + @LAST_NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@LAST_NAME", resumeSearch.LastName);
            }

            if (!string.IsNullOrEmpty(resumeSearch.MiddleName))
            {
                filterQuery.Append(" AND UPPER(R.MIDDLE_NAME) LIKE UPPER('%' + @MIDDLE_NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@MIDDLE_NAME", resumeSearch.MiddleName);
            }

            if (resumeSearch.Gender.HasValue)
            {
                filterQuery.Append(" AND GENDER = @GENDER");
                sqlCommand.Parameters.AddWithValue("@GENDER", resumeSearch.Gender);
            }

            if (!string.IsNullOrEmpty(resumeSearch.AboutExperience))
            {
                filterQuery.Append(" AND UPPER(R.ABOUT_EXPERIENCE) LIKE UPPER('%' + @ABOUT_EXPERIENCE + '%')");
                sqlCommand.Parameters.AddWithValue("@ABOUT_EXPERIENCE", resumeSearch.AboutExperience);
            }

            if (!string.IsNullOrEmpty(resumeSearch.AboutEducation))
            {
                filterQuery.Append(" AND UPPER(R.ABOUT_EDUCATION) LIKE UPPER('%' + @ABOUT_EDUCATION + '%')");
                sqlCommand.Parameters.AddWithValue("@ABOUT_EDUCATION", resumeSearch.AboutEducation);
            }

            if (!string.IsNullOrEmpty(resumeSearch.MoreInformation))
            {
                filterQuery.Append(" AND UPPER(R.MORE_INFORMATION) LIKE UPPER('%' + @MORE_INFORMATION + '%')");
                sqlCommand.Parameters.AddWithValue("@MORE_INFORMATION", resumeSearch.MoreInformation);
            }

            if (!string.IsNullOrEmpty(resumeSearch.Position))
            {
                filterQuery.Append(" AND UPPER(R.POSITION) LIKE UPPER('%' + @POSITION + '%')");
                sqlCommand.Parameters.AddWithValue("@POSITION", resumeSearch.Position);
            }

            if (!string.IsNullOrEmpty(resumeSearch.Skills))
            {
                filterQuery.Append(" AND UPPER(R.SKILLS) LIKE UPPER('%' + @SKILLS + '%')");
                sqlCommand.Parameters.AddWithValue("@SKILLS", resumeSearch.Skills);
            }

            if (!string.IsNullOrEmpty(resumeSearch.Email))
            {
                filterQuery.Append(" AND UPPER(R.EMAIL) LIKE UPPER('%' + @EMAIL + '%')");
                sqlCommand.Parameters.AddWithValue("@EMAIL", resumeSearch.Email);
            }

            if (resumeSearch.CategoryItemId.HasValue)
            {
                filterQuery.Append(" AND R.CATEGORY_ITEM_ID = @CATEGORY_ITEM_ID");
                sqlCommand.Parameters.AddWithValue("@CATEGORY_ITEM_ID", resumeSearch.CategoryItemId);
            }

            if (resumeSearch.CityId.HasValue)
            {
                filterQuery.Append(" AND R.CITY_ID = @CITY_ID");
                sqlCommand.Parameters.AddWithValue("@CITY_ID", resumeSearch.CityId);
            }

            if (resumeSearch.SalaryId.HasValue)
            {
                filterQuery.Append(" AND R.SALARY_ID = @SALARY_ID");
                sqlCommand.Parameters.AddWithValue("@SALARY_ID", resumeSearch.SalaryId);
            }

            if (resumeSearch.AgeId.HasValue)
            {
                filterQuery.Append(" AND R.AGE_ID = @AGE_ID");
                sqlCommand.Parameters.AddWithValue("@AGE_ID", resumeSearch.AgeId);
            }

            if (resumeSearch.ExperienceId.HasValue)
            {
                filterQuery.Append(" AND R.EXPERIENCE_ID = @EXPERIENCE_ID");
                sqlCommand.Parameters.AddWithValue("@EXPERIENCE_ID", resumeSearch.ExperienceId);
            }

            if (resumeSearch.EducationId.HasValue)
            {
                filterQuery.Append(" AND R.EDUCATION_ID = @EDUCATION_ID");
                sqlCommand.Parameters.AddWithValue("@EDUCATION_ID", resumeSearch.EducationId);
            }
            #endregion

            StringBuilder orderBy = new StringBuilder();
            switch (resumeSearch.SortBy.Trim().ToUpper())
            {
                case "FIRST_NAME":
                    orderBy.Append(" ORDER BY R.FIRST_NAME");
                    break;

                case "LAST_NAME":
                    orderBy.Append(" ORDER BY R.LAST_NAME");
                    break;

                case "MIDDLE_NAME":
                    orderBy.Append(" ORDER BY R.MIDDLE_NAME");
                    break;

                case "ABOUT_EXPERIENCE":
                    orderBy.Append(" ORDER BY R.ABOUT_EXPERIENCE");
                    break;

                case "ABOUT_EDUCATION":
                    orderBy.Append(" ORDER BY R.ABOUT_EDUCATION");
                    break;

                case "MORE_INFORMATION":
                    orderBy.Append(" ORDER BY R.MORE_INFORMATION");
                    break;

                case "POSITION":
                    orderBy.Append(" ORDER BY R.POSITION");
                    break;

                case "SKILLS":
                    orderBy.Append(" ORDER BY R.SKILLS");
                    break;

                case "EMAIL":
                    orderBy.Append(" ORDER BY R.EMAIL");
                    break;

                default:
                    orderBy.Append(" ORDER BY R.ID");
                    break;
            }

            string sortDirection = resumeSearch.SortDirection.Trim().ToUpper() == " DESC" ? " DESC" : " ASC";
            orderBy.Append(sortDirection);

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    R.ID,
                    R.FIRST_NAME,
                    R.LAST_NAME,
                    R.MIDDLE_NAME,
                    R.GENDER,
                    R.ABOUT_EXPERIENCE,
                    R.ABOUT_EDUCATION,
                    R.MORE_INFORMATION,
                    R.POSITION,
                    R.SKILLS,
                    R.EMAIL,
                    R.EXPIRY_DATE,
                    R.CATEGORY_ITEM_ID,
                    CI.NAME CATEGORY_ITEM_NAME,
                    R.CITY_ID,
                    C.NAME CITY_NAME,
                    R.SALARY_ID,
                    S.AMOUNT SALARY_AMOUNT,
                    R.AGE_ID,
                    A.AMOUNT AGE_AMOUNT,
                    R.EXPERIENCE_ID,
                    E.NAME EXPERIENCE_NAME,
                    R.EDUCATION_ID,
                    EDU.NAME EDUCATION_NAME
                FROM RESUMES R
                INNER JOIN CATEGORY_ITEMS CI
                ON CI.ID = R.CATEGORY_ITEM_ID
                INNER JOIN CITIES C
                ON C.ID = R.CITY_ID
                INNER JOIN SALARIES S
                ON S.ID = R.SALARY_ID
                INNER JOIN AGES A
                ON A.ID = R.AGE_ID
                INNER JOIN EXPERIENCES E
                ON E.ID = R.EXPERIENCE_ID
                INNER JOIN EDUCATIONS EDU
                ON EDU.ID = R.EDUCATION_ID
                WHERE R.IS_DELETED = 0
            ");

            query.Append(filterQuery);
            query.Append(orderBy);

            query.Append(" OFFSET @OFFSET_ROWS_COUNT ROWS FETCH NEXT @FETCH_ROWS_COUNT ROWS ONLY");
            sqlCommand.Parameters.AddWithValue("@OFFSET_ROWS_COUNT", (resumeSearch.CurrentPage - 1) * resumeSearch.PerPage);
            sqlCommand.Parameters.AddWithValue("@FETCH_ROWS_COUNT", resumeSearch.PerPage);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            DataRowCollection dataRowCollection = dataTable.Rows;
            IList<Resume> resumes = new List<Resume>();
            foreach (DataRow dataRow in dataRowCollection)
            {
                Resume resume = new Resume(dataRow);
                resumes.Add(resume);
            }
            return resumes;
        }

        private int GetResumesCount(
            SqlConnection connection,
            ResumeSearch resumeSearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            #region filterBy
            if (!string.IsNullOrEmpty(resumeSearch.FirstName))
            {
                filterQuery.Append(" AND UPPER(R.FIRST_NAME) LIKE UPPER('%' + @FIRST_NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@FIRST_NAME", resumeSearch.FirstName);
            }

            if (!string.IsNullOrEmpty(resumeSearch.LastName))
            {
                filterQuery.Append(" AND UPPER(R.LAST_NAME) LIKE UPPER('%' + @LAST_NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@LAST_NAME", resumeSearch.LastName);
            }

            if (!string.IsNullOrEmpty(resumeSearch.MiddleName))
            {
                filterQuery.Append(" AND UPPER(R.MIDDLE_NAME) LIKE UPPER('%' + @MIDDLE_NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@MIDDLE_NAME", resumeSearch.MiddleName);
            }

            if (resumeSearch.Gender.HasValue)
            {
                filterQuery.Append(" AND GENDER = @GENDER");
                sqlCommand.Parameters.AddWithValue("@GENDER", resumeSearch.Gender);
            }

            if (!string.IsNullOrEmpty(resumeSearch.AboutExperience))
            {
                filterQuery.Append(" AND UPPER(R.ABOUT_EXPERIENCE) LIKE UPPER('%' + @ABOUT_EXPERIENCE + '%')");
                sqlCommand.Parameters.AddWithValue("@ABOUT_EXPERIENCE", resumeSearch.AboutExperience);
            }

            if (!string.IsNullOrEmpty(resumeSearch.AboutEducation))
            {
                filterQuery.Append(" AND UPPER(R.ABOUT_EDUCATION) LIKE UPPER('%' + @ABOUT_EDUCATION + '%')");
                sqlCommand.Parameters.AddWithValue("@ABOUT_EDUCATION", resumeSearch.AboutEducation);
            }

            if (!string.IsNullOrEmpty(resumeSearch.MoreInformation))
            {
                filterQuery.Append(" AND UPPER(R.MORE_INFORMATION) LIKE UPPER('%' + @MORE_INFORMATION + '%')");
                sqlCommand.Parameters.AddWithValue("@MORE_INFORMATION", resumeSearch.MoreInformation);
            }

            if (!string.IsNullOrEmpty(resumeSearch.Position))
            {
                filterQuery.Append(" AND UPPER(R.POSITION) LIKE UPPER('%' + @POSITION + '%')");
                sqlCommand.Parameters.AddWithValue("@POSITION", resumeSearch.Position);
            }

            if (!string.IsNullOrEmpty(resumeSearch.Skills))
            {
                filterQuery.Append(" AND UPPER(R.SKILLS) LIKE UPPER('%' + @SKILLS + '%')");
                sqlCommand.Parameters.AddWithValue("@SKILLS", resumeSearch.Skills);
            }

            if (!string.IsNullOrEmpty(resumeSearch.Email))
            {
                filterQuery.Append(" AND UPPER(R.EMAIL) LIKE UPPER('%' + @EMAIL + '%')");
                sqlCommand.Parameters.AddWithValue("@EMAIL", resumeSearch.Email);
            }

            if (resumeSearch.CategoryItemId.HasValue)
            {
                filterQuery.Append(" AND R.CATEGORY_ITEM_ID = @CATEGORY_ITEM_ID");
                sqlCommand.Parameters.AddWithValue("@CATEGORY_ITEM_ID", resumeSearch.CategoryItemId);
            }

            if (resumeSearch.CityId.HasValue)
            {
                filterQuery.Append(" AND R.CITY_ID = @CITY_ID");
                sqlCommand.Parameters.AddWithValue("@CITY_ID", resumeSearch.CityId);
            }

            if (resumeSearch.SalaryId.HasValue)
            {
                filterQuery.Append(" AND R.SALARY_ID = @SALARY_ID");
                sqlCommand.Parameters.AddWithValue("@SALARY_ID", resumeSearch.SalaryId);
            }

            if (resumeSearch.AgeId.HasValue)
            {
                filterQuery.Append(" AND R.AGE_ID = @AGE_ID");
                sqlCommand.Parameters.AddWithValue("@AGE_ID", resumeSearch.AgeId);
            }

            if (resumeSearch.ExperienceId.HasValue)
            {
                filterQuery.Append(" AND R.EXPERIENCE_ID = @EXPERIENCE_ID");
                sqlCommand.Parameters.AddWithValue("@EXPERIENCE_ID", resumeSearch.ExperienceId);
            }

            if (resumeSearch.EducationId.HasValue)
            {
                filterQuery.Append(" AND R.EDUCATION_ID = @EDUCATION_ID");
                sqlCommand.Parameters.AddWithValue("@EDUCATION_ID", resumeSearch.EducationId);
            }
            #endregion

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    COUNT(*)
                FROM RESUMES R
                WHERE R.IS_DELETED = 0
            ");

            query.Append(filterQuery);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            int.TryParse(
                sqlCommand.ExecuteScalar().ToString(),
                out int count);
            return count;
        }
        #endregion

        public IEnumerable<SelectItem> GetList()
        {
            throw new NotImplementedException();
        }

        public Resume GetById(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT
                        ID,
                        CATEGORY_ITEM_ID,
	                    CITY_ID,
	                    SALARY_ID,
	                    AGE_ID,
	                    EDUCATION_ID,
	                    EXPERIENCE_ID,
                        IMAGE_URL,
	                    FIRST_NAME,
	                    LAST_NAME,
	                    MIDDLE_NAME,
	                    GENDER,
	                    ABOUT_EXPERIENCE,
	                    ABOUT_EDUCATION,
	                    MORE_INFORMATION,
	                    POSITION,
	                    SKILLS,
	                    EMAIL,
	                    EXPIRY_DATE
                    FROM RESUMES
                    WHERE ID = @ID AND IS_DELETED = 0
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", id);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                if (dataTable.Rows.Count == 0)
                    return null;
                DataRow dataRow = dataTable.Rows[0];
                Resume resume = new Resume(dataRow);
                resume.ResumePhones = GetResumePhones(connection, id);
                return resume;
            }
        }

        public bool CheckExistence(Resume resume)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT
	                    ISNULL(COUNT(ID), 0)
                    FROM RESUMES 
                    WHERE ID <> @ID AND UPPER(FIRST_NAME) = UPPER(@FIRST_NAME)
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", resume.Id);
                sqlCommand.Parameters.AddWithValue("@NAME", resume.FirstName);
                object value = sqlCommand.ExecuteScalar();
                int.TryParse(value.ToString(), out int result);
                return result > 0;
            }
        }

        public bool Insert(Resume resume)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                        INSERT INTO RESUMES(
                            CATEGORY_ITEM_ID,
	                        CITY_ID,
	                        SALARY_ID,
	                        AGE_ID,
	                        EDUCATION_ID,
	                        EXPERIENCE_ID,
                            IMAGE_URL,
	                        FIRST_NAME,
	                        LAST_NAME,
	                        MIDDLE_NAME,
	                        GENDER,
	                        ABOUT_EXPERIENCE,
	                        ABOUT_EDUCATION,
	                        MORE_INFORMATION,
	                        POSITION,
	                        SKILLS,
	                        EMAIL,
	                        EXPIRY_DATE,
	                        CREATED_BY                    
                        )
                        VALUES(
	                        @CATEGORY_ITEM_ID,
	                        @CITY_ID,
	                        @SALARY_ID,
	                        @AGE_ID,
	                        @EDUCATION_ID,
	                        @EXPERIENCE_ID,
                            @IMAGE_URL,
	                        @FIRST_NAME,
	                        @LAST_NAME,
	                        @MIDDLE_NAME,
	                        @GENDER,
	                        @ABOUT_EXPERIENCE,
	                        @ABOUT_EDUCATION,
	                        @MORE_INFORMATION,
	                        @POSITION,
	                        @SKILLS,
	                        @EMAIL,
	                        @EXPIRY_DATE,
	                        @CREATED_BY 
                        )
                        SELECT @ID = SCOPE_IDENTITY();
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@CATEGORY_ITEM_ID", resume.CategoryItem?.Id);
                    sqlCommand.Parameters.AddWithValue("@CITY_ID", resume.City?.Id);
                    sqlCommand.Parameters.AddWithValue("@SALARY_ID", resume.Salary?.Id);
                    sqlCommand.Parameters.AddWithValue("@AGE_ID", resume.Age?.Id);
                    sqlCommand.Parameters.AddWithValue("@EDUCATION_ID", resume.Education?.Id);
                    sqlCommand.Parameters.AddWithValue("@EXPERIENCE_ID", resume.Experience?.Id);
                    sqlCommand.Parameters.AddWithValue("@IMAGE_URL", resume.ImageUrl);
                    sqlCommand.Parameters.AddWithValue("@FIRST_NAME", resume.FirstName);
                    sqlCommand.Parameters.AddWithValue("@LAST_NAME", resume.LastName);
                    sqlCommand.Parameters.AddWithValue("@MIDDLE_NAME", resume.MiddleName);
                    sqlCommand.Parameters.AddWithValue("@GENDER", resume.Gender);
                    sqlCommand.Parameters.AddWithValue("@ABOUT_EXPERIENCE", resume.AboutExperience);
                    sqlCommand.Parameters.AddWithValue("@ABOUT_EDUCATION", resume.AboutEducation);
                    sqlCommand.Parameters.AddWithValue("@MORE_INFORMATION", resume.MoreInformation);
                    sqlCommand.Parameters.AddWithValue("@POSITION", resume.Position);
                    sqlCommand.Parameters.AddWithValue("@SKILLS", resume.Skills);
                    sqlCommand.Parameters.AddWithValue("@EMAIL", resume.Email);
                    sqlCommand.Parameters.AddWithValue("@EXPIRY_DATE", resume.ExpiryDate);
                    sqlCommand.Parameters.AddWithValue("@CREATED_BY", resume.CreatedBy);
                    sqlCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int)
                    {
                        Direction = ParameterDirection.Output
                    });
                    int affectedRow = sqlCommand.ExecuteNonQuery();

                    int resumeId = 0;
                    if (sqlCommand.Parameters["@ID"].Value != DBNull.Value)
                        resumeId = Convert.ToInt32(sqlCommand.Parameters["@ID"].Value);

                    if (resumeId > 0)
                    {
                        resume.ResumePhones = resume.ResumePhones.Select(e => new ResumePhone()
                        {
                            ResumeId = resumeId,
                            PhoneNumber = e.PhoneNumber
                        });
                        InsertResumePhones(
                            resume.ResumePhones,
                            connection,
                            transaction);
                    }

                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Update(Resume resume)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                    UPDATE RESUMES
                    SET
                        CATEGORY_ITEM_ID = @CATEGORY_ITEM_ID,
	                    CITY_ID = @CITY_ID,
	                    SALARY_ID = @SALARY_ID,
	                    AGE_ID = @AGE_ID,
	                    EDUCATION_ID = @EDUCATION_ID,
	                    EXPERIENCE_ID = @EXPERIENCE_ID,
                        IMAGE_URL = @IMAGE_URL,
	                    FIRST_NAME = @FIRST_NAME,
	                    LAST_NAME = @LAST_NAME,
	                    MIDDLE_NAME = @MIDDLE_NAME,
	                    GENDER = @GENDER,
	                    ABOUT_EXPERIENCE = @ABOUT_EXPERIENCE,
	                    ABOUT_EDUCATION = @ABOUT_EDUCATION,
	                    MORE_INFORMATION = @MORE_INFORMATION,
	                    POSITION = @POSITION,
	                    SKILLS = @SKILLS,
	                    EMAIL = @EMAIL,
	                    EXPIRY_DATE = @EXPIRY_DATE,
	                    UPDATED_BY = @UPDATED_BY,
                        UPDATED_DATE = @UPDATED_DATE
                    WHERE ID = @ID
                ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@CATEGORY_ITEM_ID", resume.CategoryItem?.Id);
                    sqlCommand.Parameters.AddWithValue("@CITY_ID", resume.City?.Id);
                    sqlCommand.Parameters.AddWithValue("@SALARY_ID", resume.Salary?.Id);
                    sqlCommand.Parameters.AddWithValue("@AGE_ID", resume.Age?.Id);
                    sqlCommand.Parameters.AddWithValue("@EDUCATION_ID", resume.Education?.Id);
                    sqlCommand.Parameters.AddWithValue("@EXPERIENCE_ID", resume.Experience?.Id);
                    sqlCommand.Parameters.AddWithValue("@IMAGE_URL", resume?.ImageUrl);
                    sqlCommand.Parameters.AddWithValue("@FIRST_NAME", resume.FirstName);
                    sqlCommand.Parameters.AddWithValue("@LAST_NAME", resume.LastName);
                    sqlCommand.Parameters.AddWithValue("@MIDDLE_NAME", resume.MiddleName);
                    sqlCommand.Parameters.AddWithValue("@GENDER", resume.Gender);
                    sqlCommand.Parameters.AddWithValue("@ABOUT_EXPERIENCE", resume.AboutExperience);
                    sqlCommand.Parameters.AddWithValue("@ABOUT_EDUCATION", resume.AboutEducation);
                    sqlCommand.Parameters.AddWithValue("@MORE_INFORMATION", resume.MoreInformation);
                    sqlCommand.Parameters.AddWithValue("@POSITION", resume.Position);
                    sqlCommand.Parameters.AddWithValue("@SKILLS", resume.Skills);
                    sqlCommand.Parameters.AddWithValue("@EMAIL", resume.Email);
                    sqlCommand.Parameters.AddWithValue("@EXPIRY_DATE", resume.ExpiryDate);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_BY", resume.UpdatedBy);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_DATE", resume.UpdatedDate);
                    sqlCommand.Parameters.AddWithValue("@ID", resume.Id);
                    int affectedRow = sqlCommand.ExecuteNonQuery();

                    UpdateResumePhones(
                        resume.ResumePhones,
                        connection,
                        transaction);

                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Delete(DeleteEntity entity)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();

                    DeleteResumePhones(connection, transaction, entity.Id);

                    string query = @"
                        UPDATE RESUMES
                        SET 
                            DELETED_BY = @DELETED_BY,
                            DELETED_DATE = @DELETED_DATE,
                            IS_DELETED = 1
                        WHERE ID = @ID AND IS_DELETED = 0
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@DELETED_BY", entity.DeletedBy);
                    sqlCommand.Parameters.AddWithValue("@DELETED_DATE", entity.DeletedDate);
                    sqlCommand.Parameters.AddWithValue("@ID", entity.Id);
                    int affectedRow = sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
        #endregion

        #region ResumePhones

        private IEnumerable<ResumePhone> GetResumePhones(
            SqlConnection connection,
            int resumeId)
        {
            string query = @"
                SELECT 
                    RESUME_ID,
                    PHONE_NUMBER
                FROM RESUME_PHONES
                WHERE RESUME_ID = @RESUME_ID
            ";
            SqlCommand sqlCommand = new SqlCommand(query, connection);
            sqlCommand.Parameters.AddWithValue("@RESUME_ID", resumeId);
            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            DataRowCollection dataRowCollection = dataTable.Rows;
            IList<ResumePhone> resumePhones = new List<ResumePhone>();
            foreach (DataRow dataRow in dataRowCollection)
            {
                ResumePhone resumePhone = new ResumePhone(dataRow);
                resumePhones.Add(resumePhone);
            }
            return resumePhones;
        }

        private void InsertResumePhones(
            IEnumerable<ResumePhone> resumePhones,
            SqlConnection connection,
            SqlTransaction transaction)
        {
            DataTable destinationTable = new DataTable();
            destinationTable.Columns.Add("RESUME_ID", typeof(int));
            destinationTable.Columns.Add("PHONE_NUMBER", typeof(string));
            foreach (ResumePhone resumePhone in resumePhones)
            {
                DataRow dataRow = destinationTable.NewRow();
                dataRow["RESUME_ID"] = resumePhone.ResumeId;
                dataRow["PHONE_NUMBER"] = resumePhone.PhoneNumber;
                destinationTable.Rows.Add(dataRow);
            }

            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(
                connection,
                SqlBulkCopyOptions.KeepIdentity,
                transaction))
            {
                sqlBulkCopy.BatchSize = resumePhones.Count();
                sqlBulkCopy.DestinationTableName = "RESUME_PHONES";
                sqlBulkCopy.WriteToServer(destinationTable);
            }
        }

        private void UpdateResumePhones(
            IEnumerable<ResumePhone> resumePhones,
            SqlConnection connection,
            SqlTransaction transaction)
        {
            //delete old unused rolePermissions
            string query = @"
                DELETE RESUME_PHONES
                WHERE RESUME_ID = @RESUME_ID
            ";
            SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
            sqlCommand.Parameters.AddWithValue("@RESUME_ID", resumePhones.FirstOrDefault().ResumeId);
            sqlCommand.ExecuteNonQuery();

            //insert or update new rolePermissions
            DataTable destinationTable = new DataTable();
            destinationTable.Columns.Add("RESUME_ID", typeof(int));
            destinationTable.Columns.Add("PHONE_NUMBER", typeof(string));

            foreach (ResumePhone resumePhone in resumePhones)
            {
                DataRow dataRow = destinationTable.NewRow();
                dataRow["RESUME_ID"] = resumePhone.ResumeId;
                dataRow["PHONE_NUMBER"] = resumePhone.PhoneNumber;
                destinationTable.Rows.Add(dataRow);
            }

            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(
                connection,
                SqlBulkCopyOptions.Default,
                transaction))
            {
                sqlBulkCopy.BatchSize = resumePhones.Count();
                sqlBulkCopy.DestinationTableName = "RESUME_PHONES";
                sqlBulkCopy.WriteToServer(destinationTable);
            }
        }

        private int DeleteResumePhones(
            SqlConnection connection,
            SqlTransaction transaction,
            int resumeId)
        {
            string query = @"
                DELETE RESUME_PHONES
                WHERE RESUME_ID = @RESUME_ID
            ";
            SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
            sqlCommand.Parameters.AddWithValue("@RESUME_ID", resumeId);
            int affectedRows = sqlCommand.ExecuteNonQuery();
            return affectedRows;
        }

        #endregion
    }
}
