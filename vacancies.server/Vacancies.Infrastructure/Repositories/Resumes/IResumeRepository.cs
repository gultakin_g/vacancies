﻿using System;
using System.Collections.Generic;
using System.Text;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Resumes;

namespace Vacancies.Infrastructure.Repositories.Resumes
{
    public interface IResumeRepository
    {
        (IEnumerable<Resume>, int) FindList(ResumeSearch resumeSearch);
        IEnumerable<SelectItem> GetList();
        Resume GetById(int id);
        bool CheckExistence(Resume resume);
        bool Insert(Resume resume);
        bool Update(Resume resume);
        bool Delete(DeleteEntity entity);
    }
}
