﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Roles;
using Vacancies.Infrastructure.Handlers;

namespace Vacancies.Infrastructure.Repositories.Roles
{
    public class RoleRepository : IRoleRepository
    {
        private readonly IDbHandler _dbHandler;

        public RoleRepository(IDbHandler dbHandler)
        {
            _dbHandler = dbHandler;
        }

        #region Roles

        #region FindList
        public (IEnumerable<Role>, int) FindList(RoleSearch roleSearch)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                IEnumerable<Role> roles = GetRolesList(connection, roleSearch);
                int rolesCount = GetRolesCount(connection, roleSearch);
                return (roles, rolesCount);
            }
        }

        private IEnumerable<Role> GetRolesList(
            SqlConnection connection,
            RoleSearch roleSearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (!string.IsNullOrEmpty(roleSearch.Name))
            {
                filterQuery.Append("AND UPPER(NAME) LIKE UPPER('%'+ @NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@NAME", roleSearch.Name);
            }

            if (!string.IsNullOrEmpty(roleSearch.Description))
            {
                filterQuery.Append("AND UPPER(DESCRIPTION) LIKE UPPER('%'+ @DESCRIPTION + '%')");
                sqlCommand.Parameters.AddWithValue("DESCRIPTION", roleSearch.Description);
            }

            StringBuilder orderBy = new StringBuilder();
            switch (roleSearch.SortBy.Trim().ToUpper())
            {
                case "NAME":
                    orderBy.Append(" ORDER BY NAME");
                    break;
                case "DESCRIPTION":
                    orderBy.Append(" ORDER BY DESCRIPTION");
                    break;
                default:
                    orderBy.Append(" ORDER BY ID");
                    break;
            }
            string sortDirection = roleSearch.SortDirection.Trim().ToUpper() == "DESC" ? " DESC" : " ASC";
            orderBy.Append(sortDirection);

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    ID,
                    NAME,
                    DESCRIPTION
                FROM ROLES
                WHERE 1=1 
            ");

            query.Append(filterQuery);
            query.Append(orderBy);

            query.Append(" OFFSET @OFFSET_ROWS_COUNT ROWS FETCH NEXT @FETCH_ROWS_COUNT ROWS ONLY");
            sqlCommand.Parameters.AddWithValue("@OFFSET_ROWS_COUNT", (roleSearch.CurrentPage - 1) * roleSearch.PerPage);
            sqlCommand.Parameters.AddWithValue("@FETCH_ROWS_COUNT", roleSearch.PerPage);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            DataRowCollection dataRowCollection = dataTable.Rows;
            IList<Role> roles = new List<Role>();
            foreach (DataRow dataRow in dataRowCollection)
            {
                Role role = new Role(dataRow);
                roles.Add(role);
            }
            return roles;
        }

        private int GetRolesCount(
            SqlConnection connection,
            RoleSearch roleSearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (!string.IsNullOrEmpty(roleSearch.Name))
            {
                filterQuery.Append(" AND UPPER(NAME) LIKE UPPER('%'+ @NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@NAME", roleSearch.Name);
            }

            if (!string.IsNullOrEmpty(roleSearch.Description))
            {
                filterQuery.Append(" AND UPPER(DESCRIPTION) LIKE UPPER('%' + @DESCRIPTION + '%')");
                sqlCommand.Parameters.AddWithValue("@DESCRIPTION", roleSearch.Description);
            }

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    COUNT(*)
                FROM ROLES
                WHERE 1=1
            ");

            query.Append(filterQuery);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            int.TryParse(
                sqlCommand.ExecuteScalar().ToString(),
                out int count);
            return count;
        }

        #endregion

        public IEnumerable<SelectItem> GetList()
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT 
                        ID,
                        NAME VALUE
                    FROM ROLES
                    ORDER BY NAME
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                DataRowCollection dataRowCollection = dataTable.Rows;
                IList<SelectItem> selectItems = new List<SelectItem>();

                foreach (DataRow dataRow in dataRowCollection)
                {
                    SelectItem selectItem = new SelectItem();
                    selectItem.Id = Convert.ToInt32(dataRow["ID"]);
                    selectItem.Value = Convert.ToString(dataRow["VALUE"]);
                    selectItems.Add(selectItem);
                }
                return selectItems;
            }
        }

        public Role GetById(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT 
                        ID,
                        NAME,
                        DESCRIPTION
                    FROM ROLES
                    WHERE ID = @ID
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", id);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                if (dataTable.Rows.Count == 0)
                    return null;
                DataRow dataRow = dataTable.Rows[0];
                Role role = new Role(dataRow);
                role.RolePermissions = GetRolePermissions(connection, id);
                return role;
            }
        }

        public bool CheckExistence(Role role)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT
	                    ISNULL(COUNT(ID), 0)
                    FROM ROLES 
                    WHERE ID <> @ID AND UPPER(NAME) = UPPER(@NAME)
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", role.Id);
                sqlCommand.Parameters.AddWithValue("@NAME", role.Name);
                object value = sqlCommand.ExecuteScalar();
                int.TryParse(value.ToString(), out int result);
                return result > 0;
            }
        }

        public bool Insert(Role role)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction sqlTransaction = connection.BeginTransaction();
                    string query = @"
                        INSERT INTO ROLES(
                            NAME,
                            DESCRIPTION,
                            CREATED_BY
                        )
                        VALUES(
                            @NAME,
                            @DESCRIPTION,
                            @CREATED_BY
                        );
                        SELECT @ID = SCOPE_IDENTITY();
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, sqlTransaction);
                    sqlCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int)
                    {
                        Direction = ParameterDirection.Output
                    });
                    sqlCommand.Parameters.AddWithValue("@NAME", role.Name);
                    sqlCommand.Parameters.AddWithValue("@DESCRIPTION", role.Description);
                    sqlCommand.Parameters.AddWithValue("@CREATED_BY", role.CreatedBy);
                    int affectedRow = sqlCommand.ExecuteNonQuery();

                    int roleId = 0;
                    if (sqlCommand.Parameters["@ID"].Value != DBNull.Value)
                        roleId = Convert.ToInt32(sqlCommand.Parameters["@ID"].Value);

                    if(roleId > 0)
                    {
                        role.RolePermissions = role.RolePermissions.Select(e => new RolePermission()
                        {
                            RoleId = roleId,
                            PermissionId = e.PermissionId
                        });
                        InsertRolePermissions(
                            role.RolePermissions, 
                            connection, 
                            sqlTransaction);
                    }
                    sqlTransaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Update(Role role)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                        UPDATE ROLES
                        SET 
	                        NAME = @NAME,
	                        DESCRIPTION = @DESCRIPTION,
	                        UPDATED_BY = @UPDATED_BY,
	                        UPDATED_DATE = @UPDATED_DATE
                        WHERE ID = @ID
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@NAME", role.Name);
                    sqlCommand.Parameters.AddWithValue("@DESCRIPTION", role.Description);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_BY", role.UpdatedBy);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_DATE", role.UpdatedDate);
                    sqlCommand.Parameters.AddWithValue("@ID", role.Id);
                    int affectedRow = sqlCommand.ExecuteNonQuery();

                    UpdateRolePermissions(
                        role.RolePermissions,
                        connection,
                        transaction);

                    transaction.Commit();

                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();

                    DeleteRolePermissions(connection, transaction, id);

                    string query = @"
                        DELETE ROLES
                        WHERE ID = @ID
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@ID", id);
                    int affectedRow = sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region RolePermissions

        private IEnumerable<RolePermission> GetRolePermissions(
            SqlConnection connection,
            int roleId)
        {
            string query = @"
                SELECT 
                    ROLE_ID,
                    PERMISSION_ID
                FROM ROLE_PERMISSIONS
                WHERE ROLE_ID = @ROLE_ID
            ";
            SqlCommand sqlCommand = new SqlCommand(query, connection);
            sqlCommand.Parameters.AddWithValue("@ROLE_ID", roleId);
            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            DataRowCollection dataRowCollection = dataTable.Rows;
            IList<RolePermission> rolePermissions = new List<RolePermission>();
            foreach (DataRow dataRow in dataRowCollection)
            {
                RolePermission rolePermission = new RolePermission(dataRow);
                rolePermissions.Add(rolePermission);
            }
            return rolePermissions;
        }

        private void InsertRolePermissions(
            IEnumerable<RolePermission> rolePermissions,
            SqlConnection connection,
            SqlTransaction transaction)
        {
            DataTable destinationTable = new DataTable();
            destinationTable.Columns.Add("ROLE_ID", typeof(int));
            destinationTable.Columns.Add("PERMISSION_ID", typeof(int));
            foreach(RolePermission rolePermission in rolePermissions)
            {
                DataRow dataRow = destinationTable.NewRow();
                dataRow["ROLE_ID"] = rolePermission.RoleId;
                dataRow["PERMISSION_ID"] = rolePermission.PermissionId;
                destinationTable.Rows.Add(dataRow);
            }

            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(
                connection,
                SqlBulkCopyOptions.KeepIdentity,
                transaction))
            {
                sqlBulkCopy.BatchSize = rolePermissions.Count();
                sqlBulkCopy.DestinationTableName = "ROLE_PERMISSIONS";
                sqlBulkCopy.WriteToServer(destinationTable);
            }
        }

        private void UpdateRolePermissions(
            IEnumerable<RolePermission> rolePermissions,
            SqlConnection connection,
            SqlTransaction transaction)
        {
            //delete old unused rolePermissions
            string query = @"
                DELETE ROLE_PERMISSIONS
                WHERE ROLE_ID = @ROLE_ID
            ";
            SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
            sqlCommand.Parameters.AddWithValue("@ROLE_ID", rolePermissions.FirstOrDefault().RoleId);
            sqlCommand.ExecuteNonQuery();

            //insert or update new rolePermissions
            DataTable destinationTable = new DataTable();
            destinationTable.Columns.Add("ROLE_ID", typeof(int));
            destinationTable.Columns.Add("PERMISSION_ID", typeof(int));
            foreach (RolePermission rolePermission in rolePermissions)
            {
                DataRow dataRow = destinationTable.NewRow();
                dataRow["ROLE_ID"] = rolePermission.RoleId;
                dataRow["PERMISSION_ID"] = rolePermission.PermissionId;
                destinationTable.Rows.Add(dataRow);
            }

            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(
                connection, 
                SqlBulkCopyOptions.Default,
                transaction))
            {
                sqlBulkCopy.BatchSize = rolePermissions.Count();
                sqlBulkCopy.DestinationTableName = "ROLE_PERMISSIONS";
                sqlBulkCopy.WriteToServer(destinationTable);
            }
        }

        private int DeleteRolePermissions(
            SqlConnection connection, 
            SqlTransaction transaction,
            int roleId)
        {
            string query = @"
                DELETE ROLE_PERMISSIONS
                WHERE ROLE_ID = @ROLE_ID
            ";
            SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
            sqlCommand.Parameters.AddWithValue("@ROLE_ID", roleId);
            int affectedRows = sqlCommand.ExecuteNonQuery();
            return affectedRows;
        }

        #endregion
    }
}
