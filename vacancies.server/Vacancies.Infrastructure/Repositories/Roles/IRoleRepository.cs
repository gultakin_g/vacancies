﻿using System;
using System.Collections.Generic;
using System.Text;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Roles;

namespace Vacancies.Infrastructure.Repositories.Roles
{
    public interface IRoleRepository
    {
        (IEnumerable<Role>, int) FindList(RoleSearch roleSearch);
        IEnumerable<SelectItem> GetList();
        Role GetById(int id);
        bool CheckExistence(Role role);
        bool Insert(Role role);
        bool Update(Role role);
        bool Delete(int id);
    }
}
