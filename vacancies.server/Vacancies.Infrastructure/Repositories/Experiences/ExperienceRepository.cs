﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Experiences;
using Vacancies.Infrastructure.Handlers;

namespace Vacancies.Infrastructure.Repositories.Experiences
{
    public class ExperienceRepository : IExperienceRepository
    {
        private readonly IDbHandler _dbHandler;
        public ExperienceRepository(IDbHandler dbHandler)
        {
            _dbHandler = dbHandler;
        }

        #region FindList
        public (IEnumerable<Experience>, int) FindList(ExperienceSearch experienceSearch)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                IEnumerable<Experience> experiences = GetExperiencesList(connection, experienceSearch);
                int experiencesCount = GetExperiencesCount(connection, experienceSearch);
                return (experiences, experiencesCount);
            }
        }
        private IEnumerable<Experience> GetExperiencesList(
        SqlConnection connection,
        ExperienceSearch experienceSearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (!string.IsNullOrEmpty(experienceSearch.Name))
            {
                filterQuery.Append("AND UPPER(NAME) LIKE UPPER('%' + @NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@NAME", experienceSearch.Name);
            }

            if (!string.IsNullOrEmpty(experienceSearch.Description))
            {
                filterQuery.Append("AND UPPER(DESCRIPTION) LIKE UPPER('%' + @DESCRIPTION + '%')");
                sqlCommand.Parameters.AddWithValue("@DESCRIPTION", experienceSearch.Description);
            }

            StringBuilder orderBy = new StringBuilder();
            switch (experienceSearch.SortBy.Trim().ToUpper())
            {
                case "NAME":
                    orderBy.Append(" ORDER BY NAME");
                    break;
                case "DESCRIPTION":
                    orderBy.Append(" ORDER BY DESCRIPTION");
                    break;
                default:
                    orderBy.Append(" ORDER BY ID");
                    break;
            }
            string sortDirection = experienceSearch.SortDirection.Trim().ToUpper() == "DESC" ? " DESC" : " ASC";
            orderBy.Append(sortDirection);

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    ID,
                    NAME,
                    DESCRIPTION
                FROM EXPERIENCES
                WHERE 1=1 
            ");

            query.Append(filterQuery);
            query.Append(orderBy);

            query.Append(" OFFSET @OFFSET_ROWS_COUNT ROWS FETCH NEXT @FETCH_ROWS_COUNT ROWS ONLY");
            sqlCommand.Parameters.AddWithValue("@OFFSET_ROWS_COUNT", (experienceSearch.CurrentPage - 1) * experienceSearch.PerPage);
            sqlCommand.Parameters.AddWithValue("@FETCH_ROWS_COUNT", experienceSearch.PerPage);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            DataRowCollection dataRowCollection = dataTable.Rows;
            IList<Experience> experiences = new List<Experience>();
            foreach (DataRow dataRow in dataRowCollection)
            {
                Experience experience = new Experience(dataRow);
                experiences.Add(experience);
            }
            return experiences;
        }

        private int GetExperiencesCount(
           SqlConnection connection,
           ExperienceSearch experienceSearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (!string.IsNullOrEmpty(experienceSearch.Name))
            {
                filterQuery.Append(" AND UPPER(NAME) LIKE UPPER('%'+ @NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@NAME", experienceSearch.Name);
            }

            if (!string.IsNullOrEmpty(experienceSearch.Description))
            {
                filterQuery.Append(" AND UPPER(DESCRIPTION) LIKE UPPER('%' + @DESCRIPTION + '%')");
                sqlCommand.Parameters.AddWithValue("@DESCRIPTION", experienceSearch.Description);
            }

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    COUNT(*)
                FROM EXPERIENCES
                WHERE 1=1
            ");

            query.Append(filterQuery);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            int.TryParse(
                sqlCommand.ExecuteScalar().ToString(),
                out int count);
            return count;
        }

        #endregion

        public IEnumerable<SelectItem> GetList()
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT 
                        ID,
                        NAME VALUE
                    FROM EXPERIENCES
                    ORDER BY NAME
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                DataRowCollection dataRowCollection = dataTable.Rows;
                IList<SelectItem> selectItems = new List<SelectItem>();

                foreach (DataRow dataRow in dataRowCollection)
                {
                    SelectItem selectItem = new SelectItem();
                    selectItem.Id = Convert.ToInt32(dataRow["ID"]);
                    selectItem.Value = Convert.ToString(dataRow["VALUE"]);
                    selectItems.Add(selectItem);
                }
                return selectItems;
            }
        }

        public Experience GetById(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT 
                        ID,
                        NAME,
                        DESCRIPTION
                    FROM EXPERIENCES
                    WHERE ID = @ID
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", id);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                if (dataTable.Rows.Count == 0)
                    return null;
                DataRow dataRow = dataTable.Rows[0];
                Experience experience = new Experience(dataRow);
                return experience;
            }
        }

        public bool CheckExistence(Experience experience)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT
	                    ISNULL(COUNT(ID), 0)
                    FROM EXPERIENCES 
                    WHERE ID <> @ID AND UPPER(NAME) = UPPER(@NAME)
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", experience.Id);
                sqlCommand.Parameters.AddWithValue("@NAME", experience.Name);
                Object value = sqlCommand.ExecuteScalar();
                int.TryParse(value.ToString(), out int result);
                return result > 0;
            }
        }

        public bool Insert(Experience experience)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                        INSERT INTO EXPERIENCES(NAME, DESCRIPTION, CREATED_BY) 
                        VALUES(@NAME, @DESCRIPTION, @CREATED_BY)
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@NAME", experience.Name);
                    sqlCommand.Parameters.AddWithValue("@DESCRIPTION", experience.Description);
                    sqlCommand.Parameters.AddWithValue("@CREATED_BY", experience.CreatedBy);
                    int affectedRow = sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Update(Experience experience)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                       UPDATE EXPERIENCES
                       SET
                            NAME = @NAME,
                            DESCRIPTION = @DESCRIPTION,
                            UPDATED_BY = @UPDATED_BY,
                            UPDATED_DATE = @UPDATED_DATE
                       WHERE ID = @ID
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@NAME", experience.Name);
                    sqlCommand.Parameters.AddWithValue("@DESCRIPTION", experience.Description);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_BY", experience.UpdatedBy);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_DATE", experience.UpdatedDate);
                    sqlCommand.Parameters.AddWithValue("@ID", experience.Id);
                    int affectedRow = sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Delete(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                     DELETE EXPERIENCES
                     WHERE ID = @ID
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", id);
                int affectedRow = sqlCommand.ExecuteNonQuery();
                return affectedRow == 1;
            }
        }
    }
}
