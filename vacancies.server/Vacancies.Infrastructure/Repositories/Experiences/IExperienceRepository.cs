﻿using System.Collections.Generic;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Experiences;

namespace Vacancies.Infrastructure.Repositories.Experiences
{
    public interface IExperienceRepository
    {
        (IEnumerable<Experience>, int) FindList(ExperienceSearch experienceSearch);
        IEnumerable<SelectItem> GetList();
        Experience GetById(int id);
        bool CheckExistence(Experience experience);
        bool Insert(Experience experience);
        bool Update(Experience experience);
        bool Delete(int id);
    }
}
