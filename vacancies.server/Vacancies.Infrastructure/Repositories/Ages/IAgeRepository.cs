﻿using System.Collections.Generic;
using Vacancies.Domain.Models.Ages;
using Vacancies.Domain.Models.Bases;

namespace Vacancies.Infrastructure.Repositories.Ages
{
    public interface IAgeRepository
    {
        (IEnumerable<Age>, int) FindList(AgeSearch ageSearch);
        IEnumerable<SelectItem> GetList();
        Age GetById(int id);
        bool CheckExistence(Age age);
        bool Insert(Age age);
        bool Update(Age age);
        bool Delete(int id);
    }
}
