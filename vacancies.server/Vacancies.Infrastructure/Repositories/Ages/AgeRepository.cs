﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Vacancies.Domain.Models.Ages;
using Vacancies.Domain.Models.Bases;
using Vacancies.Infrastructure.Handlers;

namespace Vacancies.Infrastructure.Repositories.Ages
{
    public class AgeRepository : IAgeRepository
    {
        private readonly IDbHandler _dbHandler;

        public AgeRepository(IDbHandler dbHandler)
        {
            _dbHandler = dbHandler;
        }

        #region FindList

        public (IEnumerable<Age>, int) FindList(AgeSearch ageSearch)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                IEnumerable<Age> ages = GetAgesList(connection, ageSearch);
                int agesCount = GetAgesCount(connection, ageSearch);
                return (ages, agesCount);
            }
        }

        private IEnumerable<Age> GetAgesList(
            SqlConnection connection, 
            AgeSearch ageSearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (ageSearch.Amount.HasValue)
            {
                filterQuery.Append(" AND AMOUNT = @AMOUNT");
                sqlCommand.Parameters.AddWithValue("@AMOUNT", ageSearch.Amount);
            }

            if (!string.IsNullOrEmpty(ageSearch.Description))
            {
                filterQuery.Append(" AND UPPER(DESCRIPTION) LIKE UPPER('%' + @DESCRIPTION + '%')");
                sqlCommand.Parameters.AddWithValue("@DESCRIPTION", ageSearch.Description);
            }

            StringBuilder orderBy = new StringBuilder();
            switch (ageSearch.SortBy.Trim().ToUpper())
            {
                case "AMOUNT":
                    orderBy.Append(" ORDER BY AMOUNT");
                    break;
                case "DESCRIPTION":
                    orderBy.Append(" ORDER BY DESCRIPTION");
                    break;
                default:
                    orderBy.Append(" ORDER BY ID");
                    break;
            }
            string sortDirection = ageSearch.SortDirection.Trim().ToUpper() == "DESC" ? " DESC" : " ASC";
            orderBy.Append(sortDirection);

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    ID,
                    AMOUNT,
                    DESCRIPTION
                FROM AGES
                WHERE 1=1 
            ");

            query.Append(filterQuery);
            query.Append(orderBy);

            query.Append(" OFFSET @OFFSET_ROWS_COUNT ROWS FETCH NEXT @FETCH_ROWS_COUNT ROWS ONLY");
            sqlCommand.Parameters.AddWithValue("@OFFSET_ROWS_COUNT", (ageSearch.CurrentPage - 1) * ageSearch.PerPage);
            sqlCommand.Parameters.AddWithValue("@FETCH_ROWS_COUNT", ageSearch.PerPage);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            DataRowCollection dataRowCollection = dataTable.Rows;
            IList<Age> ages = new List<Age>();
            foreach (DataRow dataRow in dataRowCollection)
            {
                Age age = new Age(dataRow);
                ages.Add(age);
            }
            return ages;
        }

        private int GetAgesCount(
            SqlConnection connection,
            AgeSearch ageSearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (ageSearch.Amount.HasValue)
            {
                filterQuery.Append(" AND AMOUNT = @AMOUNT");
                sqlCommand.Parameters.AddWithValue("@AMOUNT", ageSearch.Amount);
            }

            if (!string.IsNullOrEmpty(ageSearch.Description))
            {
                filterQuery.Append(" AND UPPER(DESCRIPTION) LIKE UPPER('%' + @DESCRIPTION + '%')");
                sqlCommand.Parameters.AddWithValue("@DESCRIPTION", ageSearch.Description);
            }

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    COUNT(*)
                FROM AGES
                WHERE 1=1
            ");

            query.Append(filterQuery);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            int.TryParse(
                sqlCommand.ExecuteScalar().ToString(),
                out int count);
            return count;
        }

        #endregion

        public IEnumerable<SelectItem> GetList()
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT
                        ID,
                        AMOUNT VALUE
                    FROM AGES
                    ORDER BY AMOUNT
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                DataRowCollection dataRowCollection = dataTable.Rows;
                IList<SelectItem> selectItems = new List<SelectItem>();
                foreach (DataRow dataRow in dataRowCollection)
                {
                    SelectItem selectItem = new SelectItem();
                    selectItem.Id = Convert.ToInt32(dataRow["ID"]);
                    selectItem.Value = Convert.ToString(dataRow["VALUE"]);
                    selectItems.Add(selectItem);
                }
                return selectItems;
            }
        }

        public Age GetById(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT 
                        ID,
                        AMOUNT,
                        DESCRIPTION
                    FROM AGES
                    WHERE ID = @ID
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", id);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                if(dataTable.Rows.Count == 0)
                {
                    return null;
                }
                DataRow dataRow = dataTable.Rows[0];
                Age age = new Age(dataRow);
                return age;
            }
        }

        public bool CheckExistence(Age age)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT
	                    ISNULL(COUNT(ID), 0)
                    FROM AGES 
                    WHERE ID <> @ID AND AMOUNT = @AMOUNT
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", age.Id);
                sqlCommand.Parameters.AddWithValue("@AMOUNT", age.Amount);
                object value = sqlCommand.ExecuteScalar();
                int.TryParse(value.ToString(), out int result);
                return result > 0;
            }
        }

        public bool Insert(Age age)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                        INSERT INTO AGES(AMOUNT, DESCRIPTION, CREATED_BY)
                        VALUES(@AMOUNT, @DESCRIPTION, @CREATED_BY)
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@AMOUNT", age.Amount);
                    sqlCommand.Parameters.AddWithValue("@DESCRIPTION", age.Description);
                    sqlCommand.Parameters.AddWithValue("@CREATED_BY", age.CreatedBy);
                    int affectedRow = sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Update(Age age)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                        UPDATE AGES
                        SET 
	                        AMOUNT = @AMOUNT,
	                        DESCRIPTION = @DESCRIPTION,
	                        UPDATED_BY = @UPDATED_BY,
	                        UPDATED_DATE = @UPDATED_DATE
                        WHERE ID = @ID;
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@AMOUNT", age.Amount);
                    sqlCommand.Parameters.AddWithValue("@DESCRIPTION", age.Description ?? (object)DBNull.Value);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_BY", age.UpdatedBy);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_DATE", age.UpdatedDate);
                    sqlCommand.Parameters.AddWithValue("@ID", age.Id);
                    int affectedRow = sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Delete(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                   DELETE AGES
                   WHERE ID = @ID
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID",id);
                int affectedRow = sqlCommand.ExecuteNonQuery();
                return affectedRow == 1;
            }
        }
    }
}
