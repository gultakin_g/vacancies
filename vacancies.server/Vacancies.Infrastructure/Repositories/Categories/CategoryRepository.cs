﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Categories;
using Vacancies.Infrastructure.Handlers;

namespace Vacancies.Infrastructure.Repositories.Categories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly IDbHandler _dbHandler;

        public CategoryRepository(IDbHandler dbHandler)
        {
            _dbHandler = dbHandler;
        }

        #region Categories

        #region FindList

        public (IEnumerable<Category>, int) FindList(CategorySearch categorySearch)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                IEnumerable<Category> categories = GetCategoriesList(connection, categorySearch);
                int categoriesCount = GetCategoriesCount(connection, categorySearch);
                return (categories, categoriesCount);
            }
        }

        private IEnumerable<Category> GetCategoriesList(
            SqlConnection connection,
            CategorySearch categorySearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (!string.IsNullOrEmpty(categorySearch.Name))
            {
                filterQuery.Append("AND UPPER(NAME) LIKE UPPER('%'+ @NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@NAME", categorySearch.Name);
            }

            if (!string.IsNullOrEmpty(categorySearch.Description))
            {
                filterQuery.Append("AND UPPER(DESCRIPTION) LIKE UPPER('%'+ @DESCRIPTION + '%')");
                sqlCommand.Parameters.AddWithValue("DESCRIPTION", categorySearch.Description);
            }

            StringBuilder orderBy = new StringBuilder();

            switch (categorySearch.SortBy.Trim().ToUpper())
            {
                case "NAME":
                    orderBy.Append(" ORDER BY NAME");
                    break;
                case "DESCRIPTION":
                    orderBy.Append(" ORDER BY DESCRIPTION");
                    break;
                default:
                    orderBy.Append(" ORDER BY ID");
                    break;
            }
            string sortDirection = categorySearch.SortDirection.Trim().ToUpper() == "DESC" ? " DESC" : " ASC";
            orderBy.Append(sortDirection);

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    ID,
                    NAME,
                    DESCRIPTION
                FROM CATEGORIES
                WHERE 1=1 
            ");

            query.Append(filterQuery);
            query.Append(orderBy);

            query.Append(" OFFSET @OFFSET_ROWS_COUNT ROWS FETCH NEXT @FETCH_ROWS_COUNT ROWS ONLY");
            sqlCommand.Parameters.AddWithValue("@OFFSET_ROWS_COUNT", (categorySearch.CurrentPage - 1) * categorySearch.PerPage);
            sqlCommand.Parameters.AddWithValue("@FETCH_ROWS_COUNT", categorySearch.PerPage);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            DataRowCollection dataRowCollection = dataTable.Rows;
            IList<Category> categories = new List<Category>();
            foreach (DataRow dataRow in dataRowCollection)
            {
                Category category = new Category(dataRow);
                categories.Add(category);
            }
            return categories;
        }

        private int GetCategoriesCount(
            SqlConnection connection,
            CategorySearch categorySearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (!string.IsNullOrEmpty(categorySearch.Name))
            {
                filterQuery.Append(" AND UPPER(NAME) LIKE UPPER('%'+ @NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@NAME", categorySearch.Name);
            }

            if (!string.IsNullOrEmpty(categorySearch.Description))
            {
                filterQuery.Append(" AND UPPER(DESCRIPTION) LIKE UPPER('%' + @DESCRIPTION + '%')");
                sqlCommand.Parameters.AddWithValue("@DESCRIPTION", categorySearch.Description);
            }

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    COUNT(*)
                FROM CATEGORIES
                WHERE 1=1
            ");

            query.Append(filterQuery);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            int.TryParse(
                sqlCommand.ExecuteScalar().ToString(),
                out int count);
            return count;
        }

        #endregion


        public IEnumerable<SelectItem> GetList()
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT 
                        ID,
                        NAME VALUE
                    FROM CATEGORIES
                    ORDER BY NAME
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                DataRowCollection dataRowCollection = dataTable.Rows;
                IList<SelectItem> selectItems = new List<SelectItem>();

                foreach (DataRow dataRow in dataRowCollection)
                {
                    SelectItem selectItem = new SelectItem();
                    selectItem.Id = Convert.ToInt32(dataRow["ID"]);
                    selectItem.Value = Convert.ToString(dataRow["VALUE"]);
                    selectItems.Add(selectItem);
                }
                return selectItems;
            }
        }

        public Category GetById(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT 
                        ID,
                        NAME,
                        DESCRIPTION
                    FROM CATEGORIES
                    WHERE ID = @ID
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", id);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                if (dataTable.Rows.Count == 0)
                {
                    return null;
                }
                DataRow dataRow = dataTable.Rows[0];
                Category category = new Category(dataRow);
                category.CategoryItems = GetCategoryItems(connection, id);
                return category;
            }
        }

        public bool CheckExistence(Category category)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT
	                    ISNULL(COUNT(ID), 0)
                    FROM CATEGORIES 
                    WHERE ID <> @ID AND UPPER(NAME) = UPPER(@NAME)
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", category.Id);
                sqlCommand.Parameters.AddWithValue("@NAME", category.Name);
                object value = sqlCommand.ExecuteScalar();
                int.TryParse(value.ToString(), out int result);
                return result > 0;
            }
        }

        public bool Insert(Category category)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                        INSERT INTO CATEGORIES(
                            NAME, 
                            DESCRIPTION,
                            CREATED_BY) 
                        VALUES(
                            @NAME,
                            @DESCRIPTION,   
                            @CREATED_BY);
                        SELECT @ID = SCOPE_IDENTITY();
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int)
                    {
                        Direction = ParameterDirection.Output
                    });
                    sqlCommand.Parameters.AddWithValue("@NAME", category.Name);
                    sqlCommand.Parameters.AddWithValue("@DESCRIPTION", category.Description);
                    sqlCommand.Parameters.AddWithValue("@CREATED_BY", category.CreatedBy);
                    int affectedRow = sqlCommand.ExecuteNonQuery();

                    int categoryId = 0;
                    if (sqlCommand.Parameters["@ID"].Value != DBNull.Value)
                        categoryId = Convert.ToInt32(sqlCommand.Parameters["@ID"].Value);

                    if (categoryId > 0)
                    {
                        category.CategoryItems = category.CategoryItems.Select(e => new CategoryItem()
                        {
                            CategoryId = categoryId,
                            Name = e.Name,
                            Description = e.Description
                        });
                        InsertCategoryItems(
                            category.CategoryItems,
                            connection,
                            transaction);
                    }
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        public bool Update(Category category)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                        UPDATE CATEGORIES
                        SET 
	                        NAME = @NAME,
	                        DESCRIPTION = @DESCRIPTION,
	                        UPDATED_BY = @UPDATED_BY,
	                        UPDATED_DATE = @UPDATED_DATE
                        WHERE ID = @ID
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection,transaction);
                    sqlCommand.Parameters.AddWithValue("@NAME", category.Name);
                    sqlCommand.Parameters.AddWithValue("@DESCRIPTION", category.Description);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_BY", category.UpdatedBy);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_DATE", category.UpdatedDate);
                    sqlCommand.Parameters.AddWithValue("@ID", category.Id);
                    int affectedRow = sqlCommand.ExecuteNonQuery();

                    UpdateCategoryItems(
                        category.CategoryItems,
                        connection,
                        transaction);

                    transaction.Commit();

                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }

        public bool Delete(int id)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();

                    DeleteCategoryItems(connection, transaction, id);

                    string query = @"
                       DELETE CATEGORIES
                       WHERE ID = @ID
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@ID", id);
                    int affectedRow = sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
            
        }
        #endregion

        #region CategoryItems

        private IEnumerable<CategoryItem> GetCategoryItems(
            SqlConnection connection,
            int id)
        {
            string query = @"
                SELECT 
                    ID,
                    NAME
                FROM CATEGORY_ITEMS
                WHERE ID = @ID
            ";
            SqlCommand sqlCommand = new SqlCommand(query, connection);
            sqlCommand.Parameters.AddWithValue("@ID", id);
            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            DataRowCollection dataRowCollection = dataTable.Rows;
            IList<CategoryItem> categoryItems = new List<CategoryItem>();
            foreach (DataRow dataRow in dataRowCollection)
            {
                CategoryItem categoryItem = new CategoryItem(dataRow);
                categoryItems.Add(categoryItem);
            }
            return categoryItems;
        }

        private void InsertCategoryItems(
            IEnumerable<CategoryItem> categoryItems,
            SqlConnection connection,
            SqlTransaction transaction)
        {
            DataTable destinationTable = new DataTable();
            destinationTable.Columns.Add("ID", typeof(int));
            destinationTable.Columns.Add("CATEGORY_ID", typeof(int));
            destinationTable.Columns.Add("NAME", typeof(string));
            destinationTable.Columns.Add("DESCRIPTION", typeof(string));

            foreach (CategoryItem categoryItem in categoryItems)
            {
                DataRow dataRow = destinationTable.NewRow();
                //dataRow["ID"] = 0;
                dataRow["CATEGORY_ID"] = categoryItem.CategoryId;
                dataRow["NAME"] = categoryItem.Name;
                dataRow["DESCRIPTION"] = categoryItem.Description;
                destinationTable.Rows.Add(dataRow);
            }

            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(
                connection,
                SqlBulkCopyOptions.Default,
                transaction))
            {
                sqlBulkCopy.BatchSize = categoryItems.Count();
                sqlBulkCopy.DestinationTableName = "CATEGORY_ITEMS";
                sqlBulkCopy.WriteToServer(destinationTable);
            }
        }

        private void UpdateCategoryItems(
            IEnumerable<CategoryItem> categoryItems,
            SqlConnection connection,
            SqlTransaction transaction)
        {
            //delete old unused rolePermissions
            string query = @"
                DELETE CATEGORY_ITEMS
                WHERE CATEGORY_ID = @CATEGORY_ID
            ";
            SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
            sqlCommand.Parameters.AddWithValue("@CATEGORY_ID", categoryItems.FirstOrDefault().CategoryId);
            sqlCommand.ExecuteNonQuery();

            //insert or update new rolePermissions
            DataTable destinationTable = new DataTable();
            destinationTable.Columns.Add("ID", typeof(int));
            destinationTable.Columns.Add("CATEGORY_ID", typeof(int));
            destinationTable.Columns.Add("NAME", typeof(string));
            destinationTable.Columns.Add("DESCRIPTION", typeof(string));
            
            foreach (CategoryItem categoryItem in categoryItems)
            {
                DataRow dataRow = destinationTable.NewRow();
                dataRow["ID"] = categoryItem.Id;
                dataRow["CATEGORY_ID"] = categoryItem.CategoryId;
                dataRow["NAME"] = categoryItem.Name;
                dataRow["DESCRIPTION"] = categoryItem.Description;
                destinationTable.Rows.Add(dataRow);
            }

            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(
                connection,
                SqlBulkCopyOptions.Default,
                transaction))
            {
                sqlBulkCopy.BatchSize = categoryItems.Count();
                sqlBulkCopy.DestinationTableName = "CATEGORY_ITEMS";
                sqlBulkCopy.WriteToServer(destinationTable);
            }
        }

        private int DeleteCategoryItems(
            SqlConnection connection,
            SqlTransaction transaction,
            int categoryId)
        {
            string query = @"
                DELETE CATEGORY_ITEMS
                WHERE CATEGORY_ID = @CATEGORY_ID
            ";
            SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
            sqlCommand.Parameters.AddWithValue("@CATEGORY_ID", categoryId);
            int affectedRows = sqlCommand.ExecuteNonQuery();
            return affectedRows;
        }

        #endregion
    }
}
