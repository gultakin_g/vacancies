﻿using System;
using System.Collections.Generic;
using System.Text;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Categories;

namespace Vacancies.Infrastructure.Repositories.Categories
{
    public interface ICategoryRepository
    {
        (IEnumerable<Category>, int) FindList(CategorySearch categorySearch);
        IEnumerable<SelectItem> GetList();
        Category GetById(int id);
        bool CheckExistence(Category category);
        bool Insert(Category category);
        bool Update(Category category);
        bool Delete(int id);
    }
}
