﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Salaries;
using Vacancies.Infrastructure.Handlers;

namespace Vacancies.Infrastructure.Repositories.Salaries
{
    public class SalaryRepository : ISalaryRepository
    {
        private readonly IDbHandler _dbHandler;
        public SalaryRepository(IDbHandler dbHandler)
        {
            _dbHandler = dbHandler;
        }
        #region FindList
        public (IEnumerable<Salary>, int) FindList(SalarySearch salarySearch)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                IEnumerable<Salary> salaries = GetSalariesList(connection, salarySearch);
                int salariesCount = GetSalariesCount(connection, salarySearch);
                return (salaries, salariesCount);
            }
        }

        private IEnumerable<Salary> GetSalariesList(
            SqlConnection connection,
            SalarySearch salarySearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (salarySearch.Amount.HasValue)
            {
                filterQuery.Append("AND AMOUNT = @AMOUNT");
                sqlCommand.Parameters.AddWithValue("@AMOUNT", salarySearch.Amount);
            }

            if (!string.IsNullOrEmpty(salarySearch.Description))
            {
                filterQuery.Append("AND UPPER(DESCRIPTION) LIKE UPPER('%' + @DESCRIPTION + '%')");
                sqlCommand.Parameters.AddWithValue("@DESCRIPTION", salarySearch.Description);
            }

            StringBuilder orderBy = new StringBuilder();
            switch (salarySearch.SortBy.Trim().ToUpper())
            {
                case "AMOUNT":
                    orderBy.Append(" ORDER BY AMOUNT");
                    break;
                case "DESCRIPTION":
                    orderBy.Append(" ORDER BY DESCRIPTION");
                    break;
                default:
                    orderBy.Append(" ORDER BY ID");
                    break;
            }
            string sortDirection = salarySearch.SortDirection.Trim().ToUpper() == "DESC" ? " DESC" : " ASC";
            orderBy.Append(sortDirection);

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    ID,
                    AMOUNT,
                    DESCRIPTION
                FROM SALARIES
                WHERE 1=1 
            ");

            query.Append(filterQuery);
            query.Append(orderBy);

            query.Append(" OFFSET @OFFSET_ROWS_COUNT ROWS FETCH NEXT @FETCH_ROWS_COUNT ROWS ONLY");
            sqlCommand.Parameters.AddWithValue("@OFFSET_ROWS_COUNT", (salarySearch.CurrentPage - 1) * salarySearch.PerPage);
            sqlCommand.Parameters.AddWithValue("@FETCH_ROWS_COUNT", salarySearch.PerPage);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            DataRowCollection dataRowCollection = dataTable.Rows;
            IList<Salary> salaries = new List<Salary>();
            foreach (DataRow dataRow in dataRowCollection)
            {
                Salary salary  = new Salary(dataRow);
                salaries.Add(salary);
            }
            return salaries;
        }

        private int GetSalariesCount(
            SqlConnection connection,
            SalarySearch salarySearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (salarySearch.Amount.HasValue)
            {
                filterQuery.Append(" AND AMOUNT = @AMOUNT");
                sqlCommand.Parameters.AddWithValue("@AMOUNT", salarySearch.Amount);
            }

            if (!string.IsNullOrEmpty(salarySearch.Description))
            {
                filterQuery.Append(" AND UPPER(DESCRIPTION) LIKE UPPER('%' + @DESCRIPTION + '%')");
                sqlCommand.Parameters.AddWithValue("@DESCRIPTION", salarySearch.Description);
            }

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    COUNT(*)
                FROM SALARIES
                WHERE 1=1
            ");

            query.Append(filterQuery);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            int.TryParse(
                sqlCommand.ExecuteScalar().ToString(),
                out int count);
            return count;
        }
        #endregion

        public IEnumerable<SelectItem> GetList()
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT
                        ID,
                        AMOUNT VALUE
                    FROM SALARIES
                    ORDER BY AMOUNT
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                DataRowCollection dataRowCollection = dataTable.Rows;
                IList<SelectItem> selectItems = new List<SelectItem>();

                foreach (DataRow dataRow in dataRowCollection)
                {
                    SelectItem selectItem = new SelectItem();
                    selectItem.Id = Convert.ToInt32(dataRow["ID"]);
                    selectItem.Value = Convert.ToString(dataRow["VALUE"]);
                    selectItems.Add(selectItem);
                }
                return selectItems;
            }
        }

        public Salary GetById(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT
                        ID ,
                        AMOUNT,
                        DESCRIPTION
                    FROM SALARIES
                    WHERE ID = @ID
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", id);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                if (dataTable.Rows.Count == 0)
                    return null;
                DataRow dataRow = dataTable.Rows[0];
                Salary salary = new Salary(dataRow);
                return salary;
            }
        }

        public bool CheckExistence(Salary salary)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT  
                        ISNULL(COUNT(ID),0)
                    FROM SALARIES
                    WHERE ID <> @ID AND AMOUNT = @AMOUNT
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@AMOUNT", salary.Amount);
                sqlCommand.Parameters.AddWithValue("@ID", salary.Amount);
                object value = sqlCommand.ExecuteScalar();
                int.TryParse(value.ToString(), out int result);
                return result > 0;
            }
        }

        public bool Insert(Salary salary)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                        INSERT INTO SALARIES(AMOUNT, DESCRIPTION, CREATED_BY)
                        VALUES(@AMOUNT, @DESCRIPTION, @CREATED_BY)
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@AMOUNT", salary.Amount);
                    sqlCommand.Parameters.AddWithValue("@DESCRIPTION", salary.Description);
                    sqlCommand.Parameters.AddWithValue("@CREATED_BY", salary.CreatedBy);
                    int affectedRow = sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Update(Salary salary)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                        UPDATE SALARIES
                        SET
                            AMOUNT = @AMOUNT,
                            DESCRIPTION = @DESCRIPTION,
                            UPDATED_BY = @UPDATED_BY,
                            UPDATED_DATE = @UPDATED_DATE
                        WHERE ID = @ID
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@AMOUNT", salary.Amount);
                    sqlCommand.Parameters.AddWithValue("@DESCRIPTION", salary.Description);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_BY", salary.UpdatedBy);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_DATE", salary.UpdatedDate);
                    sqlCommand.Parameters.AddWithValue("@ID", salary.Id);
                    int affectedRow = sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Delete(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    DELETE SALARIES
                    WHERE ID = @ID
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID",id);
                int affectedRow = sqlCommand.ExecuteNonQuery();
                return affectedRow == 1;
            }
        }
    }
}
