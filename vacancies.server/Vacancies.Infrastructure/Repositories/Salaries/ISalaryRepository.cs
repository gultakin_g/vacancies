﻿using System;
using System.Collections.Generic;
using System.Text;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Salaries;

namespace Vacancies.Infrastructure.Repositories.Salaries
{
    public interface ISalaryRepository
    {
        (IEnumerable<Salary>, int) FindList(SalarySearch salarySearch);
        IEnumerable<SelectItem> GetList();
        Salary GetById(int id);
        bool CheckExistence(Salary salary);
        bool Insert(Salary salary);
        bool Update(Salary salary);
        bool Delete(int id);
    }
}
