﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Cities;
using Vacancies.Infrastructure.Handlers;

namespace Vacancies.Infrastructure.Repositories.Cities
{
    public class CityRepository : ICityRepository
    {
        private readonly IDbHandler _dbHandler;

        public CityRepository(IDbHandler dbHandler)
        {
            _dbHandler = dbHandler;
        }

        #region
        public (IEnumerable<City>, int) FindList(CitySearch citySearch)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                IEnumerable<City> cities = GetCitiesList(connection, citySearch);
                int citiesCount = GetCitiesCount(connection, citySearch);
                return (cities, citiesCount);
            }
        }

        private IEnumerable<City> GetCitiesList(
            SqlConnection connection,
            CitySearch citySearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (!string.IsNullOrEmpty(citySearch.Name))
            {
                filterQuery.Append("AND UPPER(NAME) LIKE UPPER('%' + @NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@NAME", citySearch.Name);
            }

            if (!string.IsNullOrEmpty(citySearch.Description))
            {
                filterQuery.Append("AND UPPER(DESCRIPTION) LIKE UPPER('%' + @DESCRIPTION + '%')");
                sqlCommand.Parameters.AddWithValue("@DESCRIPTION", citySearch.Description);
            }

            StringBuilder orderBy = new StringBuilder();
            switch (citySearch.SortBy.Trim().ToUpper())
            {
                case "NAME":
                    orderBy.Append(" ORDER BY NAME");
                    break;
                case "DESCRIPTION":
                    orderBy.Append(" ORDER BY DESCRIPTION");
                    break;
                default:
                    orderBy.Append(" ORDER BY ID");
                    break;
            }
            string sortDirection = citySearch.SortDirection.Trim().ToUpper() == "DESC" ? " DESC" : " ASC";
            orderBy.Append(sortDirection);

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    ID,
                    NAME,
                    DESCRIPTION
                FROM CITIES
                WHERE 1=1 
            ");

            query.Append(filterQuery);
            query.Append(orderBy);

            query.Append(" OFFSET @OFFSET_ROWS_COUNT ROWS FETCH NEXT @FETCH_ROWS_COUNT ROWS ONLY");
            sqlCommand.Parameters.AddWithValue("@OFFSET_ROWS_COUNT", (citySearch.CurrentPage - 1) * citySearch.PerPage);
            sqlCommand.Parameters.AddWithValue("@FETCH_ROWS_COUNT", citySearch.PerPage);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            DataRowCollection dataRowCollection = dataTable.Rows;
            IList<City> cities = new List<City>();
            foreach (DataRow dataRow in dataRowCollection)
            {
                City city = new City(dataRow);
                cities.Add(city);
            }
            return cities;
        }

        private int GetCitiesCount(
            SqlConnection connection,
            CitySearch citySearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (!string.IsNullOrEmpty(citySearch.Name))
            {
                filterQuery.Append(" AND UPPER(NAME) LIKE UPPER('%'+ @NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@NAME", citySearch.Name);
            }

            if (!string.IsNullOrEmpty(citySearch.Description))
            {
                filterQuery.Append(" AND UPPER(DESCRIPTION) LIKE UPPER('%' + @DESCRIPTION + '%')");
                sqlCommand.Parameters.AddWithValue("@DESCRIPTION", citySearch.Description);
            }

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    COUNT(*)
                FROM CITIES
                WHERE 1=1
            ");

            query.Append(filterQuery);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            int.TryParse(
                sqlCommand.ExecuteScalar().ToString(),
                out int count);
            return count;
        }

        #endregion


        public IEnumerable<SelectItem> GetList()
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT 
                        ID,
                        NAME VALUE
                    FROM CITIES
                    ORDER BY NAME
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                DataRowCollection dataRowCollection = dataTable.Rows;
                IList<SelectItem> selectItems = new List<SelectItem>();

                foreach (DataRow dataRow in dataRowCollection)
                {
                    SelectItem selectItem = new SelectItem();
                    selectItem.Id = Convert.ToInt32(dataRow["ID"]);
                    selectItem.Value = Convert.ToString(dataRow["VALUE"]);
                    selectItems.Add(selectItem);
                }
                return selectItems;
            }
        }

        public City GetById(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT 
                        ID,
                        NAME,
                        DESCRIPTION
                    FROM CITIES
                    WHERE ID = @ID
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", id);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                if (dataTable.Rows.Count == 0)
                    return null;
                DataRow dataRow = dataTable.Rows[0];
                City city = new City(dataRow);
                return city;
            }
        }

        public bool CheckExistence(City city)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT
	                    ISNULL(COUNT(ID), 0)
                    FROM CITIES 
                    WHERE ID <> @ID AND UPPER(NAME) = UPPER(@NAME)
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", city.Id);
                sqlCommand.Parameters.AddWithValue("@NAME", city.Name);
                object value = sqlCommand.ExecuteScalar();
                int.TryParse(value.ToString(), out int result);
                return result > 0;
            }
        }

        public bool Insert(City city)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                        INSERT INTO CITIES(NAME, DESCRIPTION, CREATED_BY) 
                        VALUES(@NAME, @DESCRIPTION, @CREATED_BY) 
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@NAME", city.Name);
                    sqlCommand.Parameters.AddWithValue("@DESCRIPTION", city.Description);
                    sqlCommand.Parameters.AddWithValue("@CREATED_BY", city.CreatedBy);
                    int affectedRow = sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        public bool Update(City city)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                        UPDATE CITIES
                        SET 
	                        NAME = @NAME,
	                        DESCRIPTION = @DESCRIPTION,
	                        UPDATED_BY = @UPDATED_BY,
	                        UPDATED_DATE = @UPDATED_DATE
                        WHERE ID = @ID
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@NAME", city.Name);
                    sqlCommand.Parameters.AddWithValue("@DESCRIPTION", city.Description);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_BY", city.UpdatedBy);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_DATE", city.UpdatedDate);
                    sqlCommand.Parameters.AddWithValue("@ID", city.Id);
                    int affectedRow = sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Delete(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    DELETE CITIES
                    WHERE ID = @ID
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", id);
                int affectedRow = sqlCommand.ExecuteNonQuery();
                return affectedRow == 1;
            }
        }
    }
}
