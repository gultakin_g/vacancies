﻿using System.Collections.Generic;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Cities;

namespace Vacancies.Infrastructure.Repositories.Cities
{
    public interface ICityRepository
    {
        (IEnumerable<City>, int) FindList(CitySearch citySearch);
        IEnumerable<SelectItem> GetList();
        City GetById(int id);
        bool CheckExistence(City city);
        bool Insert(City city);
        bool Update(City city);
        bool Delete(int id);
    }
}
