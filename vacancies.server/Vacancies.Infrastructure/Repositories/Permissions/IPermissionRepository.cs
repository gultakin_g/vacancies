﻿using System;
using System.Collections.Generic;
using System.Text;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Permissions;

namespace Vacancies.Infrastructure.Repositories.Permissions
{
    public interface IPermissionRepository
    {
        (IEnumerable<Permission>, int) FindList(PermissionSearch permissionSearch);
        IEnumerable<SelectItem> GetList();
        Permission GetById(int id);
        bool CheckExistence(Permission permission);
        bool Insert(Permission permission);
        bool Update(Permission permission);
        bool Delete(int id);
    }
}
