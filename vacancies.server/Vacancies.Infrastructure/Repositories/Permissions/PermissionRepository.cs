﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Permissions;
using Vacancies.Infrastructure.Handlers;

namespace Vacancies.Infrastructure.Repositories.Permissions
{
    public class PermissionRepository : IPermissionRepository
    {
        private readonly IDbHandler _dbHandler;

        public PermissionRepository(IDbHandler dbHandler)
        {
            _dbHandler = dbHandler;
        }
        #region
        public (IEnumerable<Permission>, int) FindList(PermissionSearch permissionSearch)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                IEnumerable<Permission> permissions = GetPermissionsList(connection, permissionSearch);
                int permissionCount = GetPermissionsCount(connection, permissionSearch);
                return (permissions, permissionCount);
            }
        }

        private IEnumerable<Permission> GetPermissionsList(
            SqlConnection connection,
            PermissionSearch permissionSearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (!string.IsNullOrEmpty(permissionSearch.Name))
            {
                filterQuery.Append("AND UPPER(NAME) LIKE UPPER('%' + @NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@NAME", permissionSearch.Name);
            }

            if (!string.IsNullOrEmpty(permissionSearch.Description))
            {
                filterQuery.Append("AND UPPER(DESCRIPTION) LIKE UPPER('%' + @DESCRIPTION + '%')");
                sqlCommand.Parameters.AddWithValue("@DESCRIPTION", permissionSearch.Description);
            }

            StringBuilder orderBy = new StringBuilder();
            switch (permissionSearch.SortBy.Trim().ToUpper())
            {
                case "NAME":
                    orderBy.Append(" ORDER BY NAME");
                    break;
                case "DESCRIPTION":
                    orderBy.Append(" ORDER BY DESCRIPTION");
                    break;
                default:
                    orderBy.Append(" ORDER BY ID");
                    break;
            }
            string sortDirection = permissionSearch.SortDirection.Trim().ToUpper() == "DESC" ? " DESC" : " ASC";
            orderBy.Append(sortDirection);

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    ID,
                    NAME,
                    DESCRIPTION
                FROM PERMISSIONS
                WHERE 1=1 
            ");

            query.Append(filterQuery);
            query.Append(orderBy);

            query.Append(" OFFSET @OFFSET_ROWS_COUNT ROWS FETCH NEXT @FETCH_ROWS_COUNT ROWS ONLY");
            sqlCommand.Parameters.AddWithValue("@OFFSET_ROWS_COUNT", (permissionSearch.CurrentPage - 1) * permissionSearch.PerPage);
            sqlCommand.Parameters.AddWithValue("@FETCH_ROWS_COUNT", permissionSearch.PerPage);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            DataRowCollection dataRowCollection = dataTable.Rows;
            IList<Permission> permissions = new List<Permission>();
            foreach (DataRow dataRow in dataRowCollection)
            {
                Permission permission = new Permission(dataRow);
                permissions.Add(permission);
            }
            return permissions;
        }

        private int GetPermissionsCount(
            SqlConnection connection,
            PermissionSearch permissionSearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (!string.IsNullOrEmpty(permissionSearch.Name))
            {
                filterQuery.Append(" AND UPPER(NAME) LIKE UPPER('%'+ @NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@NAME", permissionSearch.Name);
            }

            if (!string.IsNullOrEmpty(permissionSearch.Description))
            {
                filterQuery.Append(" AND UPPER(DESCRIPTION) LIKE UPPER('%' + @DESCRIPTION + '%')");
                sqlCommand.Parameters.AddWithValue("@DESCRIPTION", permissionSearch.Description);
            }

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    COUNT(*)
                FROM PERMISSIONS
                WHERE 1=1
            ");

            query.Append(filterQuery);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            int.TryParse(
                sqlCommand.ExecuteScalar().ToString(),
                out int count);
            return count;
        }

        #endregion

        public IEnumerable<SelectItem> GetList()
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT 
                        ID,
                        NAME VALUE
                    FROM PERMISSIONS
                    ORDER BY NAME
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                DataRowCollection dataRowCollection = dataTable.Rows;
                IList<SelectItem> selectItems = new List<SelectItem>();

                foreach (DataRow dataRow in dataRowCollection)
                {
                    SelectItem selectItem = new SelectItem();
                    selectItem.Id = Convert.ToInt32(dataRow["ID"]);
                    selectItem.Value = Convert.ToString(dataRow["VALUE"]);
                    selectItems.Add(selectItem);
                }
                return selectItems;
            }
        }

        public Permission GetById(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT 
                        ID,
                        NAME,
                        DESCRIPTION
                    FROM PERMISSIONS
                    WHERE ID = @ID
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", id);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                if (dataTable.Rows.Count == 0)
                    return null;
                DataRow dataRow = dataTable.Rows[0];
                Permission permission = new Permission(dataRow);
                return permission;
            }
        }

        public bool CheckExistence(Permission permission)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT
	                    ISNULL(COUNT(ID), 0)
                    FROM PERMISSIONS 
                    WHERE ID <> @ID AND UPPER(NAME) = UPPER(@NAME)
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", permission.Id);
                sqlCommand.Parameters.AddWithValue("@NAME", permission.Name);
                object value = sqlCommand.ExecuteScalar();
                int.TryParse(value.ToString(), out int result);
                return result > 0;
            }
        }

        public bool Insert(Permission permission)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();

                    string query = @"
                        INSERT INTO PERMISSIONS (
                            NAME,
                            DESCRIPTION,
                            CREATED_BY
                        )
                        VALUES(
                            @NAME,
                            @DESCRIPTION,
                            @CREATED_BY
                        )
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@NAME", permission.Name);
                    sqlCommand.Parameters.AddWithValue("@DESCRIPTION", permission.Description);
                    sqlCommand.Parameters.AddWithValue("@CREATED_BY", permission.CreatedBy);
                    int affectedRow = sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Update(Permission permission)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();

                    string query = @"
                    UPDATE PERMISSIONS     
                    SET
                        NAME = @NAME,
                        DESCRIPTION = @DESCRIPTION,
                        UPDATED_BY = @UPDATED_BY,
                        UPDATED_DATE = @UPDATED_DATE
                    WHERE ID = @ID
                ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@NAME", permission.Name);
                    sqlCommand.Parameters.AddWithValue("@DESCRIPTION", permission.Description);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_BY", permission.UpdatedBy);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_DATE", permission.UpdatedDate);
                    sqlCommand.Parameters.AddWithValue("@ID", permission.Id);
                    int affectedRow = sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Delete(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    DELETE PERMISSIONS
                    WHERE ID = @ID
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", id);
                int affectedRow = sqlCommand.ExecuteNonQuery();
                return affectedRow == 1;
            }
        }
    }
}
