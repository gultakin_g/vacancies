﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Educations;
using Vacancies.Infrastructure.Handlers;

namespace Vacancies.Infrastructure.Repositories.Educations
{
    public class EducationRepository : IEducationRepository
    {

        private readonly IDbHandler _dbHandler;

        public EducationRepository(IDbHandler dbHandler)
        {
            _dbHandler = dbHandler;
        }

        #region
        public (IEnumerable<Education>, int) FindList(EducationSearch educationSearch)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                IEnumerable<Education> educations = GetEducationsList(connection, educationSearch);
                int educationCount = GetEducationsCount(connection, educationSearch);
                return (educations, educationCount);
            }
        }

        private IEnumerable<Education> GetEducationsList(
        SqlConnection connection,
        EducationSearch educationSearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (!string.IsNullOrEmpty(educationSearch.Name))
            {
                filterQuery.Append("AND UPPER(NAME) LIKE UPPER('%' + @NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@NAME", educationSearch.Name);
            }

            if (!string.IsNullOrEmpty(educationSearch.Description))
            {
                filterQuery.Append("AND UPPER(DESCRIPTION) LIKE UPPER('%' + @DESCRIPTION + '%')");
                sqlCommand.Parameters.AddWithValue("@DESCRIPTION", educationSearch.Description);
            }

            StringBuilder orderBy = new StringBuilder();
            switch (educationSearch.SortBy.Trim().ToUpper())
            {
                case "NAME":
                    orderBy.Append(" ORDER BY NAME");
                    break;
                case "DESCRIPTION":
                    orderBy.Append(" ORDER BY DESCRIPTION");
                    break;
                default:
                    orderBy.Append(" ORDER BY ID");
                    break;
            }
            string sortDirection = educationSearch.SortDirection.Trim().ToUpper() == "DESC" ? " DESC" : " ASC";
            orderBy.Append(sortDirection);

            StringBuilder query = new StringBuilder(@"
                    SELECT 
                        ID,
                        NAME,
                        DESCRIPTION
                    FROM EDUCATIONS
                    WHERE 1=1 
                ");

            query.Append(filterQuery);
            query.Append(orderBy);

            query.Append(" OFFSET @OFFSET_ROWS_COUNT ROWS FETCH NEXT @FETCH_ROWS_COUNT ROWS ONLY");
            sqlCommand.Parameters.AddWithValue("@OFFSET_ROWS_COUNT", (educationSearch.CurrentPage - 1) * educationSearch.PerPage);
            sqlCommand.Parameters.AddWithValue("@FETCH_ROWS_COUNT", educationSearch.PerPage);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            DataRowCollection dataRowCollection = dataTable.Rows;
            IList<Education> educations = new List<Education>();
            foreach (DataRow dataRow in dataRowCollection)
            {
                Education education = new Education(dataRow);
                educations.Add(education);
            }
            return educations;
        }

        private int GetEducationsCount(
            SqlConnection connection,
            EducationSearch educationSearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (!string.IsNullOrEmpty(educationSearch.Name))
            {
                filterQuery.Append(" AND UPPER(NAME) LIKE UPPER('%'+ @NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@NAME", educationSearch.Name);
            }

            if (!string.IsNullOrEmpty(educationSearch.Description))
            {
                filterQuery.Append(" AND UPPER(DESCRIPTION) LIKE UPPER('%' + @DESCRIPTION + '%')");
                sqlCommand.Parameters.AddWithValue("@DESCRIPTION", educationSearch.Description);
            }

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    COUNT(*)
                FROM EDUCATIONS
                WHERE 1=1
            ");

            query.Append(filterQuery);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            int.TryParse(
                sqlCommand.ExecuteScalar().ToString(),
                out int count);
            return count;
        }

        #endregion

        public IEnumerable<SelectItem> GetList()
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT
                        ID,
                        NAME VALUE
                    FROM EDUCATIONS
                    ORDER BY NAME
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                DataRowCollection dataRowCollection = dataTable.Rows;
                IList<SelectItem> selectItems = new List<SelectItem>();
                foreach (DataRow dataRow in dataRowCollection)
                {
                    SelectItem selectItem = new SelectItem();
                    selectItem.Id = Convert.ToInt32(dataRow["ID"]);
                    selectItem.Value = Convert.ToString(dataRow["VALUE"]);
                    selectItems.Add(selectItem);
                }
                return selectItems;
            }
        }

        public Education GetById(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT 
                        ID,
                        NAME,
                        DESCRIPTION
                    FROM EDUCATIONS
                    WHERE ID = @ID
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", id);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                if (dataTable.Rows.Count == 0)
                    return null;
                DataRow dataRow = dataTable.Rows[0];
                Education education = new Education(dataRow);
                return education;
            }
        }

        public bool CheckExistence(Education education)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT
                       ISNULL(COUNT(ID),0)
                    FROM EDUCATIONS
                    WHERE ID <> @ID AND UPPER(NAME) = UPPER(@NAME)
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", education.Id);
                sqlCommand.Parameters.AddWithValue("@NAME", education.Name);
                object value = sqlCommand.ExecuteScalar();
                int.TryParse(value.ToString(), out int result);
                return result > 0;
            }
        }

        public bool Insert(Education education)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                        INSERT INTO EDUCATIONS(NAME, DESCRIPTION, CREATED_BY) 
                        VALUES(@NAME, @DESCRIPTION, @CREATED_BY); 
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@NAME", education.Name);
                    sqlCommand.Parameters.AddWithValue("@DESCRIPTION", education.Description);
                    sqlCommand.Parameters.AddWithValue("@CREATED_BY", education.CreatedBy);
                    int affectedRow = sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Update(Education education)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                        UPDATE EDUCATIONS 
                        SET
                            NAME = @NAME,
                            DESCRIPTION = @DESCRIPTION,
                            UPDATED_BY = @UPDATED_BY,
                            UPDATED_DATE = @UPDATED_DATE
                        WHERE ID = @ID
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@NAME", education.Name);
                    sqlCommand.Parameters.AddWithValue("@DESCRIPTION", education.Description);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_BY", education.UpdatedBy);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_DATE", education.UpdatedDate);
                    sqlCommand.Parameters.AddWithValue("@ID", education.Id);
                    int affectedRow = sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Delete(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    DELETE EDUCATIONS
                    WHERE ID = @ID
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", id);
                int affectedRow = sqlCommand.ExecuteNonQuery();
                return affectedRow == 1;
            }
        }
    }
}
