﻿using System;
using System.Collections.Generic;
using System.Text;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Educations;

namespace Vacancies.Infrastructure.Repositories.Educations
{
    public interface IEducationRepository
    {
        (IEnumerable<Education>, int) FindList(EducationSearch educationSearch);
        IEnumerable<SelectItem> GetList();
        Education GetById(int id);
        bool CheckExistence(Education education);
        bool Insert(Education education);
        bool Update(Education education);
        bool Delete(int id);
    }
}
