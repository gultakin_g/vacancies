﻿using System.Collections.Generic;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Users;

namespace Vacancies.Infrastructure.Repositories.Users
{
    public interface IUserRepository
    {
        #region Auth
        (User, string) Authenticate(string userName, string password);
        #endregion

        #region Users

        (IEnumerable<User>, int) FindList(UserSearch userSearch);
        IEnumerable<SelectItem> GetList();
        User GetById(int id);
        bool CheckExistenceUserName(User user);
        bool CheckExistenceEmail(User user);
        bool Insert(User user);
        bool Update(User user);
        bool Delete(int id);

        #endregion
    }
}
