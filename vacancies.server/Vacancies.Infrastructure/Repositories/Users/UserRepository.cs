﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Users;
using Vacancies.Infrastructure.Handlers;

namespace Vacancies.Infrastructure.Repositories.Users
{
    public class UserRepository : IUserRepository
    {
        private readonly IDbHandler _dbHandler;

        public UserRepository(IDbHandler dbHandler)
        {
            _dbHandler = dbHandler;
        }

        #region Auth
        public (User, string) Authenticate(string userName, string password)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT 
                        ID,
                        IMAGE_URL,
                        FIRST_NAME,
                        LAST_NAME,
                        MIDDLE_NAME,
                        GENDER,
                        BIRTH_DATE,
                        EMAIL,
                        PHONE_NUMBER,
                        USER_NAME,
                        PASSWORD
                    FROM USERS
                    WHERE USER_NAME = @USER_NAME
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@USER_NAME", userName);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                if (dataTable.Rows.Count == 0)
                {
                    return (null, "Username and/or Password are incorrect!");
                }
                DataRow dataRow = dataTable.Rows[0];
                User user = new User(dataRow);
                if(user.Password != password)
                {
                    return (null, "Username and/or Password are incorrect!");
                }
                user.UserRoles = GetUserRoles(connection, user.Id);
                return (user, null);
            }
        }

        #endregion

        #region Users

        #region FindList
        public (IEnumerable<User>, int) FindList(UserSearch userSearch)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                IEnumerable<User> users = GetUsersList(connection, userSearch);
                int usersCount = GetUsersCount(connection, userSearch);
                return (users, usersCount);
            }
        }

        private IEnumerable<User> GetUsersList(SqlConnection connection, UserSearch userSearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            #region filterBy
            if (!string.IsNullOrEmpty(userSearch.FirstName))
            {
                filterQuery.Append("AND UPPER(FIRST_NAME) LIKE UPPER('%' + @FIRST_NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@FIRST_NAME", userSearch.FirstName);
            }

            if (!string.IsNullOrEmpty(userSearch.LastName))
            {
                filterQuery.Append("AND UPPER(LAST_NAME) LIKE UPPER('%' + @LAST_NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@LAST_NAME", userSearch.LastName);
            }

            if (!string.IsNullOrEmpty(userSearch.MiddleName))
            {
                filterQuery.Append("AND UPPER(MIDDLE_NAME) LIKE UPPER('%' + @MIDDLE_NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@MIDDLE_NAME", userSearch.MiddleName);
            }

            if (userSearch.BirthDate.HasValue)
            {
                filterQuery.Append("AND UPPER(BIRTH_DATE) LIKE UPPER('%' + @BIRTH_DATE + '%')");
                sqlCommand.Parameters.AddWithValue("@BIRTH_DATE", userSearch.BirthDate);
            }

            if (!string.IsNullOrEmpty(userSearch.Email))
            {
                filterQuery.Append("AND UPPER(EMAIL) LIKE UPPER('%' + @EMAIL + '%')");
                sqlCommand.Parameters.AddWithValue("@EMAIL", userSearch.Email);

            }

            if (!string.IsNullOrEmpty(userSearch.PhoneNumber))
            {
                filterQuery.Append("AND UPPER(PHONE_NUMBER) LIKE UPPER('%' + @PHONE_NUMBER + '%')");
                sqlCommand.Parameters.AddWithValue("@PHONE_NUMBER", userSearch.PhoneNumber);
            }

            if (!string.IsNullOrEmpty(userSearch.UserName))
            {
                filterQuery.Append("AND UPPER(USER_NAME) LIKE UPPER('%' + @USER_NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@USER_NAME", userSearch.UserName);
            }

            #endregion

            StringBuilder orderBy = new StringBuilder();
            switch (userSearch.SortBy.Trim().ToUpper())
            {
                case "FIRST_NAME":
                    orderBy.Append(" ORDER BY FIRST_NAME");
                    break;

                case "LAST_NAME":
                    orderBy.Append(" ORDER BY LAST_NAME");
                    break;

                case "MIDDLE_NAME":
                    orderBy.Append(" ORDER BY MIDDLE_NAME");
                    break;

                case "BIRTH_DATE":
                    orderBy.Append(" ORDER BY BIRTH_DATE");
                    break;

                case "EMAIL":
                    orderBy.Append(" ORDER BY EMAIL");
                    break;

                case "PHONE_NUMBER":
                    orderBy.Append(" ORDER BY PHONE_NUMBER");
                    break;

                case "USER_NAME":
                    orderBy.Append(" ORDER BY USER_NAME");
                    break;

                default:
                    orderBy.Append(" ORDER BY ID");
                    break;
            }

            string sortDirection = userSearch.SortDirection.Trim().ToUpper() == "DESC" ? " DESC" : " ASC";
            orderBy.Append(sortDirection);

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    ID,
                    FIRST_NAME,
                    LAST_NAME,
                    MIDDLE_NAME,
                    GENDER,
                    BIRTH_DATE,
                    EMAIL,
                    PHONE_NUMBER,
                    USER_NAME
                FROM USERS
                WHERE 1 = 1
            ");

            query.Append(filterQuery);
            query.Append(orderBy);

            query.Append(" OFFSET @OFFSET_ROWS_COUNT ROWS FETCH NEXT @FETCH_ROWS_COUNT ROWS ONLY");
            sqlCommand.Parameters.AddWithValue("@OFFSET_ROWS_COUNT", (userSearch.CurrentPage - 1) * userSearch.PerPage);
            sqlCommand.Parameters.AddWithValue("@FETCH_ROWS_COUNT", userSearch.PerPage);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            DataRowCollection dataRowCollection = dataTable.Rows;
            IList<User> users = new List<User>();
            foreach (DataRow dataRow in dataRowCollection)
            {
                User user = new User(dataRow);
                users.Add(user);
            }
            return users;
        }

        private int GetUsersCount(SqlConnection connection, UserSearch userSearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            #region filterBy
            if (!string.IsNullOrEmpty(userSearch.FirstName))
            {
                filterQuery.Append("AND UPPER(FIRST_NAME) LIKE UPPER('%' + @FIRST_NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@FIRST_NAME", userSearch.FirstName);
            }

            if (!string.IsNullOrEmpty(userSearch.LastName))
            {
                filterQuery.Append("AND UPPER(LAST_NAME) LIKE UPPER('%' + @LAST_NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@LAST_NAME", userSearch.LastName);
            }

            if (!string.IsNullOrEmpty(userSearch.MiddleName))
            {
                filterQuery.Append("AND UPPER(MIDDLE_NAME) LIKE UPPER('%' + @MIDDLE_NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@MIDDLE_NAME", userSearch.MiddleName);
            }

            if (userSearch.BirthDate.HasValue)
            {
                filterQuery.Append("AND BIRTH_DATE = @BIRTH_DATE");
                sqlCommand.Parameters.AddWithValue("@BIRTH_DATE", userSearch.BirthDate);
            }

            if (!string.IsNullOrEmpty(userSearch.Email))
            {
                filterQuery.Append("AND UPPER(EMAIL) LIKE UPPER('%' + @EMAIL + '%')");
                sqlCommand.Parameters.AddWithValue("@EMAIL", userSearch.Email);

            }

            if (!string.IsNullOrEmpty(userSearch.PhoneNumber))
            {
                filterQuery.Append("AND PHONE_NUMBER = @PHONE_NUMBER");
                sqlCommand.Parameters.AddWithValue("@PHONE_NUMBER", userSearch.PhoneNumber);
            }

            if (!string.IsNullOrEmpty(userSearch.UserName))
            {
                filterQuery.Append("AND UPPER(USER_NAME) LIKE UPPER('%' + @USER_NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@USER_NAME", userSearch.UserName);
            }

            #endregion

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    COUNT(*)
                FROM USERS
                WHERE 1=1
            ");

            query.Append(filterQuery);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            int.TryParse(
                sqlCommand.ExecuteScalar().ToString(),
                out int count);
            return count;
        }
        #endregion


        public IEnumerable<SelectItem> GetList()
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT 
                        ID,
                        CONCAT(FIRST_NAME, ' ', LAST_NAME, ' ', MIDDLE_NAME) VALUE
                    FROM USERS
                    ORDER BY 
	                    FIRST_NAME,
	                    LAST_NAME,
	                    MIDDLE_NAME
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                DataRowCollection dataRowCollection = dataTable.Rows;
                IList<SelectItem> selectItems = new List<SelectItem>();

                foreach (DataRow dataRow in dataRowCollection)
                {
                    SelectItem selectItem = new SelectItem();
                    selectItem.Id = Convert.ToInt32(dataRow["ID"]);
                    selectItem.Value = Convert.ToString(dataRow["VALUE"]);
                    selectItems.Add(selectItem);
                }
                return selectItems;
            }
        }

        public User GetById(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT 
                        ID,
                        IMAGE_URL,
                        FIRST_NAME,
                        LAST_NAME,
                        MIDDLE_NAME,
                        GENDER,
                        BIRTH_DATE,
                        EMAIL,
                        PHONE_NUMBER,
                        USER_NAME,
                        PASSWORD
                    FROM USERS
                    WHERE ID = @ID
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", id);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                if (dataTable.Rows.Count == 0)
                    return null;
                DataRow dataRow = dataTable.Rows[0];
                User user = new User(dataRow);
                user.UserRoles = GetUserRoles(connection, id);
                return user;
            }
        }

        public bool CheckExistenceUserName(User user)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT
	                    ISNULL(COUNT(ID), 0)
                    FROM USERS 
                    WHERE ID <> @ID AND UPPER(USER_NAME) = UPPER(@USER_NAME)
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", user.Id);
                sqlCommand.Parameters.AddWithValue("@USER_NAME", user.UserName);
                object value = sqlCommand.ExecuteScalar();
                int.TryParse(value.ToString(), out int result);
                return result > 0;
            }
        }

        public bool CheckExistenceEmail(User user)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT
	                    ISNULL(COUNT(ID), 0)
                    FROM USERS 
                    WHERE ID <> @ID AND UPPER(EMAIL) = UPPER(@EMAIL)
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", user.Id);
                sqlCommand.Parameters.AddWithValue("@EMAIL", user.Email);
                object value = sqlCommand.ExecuteScalar();
                int.TryParse(value.ToString(), out int result);
                return result > 0;
            }
        }

        public bool Insert(User user)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                        INSERT INTO USERS(
                            IMAGE_URL,
                            FIRST_NAME,
                            LAST_NAME,
                            MIDDLE_NAME,
                            GENDER,
                            BIRTH_DATE,
                            EMAIL,
                            PHONE_NUMBER,
                            USER_NAME,
                            PASSWORD,
                            CREATED_BY
                        )
                        VALUES(
                            @IMAGE_URL,
                            @FIRST_NAME,
                            @LAST_NAME,
                            @MIDDLE_NAME,
                            @GENDER,
                            @BIRTH_DATE,
                            @EMAIL,
                            @PHONE_NUMBER,
                            @USER_NAME,
                            @PASSWORD,
                            @CREATED_BY
                        )
                        SELECT @ID = SCOPE_IDENTITY();
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int)
                    {
                        Direction = ParameterDirection.Output
                    });
                    sqlCommand.Parameters.AddWithValue("@IMAGE_URL", user.ImageUrl);
                    sqlCommand.Parameters.AddWithValue("@FIRST_NAME", user.FirstName);
                    sqlCommand.Parameters.AddWithValue("@LAST_NAME", user.LastName);
                    sqlCommand.Parameters.AddWithValue("@MIDDLE_NAME", user.MiddleName);
                    sqlCommand.Parameters.AddWithValue("@GENDER", user.Gender);
                    sqlCommand.Parameters.AddWithValue("@BIRTH_DATE", user.BirthDate);
                    sqlCommand.Parameters.AddWithValue("@EMAIL", user.Email);
                    sqlCommand.Parameters.AddWithValue("@PHONE_NUMBER", user.PhoneNumber);
                    sqlCommand.Parameters.AddWithValue("@USER_NAME", user.UserName);
                    sqlCommand.Parameters.AddWithValue("@PASSWORD", user.Password);
                    sqlCommand.Parameters.AddWithValue("@CREATED_BY", user.CreatedBy);
                    int affectedRow = sqlCommand.ExecuteNonQuery();

                    int userId = 0;
                    if (sqlCommand.Parameters["@ID"].Value != DBNull.Value)
                        userId = Convert.ToInt32(sqlCommand.Parameters["@ID"].Value);

                    if (userId > 0)
                    {
                        user.UserRoles = user.UserRoles.Select(e => new UserRole()
                        {
                            UserId = userId,
                            RoleId = e.RoleId
                        });
                        InsertUserRoles(
                            user.UserRoles,
                            connection,
                            transaction);
                    }
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Update(User user)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    string query = @"
                        UPDATE USERS
                        SET 
	                        FIRST_NAME = @FIRST_NAME,
                            LAST_NAME = @LAST_NAME,
                            MIDDLE_NAME = @MIDDLE_NAME,
                            GENDER = @GENDER,
                            BIRTH_DATE = @BIRTH_DATE,
                            EMAIL = @EMAIL,
                            PHONE_NUMBER = @PHONE_NUMBER,
                            USER_NAME = @USER_NAME,
                            PASSWORD = @PASSWORD,
                            UPDATED_BY = @UPDATED_BY,
                            UPDATED_DATE = @UPDATED_DATE
                        WHERE ID = @ID
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@FIRST_NAME", user.FirstName);
                    sqlCommand.Parameters.AddWithValue("@LAST_NAME", user.LastName);
                    sqlCommand.Parameters.AddWithValue("@MIDDLE_NAME", user.MiddleName);
                    sqlCommand.Parameters.AddWithValue("@GENDER", user.Gender);
                    sqlCommand.Parameters.AddWithValue("@BIRTH_DATE", user.BirthDate);
                    sqlCommand.Parameters.AddWithValue("@EMAIL", user.Email);
                    sqlCommand.Parameters.AddWithValue("@PHONE_NUMBER", user.PhoneNumber);
                    sqlCommand.Parameters.AddWithValue("@USER_NAME", user.UserName);
                    sqlCommand.Parameters.AddWithValue("@PASSWORD", user.Password);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_BY", user.UpdatedBy);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_DATE", user.UpdatedDate);
                    sqlCommand.Parameters.AddWithValue("@ID", user.Id);
                    int affectedRow = sqlCommand.ExecuteNonQuery();

                    UpdateUserRoles(
                        user.UserRoles,
                        connection,
                        transaction);

                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();

                    DeleteUserRoles(connection, transaction, id);

                    string query = @"
                        DELETE USERS
                        WHERE ID = @ID
                    ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@ID", id);
                    int affectedRow = sqlCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region UserRoles

        private IEnumerable<UserRole> GetUserRoles(
            SqlConnection connection,
            int userId)
        {
            string query = @"
                SELECT 
                    USER_ID,
                    ROLE_ID
                FROM USER_ROLES
                WHERE USER_ID = @USER_ID
            ";
            SqlCommand sqlCommand = new SqlCommand(query, connection);
            sqlCommand.Parameters.AddWithValue("@USER_ID", userId);
            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            DataRowCollection dataRowCollection = dataTable.Rows;
            IList<UserRole> userRoles = new List<UserRole>();
            foreach (DataRow dataRow in dataRowCollection)
            {
                UserRole userRole = new UserRole(dataRow);
                userRoles.Add(userRole);
            }
            return userRoles;
        }

        private void InsertUserRoles(
            IEnumerable<UserRole> userRoles,
            SqlConnection connection,
            SqlTransaction transaction)
        {
            DataTable destinationTable = new DataTable();
            destinationTable.Columns.Add("USER_ID", typeof(int));
            destinationTable.Columns.Add("ROLE_ID", typeof(int));
            foreach (UserRole userRole in userRoles)
            {
                DataRow dataRow = destinationTable.NewRow();
                dataRow["USER_ID"] = userRole.UserId;
                dataRow["ROLE_ID"] = userRole.RoleId;
                destinationTable.Rows.Add(dataRow);
            }
            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(
                connection,
                SqlBulkCopyOptions.KeepIdentity,
                transaction))
            {
                sqlBulkCopy.BatchSize = userRoles.Count();
                sqlBulkCopy.DestinationTableName = "USER_ROLES";
                sqlBulkCopy.WriteToServer(destinationTable);
            }
        }

        private void UpdateUserRoles(
            IEnumerable<UserRole> userRoles,
            SqlConnection connection,
            SqlTransaction transaction
            )
        {
            //delete old unused userRoles
            string query = @"
                DELETE USER_ROLES
                WHERE  USER_ID = @USER_ID
            ";
            SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
            sqlCommand.Parameters.AddWithValue("@USER_ID", userRoles.FirstOrDefault()?.UserId);
            sqlCommand.ExecuteNonQuery();

            //insert or update new rolePermissions 
            DataTable destinationTable = new DataTable();
            destinationTable.Columns.Add("USER_ID", typeof(int));
            destinationTable.Columns.Add("ROLE_ID", typeof(int));
            foreach (UserRole userRole in userRoles)
            {
                DataRow dataRow = destinationTable.NewRow();
                dataRow["USER_ID"] = userRole.UserId;
                dataRow["ROLE_ID"] = userRole.RoleId;
                destinationTable.Rows.Add(dataRow);
            }

            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(
                connection,
                SqlBulkCopyOptions.Default,
                transaction))
            {
                sqlBulkCopy.BatchSize = userRoles.Count();
                sqlBulkCopy.DestinationTableName = "USER_ROLES";
                sqlBulkCopy.WriteToServer(destinationTable);
            }
        }

        private int DeleteUserRoles(
            SqlConnection connection,
            SqlTransaction transaction,
            int userId)
        {
            string query = @"
                DELETE USER_ROLES
                WHERE USER_ID = @USER_ID
            ";
            SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
            sqlCommand.Parameters.AddWithValue("@USER_ID", userId);
            int affectedRows = sqlCommand.ExecuteNonQuery();
            return affectedRows;
        }
        #endregion
    }
}