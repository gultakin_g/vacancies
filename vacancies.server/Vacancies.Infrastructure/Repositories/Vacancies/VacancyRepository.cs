﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Vacancies;
using Vacancies.Infrastructure.Handlers;

namespace Vacancies.Infrastructure.Repositories.Vacancies
{
    public class VacancyRepository : IVacancyRepository
    {
        private readonly IDbHandler _dbHandler;

        public VacancyRepository(IDbHandler dbHandler)
        {
            _dbHandler = dbHandler;
        }

        #region Vacancies

        #region FindList
        public (IEnumerable<Vacancy>, int) FindList(VacancySearch vacancySearch)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                IEnumerable<Vacancy> vacancies = GetVacanciesList(connection, vacancySearch);
                int vacanciesCount = GetVacanciesCount(connection, vacancySearch);
                return (vacancies, vacanciesCount);
            }
        }

        private IEnumerable<Vacancy> GetVacanciesList(
            SqlConnection connection,
            VacancySearch vacancySearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            if (!string.IsNullOrEmpty(vacancySearch.Email))
            {
                filterQuery.Append(" AND UPPER(V.EMAIL) LIKE UPPER('%' + @EMAIL + '%')");
                sqlCommand.Parameters.AddWithValue("@EMAIL", vacancySearch.Email);
            }

            if (!string.IsNullOrEmpty(vacancySearch.Position))
            {
                filterQuery.Append(" AND UPPER(V.POSITION) LIKE UPPER('%' + @POSITION + '%')");
                sqlCommand.Parameters.AddWithValue("@POSITION", vacancySearch.Position);
            }

            if (!string.IsNullOrEmpty(vacancySearch.CompanyName))
            {
                filterQuery.Append(" AND UPPER(V.COMPANY_NAME) LIKE UPPER('%' + @COMPANY_NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@COMPANY_NAME", vacancySearch.CompanyName);
            }

            if (!string.IsNullOrEmpty(vacancySearch.RelevantPerson))
            {
                filterQuery.Append(" AND UPPER(V.RELEVANT_PERSON) LIKE UPPER('%' + @RELEVANT_PERSON + '%')");
                sqlCommand.Parameters.AddWithValue("@RELEVANT_PERSON", vacancySearch.RelevantPerson);
            }

            if (!string.IsNullOrEmpty(vacancySearch.CandidateRequirements))
            {
                filterQuery.Append(" AND UPPER(V.CONDIDATE_REQUIREMENTS) LIKE UPPER('%' + @CONDIDATE_REQUIREMENTS + '%')");
                sqlCommand.Parameters.AddWithValue("@CONDIDATE_REQUIREMENTS", vacancySearch.CandidateRequirements);
            }

            if (!string.IsNullOrEmpty(vacancySearch.AboutWork))
            {
                filterQuery.Append(" AND UPPER(V.ABOUT_WORK) LIKE UPPER('%' + @ABOUT_WORK + '%')");
                sqlCommand.Parameters.AddWithValue("@ABOUT_WORK", vacancySearch.AboutWork);
            }

            if (vacancySearch.ExpiryDate.HasValue)
            {
                filterQuery.Append(" AND V.EXPIRY_DATE = @EXPIRY_DATE");
                sqlCommand.Parameters.AddWithValue("@EXPIRY_DATE", vacancySearch.ExpiryDate);
            }

            if (vacancySearch.CategoryItemId.HasValue)
            {
                filterQuery.Append(" AND V.CATEGORY_ITEM_ID = @CATEGORY_ITEM_ID");
                sqlCommand.Parameters.AddWithValue("@CATEGORY_ITEM_ID", vacancySearch.CategoryItemId);
            }

            if (vacancySearch.CityId.HasValue)
            {
                filterQuery.Append(" AND V.CITY_ID = @CITY_ID");
                sqlCommand.Parameters.AddWithValue("@CITY_ID", vacancySearch.CityId);
            }

            if (vacancySearch.MinSalaryId.HasValue)
            {
                filterQuery.Append(" AND V.MIN_SALARY_ID = @MIN_SALARY_ID");
                sqlCommand.Parameters.AddWithValue("@MIN_SALARY_ID", vacancySearch.MinSalaryId);
            }

            if (vacancySearch.MaxSalaryId.HasValue)
            {
                filterQuery.Append(" AND V.MAX_SALARY_ID = @MAX_SALARY_ID");
                sqlCommand.Parameters.AddWithValue("@MAX_SALARY_ID", vacancySearch.MinSalaryId);
            }

            if (vacancySearch.MinAgeId.HasValue)
            {
                filterQuery.Append("AND V.MIN_AGE_ID = @MIN_AGE_ID");
                sqlCommand.Parameters.AddWithValue("@MIN_AGE_ID", vacancySearch.MinAgeId);
            }

            if (vacancySearch.MaxAgeId.HasValue)
            {
                filterQuery.Append(" AND V.MAX_AGE_ID = @MAX_AGE_ID");
                sqlCommand.Parameters.AddWithValue("@MAX_AGE_ID", vacancySearch.MaxAgeId);
            }

            if (vacancySearch.EducationId.HasValue)
            {
                filterQuery.Append(" AND V.EDUCATION_ID = @EDUCATION_ID");
                sqlCommand.Parameters.AddWithValue("@EDUCATION_ID", vacancySearch.EducationId);
            }

            if (vacancySearch.ExperienceId.HasValue)
            {
                filterQuery.Append(" AND V.EXPERIENCE_ID = @EXPERIENCE_ID");
                sqlCommand.Parameters.AddWithValue("@EXPERIENCE_ID", vacancySearch.ExperienceId);
            }

            StringBuilder orderBy = new StringBuilder();

            switch (vacancySearch.SortBy.Trim().ToUpper())
            {
                case "EMAIL":
                    orderBy.Append(" ORDER BY V.EMAIL");
                    break;

                case "POSITION":
                    orderBy.Append(" ORDER BY V.POSITION");
                    break;

                case "COMPANY_NAME":
                    orderBy.Append(" ORDER BY V.COMPANY_NAME");
                    break;

                case "RELEVANT_PERSON":
                    orderBy.Append(" ORDER BY V.RELEVANT_PERSON");
                    break;

                case "CONDIDATE_REQUIREMENTS":
                    orderBy.Append(" ORDER BY V.CONDIDATE_REQUIREMENTS");
                    break;

                case "ABOUT_WORK":
                    orderBy.Append(" ORDER BY V.ABOUT_WORK");
                    break;

                case "EXPIRY_DATE":
                    orderBy.Append(" ORDER BY V.EXPIRY_DATE");
                    break;


                default:
                    orderBy.Append(" ORDER BY V.ID");
                    break;
            }

            string sortDirection = vacancySearch.SortDirection.Trim().ToUpper() == "DESC" ? " DESC" : " ASC";
            orderBy.Append(sortDirection);

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    V.ID,
                    V.EMAIL,
                    V.POSITION,
                    V.COMPANY_NAME,
                    V.RELEVANT_PERSON,
                    V.CONDIDATE_REQUIREMENTS,
                    V.ABOUT_WORK,
                    V.EXPIRY_DATE,
                    V.CATEGORY_ITEM_ID,
                    CI.NAME CATEGORY_ITEM_NAME,
                    V.CITY_ID,
                    C.NAME CITY_NAME,
                    V.MIN_SALARY_ID,
                    MIN_S.AMOUNT MIN_SALARY_AMOUNT,
                    V.MAX_SALARY_ID,
                    MAX_S.AMOUNT MAX_SALARY_AMOUNT
                    V.MIN_AGE_ID,
                    MIN_A.AMOUNT MIN_AGE_AMOUNT,
                    V.MAX_AGE_ID,
                    MAX_A.AMOUNT MAX_AGE_AMOUNT,
                    V.EDUCATION_ID,
                    EDU.NAME EDUCATION_NAME,
                    V.EXPERIENCE_ID,
                    E.NAME EXPERIENCE_NAME
                FROM VACANCIES V
                INNER JOIN CATEGORY_ITEMS CI
                ON CI.ID = V.CATEGORY_ITEM_ID
                INNER JOIN CITIES C
                ON C.ID = V.CITY_ID
                INNER JOIN SALARIES MIN_S
                ON MIN_S.ID = V.MIN_SALARY_ID
                INNER JOIN SALARIES MAX_S
                ON MAX_S.ID = V.MAX_SALARY_ID
                INNER JOIN AGES MIN_A
                ON MIN_A.ID = V.MIN_AGE_ID
                INNER JOIN AGES MAX_A
                ON MAX_A.ID = V.MAX_AGE_ID
                INNER JOIN EDUCATIONS EDU
                ON EDU.ID = V.EDUCATION_ID
                INNER JOIN EXPERIENCES E
                ON E.ID = V.EXPERIENCE_ID
                WHERE V.IS_DELETED = 0 
            ");

            query.Append(filterQuery);
            query.Append(orderBy);

            query.Append(" OFFSET @OFFSET_ROWS_COUNT ROWS FETCH NEXT @FETCH_ROWS_COUNT ROWS ONLY");
            sqlCommand.Parameters.AddWithValue("@OFFSET_ROWS_COUNT", (vacancySearch.CurrentPage - 1) * vacancySearch.PerPage);
            sqlCommand.Parameters.AddWithValue("@FETCH_ROWS_COUNT", vacancySearch.PerPage);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            DataRowCollection dataRowCollection = dataTable.Rows;
            IList<Vacancy> vacancies = new List<Vacancy>();
            foreach (DataRow dataRow in dataRowCollection)
            {
                Vacancy vacancy = new Vacancy(dataRow);
                vacancies.Add(vacancy);
            }
            return vacancies;
        }

        private int GetVacanciesCount(
            SqlConnection connection,
            VacancySearch vacancySearch)
        {
            SqlCommand sqlCommand = new SqlCommand();

            StringBuilder filterQuery = new StringBuilder();

            #region FilterBy
            if (!string.IsNullOrEmpty(vacancySearch.Email))
            {
                filterQuery.Append(" AND UPPER(EMAIL) LIKE UPPER('%' + @EMAIL + '%')");
                sqlCommand.Parameters.AddWithValue("@EMAIL", vacancySearch.Email);
            }

            if (!string.IsNullOrEmpty(vacancySearch.Position))
            {
                filterQuery.Append(" AND UPPER(POSITION) LIKE UPPER('%' + @POSITION + '%')");
                sqlCommand.Parameters.AddWithValue("@POSITION", vacancySearch.Position);
            }

            if (!string.IsNullOrEmpty(vacancySearch.CompanyName))
            {
                filterQuery.Append(" AND UPPER(COMPANY_NAME) LIKE UPPER('%' + @COMPANY_NAME + '%')");
                sqlCommand.Parameters.AddWithValue("@COMPANY_NAME", vacancySearch.CompanyName);
            }

            if (!string.IsNullOrEmpty(vacancySearch.RelevantPerson))
            {
                filterQuery.Append(" AND UPPER(RELEVANT_PERSON) LIKE UPPER('%' + @RELEVANT_PERSON + '%')");
                sqlCommand.Parameters.AddWithValue("@RELEVANT_PERSON", vacancySearch.RelevantPerson);
            }

            if (!string.IsNullOrEmpty(vacancySearch.CandidateRequirements))
            {
                filterQuery.Append("AND UPPER(CONDIDATE_REQUIREMENTS) LIKE UPPER('%' + @CONDIDATE_REQUIREMENTS + '%')");
                sqlCommand.Parameters.AddWithValue("@CONDIDATE_REQUIREMENTS", vacancySearch.CandidateRequirements);
            }

            if (!string.IsNullOrEmpty(vacancySearch.AboutWork))
            {
                filterQuery.Append(" AND UPPER(ABOUT_WORK) LIKE UPPER('%' + @ABOUT_WORK + '%')");
                sqlCommand.Parameters.AddWithValue("@ABOUT_WORK", vacancySearch.AboutWork);
            }

            if (vacancySearch.ExpiryDate.HasValue)
            {
                filterQuery.Append(" AND EXPIRY_DATE = @EXPIRY_DATE");
                sqlCommand.Parameters.AddWithValue("@EXPIRY_DATE", vacancySearch.ExpiryDate);
            }

            if (vacancySearch.CategoryItemId.HasValue)
            {
                filterQuery.Append(" AND CATEGORY_ITEM_ID = @CATEGORY_ITEM_ID");
                sqlCommand.Parameters.AddWithValue("@CATEGORY_ITEM_ID", vacancySearch.CategoryItemId);
            }

            if (vacancySearch.CityId.HasValue)
            {
                filterQuery.Append(" AND CITY_ID = @CITY_ID");
                sqlCommand.Parameters.AddWithValue("@CITY_ID", vacancySearch.CityId);
            }

            if (vacancySearch.MinSalaryId.HasValue)
            {
                filterQuery.Append(" AND MIN_SALARY_ID = @MIN_SALARY_ID");
                sqlCommand.Parameters.AddWithValue("@MIN_SALARY_ID", vacancySearch.MinSalaryId);
            }

            if (vacancySearch.MaxSalaryId.HasValue)
            {
                filterQuery.Append(" AND MAX_SALARY_ID = @MAX_SALARY_ID");
                sqlCommand.Parameters.AddWithValue("@MAX_SALARY_ID", vacancySearch.MinSalaryId);
            }

            if (vacancySearch.MinAgeId.HasValue)
            {
                filterQuery.Append("AND MIN_AGE_ID = @MIN_AGE_ID");
                sqlCommand.Parameters.AddWithValue("@MIN_AGE_ID", vacancySearch.MinAgeId);
            }

            if (vacancySearch.MaxAgeId.HasValue)
            {
                filterQuery.Append(" AND MAX_AGE_ID = @MAX_AGE_ID");
                sqlCommand.Parameters.AddWithValue("@MAX_AGE_ID", vacancySearch.MaxAgeId);
            }

            if (vacancySearch.EducationId.HasValue)
            {
                filterQuery.Append(" AND EDUCATION_ID = @EDUCATION_ID");
                sqlCommand.Parameters.AddWithValue("@EDUCATION_ID", vacancySearch.EducationId);
            }

            if (vacancySearch.ExperienceId.HasValue)
            {
                filterQuery.Append(" AND EXPERIENCE_ID = @EXPERIENCE_ID");
                sqlCommand.Parameters.AddWithValue("@EXPERIENCE_ID", vacancySearch.ExperienceId);
            }
            #endregion

            StringBuilder query = new StringBuilder(@"
                SELECT 
                    COUNT(*)
                FROM VACANCIES
                WHERE IS_DELETED = 0
            ");

            query.Append(filterQuery);

            sqlCommand.Connection = connection;
            sqlCommand.CommandText = query.ToString();

            int.TryParse(
                sqlCommand.ExecuteScalar().ToString(),
                out int count);
            return count;
        }
        #endregion

        public IEnumerable<SelectItem> GetList()
        {
            throw new NotImplementedException();
        }

        public Vacancy GetById(int id)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    SELECT
                        ID,
                        CATEGORY_ITEM_ID,
                        CITY_ID,
                        MIN_SALARY_ID,
                        MAX_SALARY_ID,
                        MIN_AGE_ID,
                        MAX_AGE_ID,
                        EDUCATION_ID,
                        EXPERIENCE_ID,
                        EMAIL,
                        POSITION,
                        COMPANY_NAME,
                        RELEVANT_PERSON,
                        CONDIDATE_REQUIREMENTS,
                        ABOUT_WORK,
                        EXPIRY_DATE
                    FROM VACANCIES
                    WHERE ID = @ID
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@ID", id);
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                if (dataTable.Rows.Count == 0)
                    return null;
                DataRow dataRow = dataTable.Rows[0];
                Vacancy vacancy = new Vacancy(dataRow);

                vacancy.VacancyPhones = GetVacancyPhones(connection, id);

                return vacancy;
            }
        }

        public bool CheckExistence(Vacancy vacancy)
        {
            throw new NotImplementedException();
        }

        public bool Insert(Vacancy vacancy)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();

                    SqlTransaction transaction = connection.BeginTransaction();

                    string query = @"
                    INSERT INTO VACANCIES(
                        CATEGORY_ITEM_ID,
                        CITY_ID,
                        MIN_SALARY_ID,
                        MAX_SALARY_ID,
                        MIN_AGE_ID,
                        MAX_AGE_ID,
                        EDUCATION_ID,
                        EXPERIENCE_ID,
                        EMAIL,
                        POSITION,
                        COMPANY_NAME,
                        RELEVANT_PERSON,
                        CONDIDATE_REQUIREMENTS,
                        ABOUT_WORK,
                        EXPIRY_DATE,
                        CREATED_BY
                    )
                    VALUES(
                        @CATEGORY_ITEM_ID,
                        @CITY_ID,
                        @MIN_SALARY_ID,
                        @MAX_SALARY_ID,
                        @MIN_AGE_ID,
                        @MAX_AGE_ID,
                        @EDUCATION_ID,
                        @EXPERIENCE_ID,
                        @EMAIL,
                        @POSITION,
                        @COMPANY_NAME,
                        @RELEVANT_PERSON,
                        @CONDIDATE_REQUIREMENTS,
                        @ABOUT_WORK,
                        @EXPIRY_DATE,
                        @CREATED_BY
                    )
                ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@CATEGORY_ITEM_ID", vacancy.CategoryItem?.Id);
                    sqlCommand.Parameters.AddWithValue("@CITY_ID", vacancy.City?.Id);
                    sqlCommand.Parameters.AddWithValue("@MIN_SALARY_ID", vacancy.MinSalary?.Id);
                    sqlCommand.Parameters.AddWithValue("@MAX_SALARY_ID", vacancy.MaxSalary?.Id);
                    sqlCommand.Parameters.AddWithValue("@MIN_AGE_ID", vacancy.MinAge?.Id);
                    sqlCommand.Parameters.AddWithValue("@MAX_AGE_ID", vacancy.MaxAge?.Id);
                    sqlCommand.Parameters.AddWithValue("@EDUCATION_ID", vacancy.Education?.Id);
                    sqlCommand.Parameters.AddWithValue("@EXPERIENCE_ID", vacancy.Experience?.Id);
                    sqlCommand.Parameters.AddWithValue("@EMAIL", vacancy.Email);
                    sqlCommand.Parameters.AddWithValue("@POSITION", vacancy.Position);
                    sqlCommand.Parameters.AddWithValue("@COMPANY_NAME", vacancy.CompanyName);
                    sqlCommand.Parameters.AddWithValue("@RELEVANT_PERSON", vacancy.RelevantPerson);
                    sqlCommand.Parameters.AddWithValue("@CONDIDATE_REQUIREMENTS", vacancy.CandidateRequirements);
                    sqlCommand.Parameters.AddWithValue("@ABOUT_WORK", vacancy.AboutWork);
                    sqlCommand.Parameters.AddWithValue("@EXPIRY_DATE", vacancy.ExpiryDate);
                    sqlCommand.Parameters.AddWithValue("@CREATED_BY", vacancy.CreatedBy);
                    int affectedRow = sqlCommand.ExecuteNonQuery();

                    int vacancyId = 0;
                    if (sqlCommand.Parameters["@ID"].Value != DBNull.Value)
                        vacancyId = Convert.ToInt32(sqlCommand.Parameters["@ID"].Value);
                    if (vacancyId > 0)
                    {
                        vacancy.VacancyPhones = vacancy.VacancyPhones.Select(e => new VacancyPhone()
                        {
                            VacancyId = vacancyId,
                            PhoneNumber = e.PhoneNumber
                        });
                        InsertVacancyPhones(
                            vacancy.VacancyPhones,
                            connection,
                            transaction);
                    }
                    transaction.Commit();
                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Update(Vacancy vacancy)
        {
            try
            {
                using (SqlConnection connection = _dbHandler.GetConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();

                    string query = @"
                    UPDATE VACANCIES
                    SET
                        CATEGORY_ITEM_ID = @CATEGORY_ITEM_ID,
	                    CITY_ID = @CITY_ID,
	                    MIN_SALARY_ID = @MIN_SALARY_ID,
	                    MAX_SALARY_ID = @MAX_SALARY_ID,
	                    MIN_AGE_ID = @MIN_AGE_ID,
	                    MAX_AGE_ID = @MAX_AGE_ID,
	                    EDUCATION_ID = @EDUCATION_ID,
	                    EXPERIENCE_ID = @EXPERIENCE_ID,
                        EMAIL = @EMAIL,
	                    POSITION = @POSITION,
	                    COMPANY_NAME = @COMPANY_NAME,
	                    RELEVANT_PERSON = @RELEVANT_PERSON,
                        CONDIDATE_REQUIREMENTS = @CONDIDATE_REQUIREMENTS,
                        ABOUT_WORK = @ABOUT_WORK,
	                    EXPIRY_DATE = @EXPIRY_DATE,
	                    UPDATED_BY = @UPDATED_BY,
                        UPDATED_DATE = @UPDATED_DATE
                    WHERE ID = @ID
                ";
                    SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
                    sqlCommand.Parameters.AddWithValue("@CATEGORY_ITEM_ID", vacancy.CategoryItem?.Id);
                    sqlCommand.Parameters.AddWithValue("@CITY_ID", vacancy.City?.Id);
                    sqlCommand.Parameters.AddWithValue("@MIN_SALARY_ID", vacancy.MinSalary?.Id);
                    sqlCommand.Parameters.AddWithValue("@MAX_SALARY_ID", vacancy.MaxSalary?.Id);
                    sqlCommand.Parameters.AddWithValue("@MIN_AGE_ID", vacancy.MinAge?.Id);
                    sqlCommand.Parameters.AddWithValue("@MAX_AGE_ID", vacancy.MaxAge?.Id);
                    sqlCommand.Parameters.AddWithValue("@EDUCATION_ID", vacancy.Education?.Id);
                    sqlCommand.Parameters.AddWithValue("@EXPERIENCE_ID", vacancy.Experience?.Id);
                    sqlCommand.Parameters.AddWithValue("@EMAIL", vacancy.Email);
                    sqlCommand.Parameters.AddWithValue("@POSITION", vacancy.Position);
                    sqlCommand.Parameters.AddWithValue("@COMPANY_NAME", vacancy.CompanyName);
                    sqlCommand.Parameters.AddWithValue("@RELEVANT_PERSON", vacancy.RelevantPerson);
                    sqlCommand.Parameters.AddWithValue("@CONDIDATE_REQUIREMENTS", vacancy.CandidateRequirements);
                    sqlCommand.Parameters.AddWithValue("@ABOUT_WORK", vacancy.AboutWork);
                    sqlCommand.Parameters.AddWithValue("@EXPIRY_DATE", vacancy.ExpiryDate);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_BY", vacancy.UpdatedBy);
                    sqlCommand.Parameters.AddWithValue("@UPDATED_DATE", vacancy.UpdatedDate);
                    sqlCommand.Parameters.AddWithValue("@ID", vacancy.Id);
                    int affectedRow = sqlCommand.ExecuteNonQuery();

                    UpdateVacancyPhones(
                        vacancy.VacancyPhones,
                        connection,
                        transaction);

                    transaction.Commit();

                    return affectedRow == 1;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Delete(DeleteEntity entity)
        {
            using (SqlConnection connection = _dbHandler.GetConnection())
            {
                connection.Open();
                string query = @"
                    UPDATE VACANCIES
                    SET 
                        DELETED_BY = @DELETED_BY,
                        DELETED_DATE = @DELETED_DATE,
                        IS_DELETED = 1
                    WHERE ID = @ID AND IS_DELETED = 0
                ";
                SqlCommand sqlCommand = new SqlCommand(query, connection);
                sqlCommand.Parameters.AddWithValue("@DELETED_BY", entity.DeletedBy);
                sqlCommand.Parameters.AddWithValue("@DELETED_DATE", entity.DeletedDate);
                sqlCommand.Parameters.AddWithValue("@ID", entity.Id);
                int affectedRow = sqlCommand.ExecuteNonQuery();
                return affectedRow == 1;
            }
        }

        #endregion

        #region VacancyPhones

        private IEnumerable<VacancyPhone> GetVacancyPhones(SqlConnection connection, int vacancyId)
        {
            string query = @"
                SELECT 
                    VACANCY_ID,
                    PHONE_NUMBER
                FROM VACANCY_PHONES
                WHERE VACANCY_ID = @VACANCY_ID
            ";

            SqlCommand sqlCommand = new SqlCommand(query, connection);
            sqlCommand.Parameters.AddWithValue("@VACANCY_ID", vacancyId);

            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            DataRowCollection dataRowCollection = dataTable.Rows;

            IList<VacancyPhone> vacancyPhones = new List<VacancyPhone>();

            foreach (DataRow dataRow in dataRowCollection)
            {
                VacancyPhone vacancyPhone = new VacancyPhone(dataRow);
                vacancyPhones.Add(vacancyPhone);
            }
            return vacancyPhones;
        }

        private void InsertVacancyPhones(
            IEnumerable<VacancyPhone> vacancyPhones,
            SqlConnection connection,
            SqlTransaction transaction)
        {
            DataTable destinationTable = new DataTable();
            destinationTable.Columns.Add("VACANCY_ID", typeof(int));
            destinationTable.Columns.Add("PHONE_NUMBER", typeof(string));

            foreach (VacancyPhone vacancyPhone in vacancyPhones)
            {
                DataRow dataRow = destinationTable.NewRow();
                dataRow["VACANCY_ID"] = vacancyPhone.VacancyId;
                dataRow["PHONE_NUMBER"] = vacancyPhone.PhoneNumber;
                destinationTable.Rows.Add(dataRow);
            }

            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(
               connection,
               SqlBulkCopyOptions.KeepIdentity,
               transaction))
            {
                sqlBulkCopy.BatchSize = vacancyPhones.Count();
                sqlBulkCopy.DestinationTableName = "VACANCY_PHONES";
                sqlBulkCopy.WriteToServer(destinationTable);
            }
        }

        private void UpdateVacancyPhones(
            IEnumerable<VacancyPhone> vacancyPhones,
            SqlConnection connection,
            SqlTransaction transaction)
        {
            //delete old unused rolePermissions
            string query = @"
                DELETE VACANCY_PHONES
                WHERE VACANCY_ID = @VACANCY_ID
            ";

            SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
            sqlCommand.Parameters.AddWithValue("@VACANCY_ID", vacancyPhones.FirstOrDefault().VacancyId);
            sqlCommand.ExecuteNonQuery();

            //insert or update new rolePermissions
            DataTable destinationTable = new DataTable();
            destinationTable.Columns.Add("VACANCY_ID", typeof(int));
            destinationTable.Columns.Add("PHONE_NUMBER", typeof(string));

            foreach (VacancyPhone vacancyPhone in vacancyPhones)
            {
                DataRow dataRow = destinationTable.NewRow();
                dataRow["VACANCY_ID"] = vacancyPhone.VacancyId;
                dataRow["PHONE_NUMBER"] = vacancyPhone.PhoneNumber;
                destinationTable.Rows.Add(dataRow);
            }

            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(
               connection,
               SqlBulkCopyOptions.KeepIdentity,
               transaction))
            {
                sqlBulkCopy.BatchSize = vacancyPhones.Count();
                sqlBulkCopy.DestinationTableName = "VACANCY_PHONES";
                sqlBulkCopy.WriteToServer(destinationTable);
            }
        }

        private int DeleteVacancyPhones(
            SqlConnection connection,
            SqlTransaction transaction,
            int vacancyId)
        {
            string query = @"
                DELETE VACANCY_PHONES
                WHERE VACANCY_ID = @VACANCY_ID
            ";
            SqlCommand sqlCommand = new SqlCommand(query, connection, transaction);
            sqlCommand.Parameters.AddWithValue("@VACANCY_PHONES", vacancyId);
            int affectedRows = sqlCommand.ExecuteNonQuery();
            return affectedRows;
        }

        #endregion
    }
}
