﻿using System.Collections.Generic;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Vacancies;

namespace Vacancies.Infrastructure.Repositories.Vacancies
{
    public interface IVacancyRepository
    {
        (IEnumerable<Vacancy>, int) FindList(VacancySearch vacancySearch);
        IEnumerable<SelectItem> GetList();
        Vacancy GetById(int id);
        bool CheckExistence(Vacancy vacancy);
        bool Insert(Vacancy vacancy);
        bool Update(Vacancy vacancy);
        bool Delete(DeleteEntity entity);
    }
}
