﻿namespace Vacancies.Domain.Enums
{
    public enum Gender
    {
        Male, 
        Female
    }
}
