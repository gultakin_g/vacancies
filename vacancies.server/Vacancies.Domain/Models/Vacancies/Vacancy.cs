﻿using System;
using System.Collections.Generic;
using System.Data;
using Vacancies.Domain.Models.Ages;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Categories;
using Vacancies.Domain.Models.Cities;
using Vacancies.Domain.Models.Educations;
using Vacancies.Domain.Models.Experiences;
using Vacancies.Domain.Models.Salaries;

namespace Vacancies.Domain.Models.Vacancies
{
    public class Vacancy : BaseEntity
    {
        public Vacancy()
        {
        }

        public Vacancy(DataRow dataRow)
        {
            if (dataRow.Table.Columns.Contains("CATEGORY_ITEM_ID"))
            {
                CategoryItem = new CategoryItem()
                {
                    Id = Convert.ToInt32(dataRow["CATEGORY_ITEM_ID"])
                };

                if (dataRow.Table.Columns.Contains("CATEGORY_ITEM_NAME"))
                    CategoryItem.Name = dataRow["CATEGORY_ITEM_NAME"].ToString();
            }

            if (dataRow.Table.Columns.Contains("CITY_ID"))
            {
                City = new City()
                {
                    Id = Convert.ToInt32(dataRow["CITY_ID"])
                };

                if (dataRow.Table.Columns.Contains("CITY_NAME"))
                    City.Name = dataRow["CITY_NAME"].ToString();
            }

            if (dataRow.Table.Columns.Contains("MIN_SALARY_ID"))
            {
                MinSalary = new Salary()
                {
                    Id = Convert.ToInt32(dataRow["MIN_SALARY_ID"])
                };

                if (dataRow.Table.Columns.Contains("MIN_SALARY_AMOUNT"))
                    MinSalary.Amount = Convert.ToDouble(dataRow["MIN_SALARY_AMOUNT"]);
            }

            if (dataRow.Table.Columns.Contains("MAX_SALARY_ID"))
            {
                MaxSalary = new Salary()
                {
                    Id = Convert.ToInt32(dataRow["MAX_SALARY_ID"])
                };

                if (dataRow.Table.Columns.Contains("MAX_SALARY_AMOUNT"))
                    MaxSalary.Amount = Convert.ToDouble(dataRow["MIN_SALARY_AMOUNT"]);
            }

            if (dataRow.Table.Columns.Contains("MIN_AGE_ID"))
            {
                MinAge = new Age()
                {
                    Id = Convert.ToInt32(dataRow["MIN_AGE_ID"])
                };

                if (dataRow.Table.Columns.Contains("MIN_AGE_AMOUNT"))
                    MinAge.Amount = Convert.ToInt32(dataRow["MIN_AGE_AMOUNT"]);
            }

            if (dataRow.Table.Columns.Contains("MAX_AGE_ID"))
            {
                MaxAge = new Age()
                {
                    Id = Convert.ToInt32(dataRow["MAX_AGE_ID"])
                };

                if (dataRow.Table.Columns.Contains("MAX_AGE_AMOUNT"))
                    MaxAge.Amount = Convert.ToInt32(dataRow["MAX_AGE_AMOUNT"]);
            }

            if (dataRow.Table.Columns.Contains("EDUCATION_ID"))
            {
                Education = new Education()
                {
                    Id = Convert.ToInt32(dataRow["EDUCATION_ID"])
                };

                if (dataRow.Table.Columns.Contains("EDUCATION_NAME"))
                    Education.Name = dataRow["EDUCATION_NAME"].ToString();
            }

            if (dataRow.Table.Columns.Contains("EXPERIENCE_ID"))
            {
                Experience = new Experience()
                {
                    Id = Convert.ToInt32(dataRow["EXPERIENCE_ID"])
                };

                if (dataRow.Table.Columns.Contains("EXPERIENCE_NAME"))
                    Experience.Name = dataRow["EXPERIENCE_NAME"].ToString();
            }

            Id = Convert.ToInt32(dataRow["ID"]);
            Email = Convert.ToString(dataRow["EMAIL"]);
            Position = Convert.ToString(dataRow["POSITION"]);
            CompanyName = Convert.ToString(dataRow["COMPANY_NAME"]);
            RelevantPerson = Convert.ToString(dataRow["RELEVANT_PERSON"]);
            CandidateRequirements = Convert.ToString(dataRow["CONDIDATE_REQUIREMENTS"]);
            AboutWork = Convert.ToString(dataRow["ABOUT_WORK"]);
            ExpiryDate = Convert.ToDateTime(dataRow["EXPIRY_DATE"]);
        }

        public string Email { get; set; }
        public string Position { get; set; }
        public string CompanyName { get; set; }
        public string RelevantPerson { get; set; }
        public string CandidateRequirements { get; set; }
        public string AboutWork { get; set; }
        public DateTime ExpiryDate { get; set; }

        public CategoryItem CategoryItem { get; set; }
        public City City { get; set; }
        public Salary MinSalary { get; set; }
        public Salary MaxSalary { get; set; }
        public Age MinAge { get; set; }
        public Age MaxAge { get; set; }
        public Education Education { get; set; }
        public Experience Experience { get; set; }

        public IEnumerable<VacancyPhone> VacancyPhones { get; set; }
    }
}
