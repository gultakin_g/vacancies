﻿using System;
using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Vacancies
{
    public class VacancySearch : BaseSearch
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Position { get; set; }
        public string CompanyName { get; set; }
        public string RelevantPerson { get; set; }
        public string CandidateRequirements { get; set; }
        public string AboutWork { get; set; }
        public DateTime? ExpiryDate { get; set; }

        public int? CategoryItemId { get; set; }
        public int? CityId { get; set; }
        public int? MinSalaryId { get; set; }
        public int? MaxSalaryId { get; set; }
        public int? MinAgeId { get; set; }
        public int? MaxAgeId { get; set; }
        public int? EducationId { get; set; }
        public int? ExperienceId { get; set; }
    }
}
