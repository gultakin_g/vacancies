﻿using System;
using System.Data;

namespace Vacancies.Domain.Models.Vacancies
{
    public class VacancyPhone
    {
        public VacancyPhone()
        {

        }

        public VacancyPhone(DataRow dataRow)
        {
            if (dataRow.Table.Columns.Contains("VACANCY_ID"))
                VacancyId = Convert.ToInt32(dataRow["VACANCY_ID"]);

            if (dataRow.Table.Columns.Contains("PHONE_NUMBER"))
                PhoneNumber = Convert.ToString(dataRow["PHONE_NUMBER"]);
        }

        public int VacancyId { get; set; }
        public string PhoneNumber { get; set; }
    }
}
