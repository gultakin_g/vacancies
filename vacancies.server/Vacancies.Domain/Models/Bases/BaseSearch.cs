﻿namespace Vacancies.Domain.Models.Bases
{
    public abstract class BaseSearch
    {
        public int CurrentPage { get; set; }
        public int PerPage { get; set; }
        public string SortBy { get; set; }
        public string SortDirection { get; set; }
    }
}
