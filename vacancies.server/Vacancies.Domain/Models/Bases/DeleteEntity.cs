﻿using System;

namespace Vacancies.Domain.Models.Bases
{
    public class DeleteEntity
    {
        public int Id { get; set; }
        public int DeletedBy { get; set; }
        public DateTime DeletedDate { get; set; }
    }
}
