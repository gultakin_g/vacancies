﻿namespace Vacancies.Domain.Models.Bases
{
    public class SelectItem
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
