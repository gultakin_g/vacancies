﻿using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Educations
{
    public class EducationSearch : BaseSearch
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
