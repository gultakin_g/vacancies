﻿using System;
using System.Data;
using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Educations
{
    public class Education : BaseEntity
    {
        public Education()
        {

        }

        public Education(DataRow dataRow)
        {
            Id = Convert.ToInt32(dataRow["ID"]);
            Name = dataRow["NAME"].ToString();
            Description = dataRow["DESCRIPTION"].ToString();
        }

        public string Name { get; set; }
        public string Description { get; set; }
    }
}
