﻿using System;
using System.Data;
using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Experiences
{
    public class Experience : BaseEntity
    {
        public Experience()
        {

        }
        public Experience(DataRow dataRow)
        {
            Id = Convert.ToInt32(dataRow["ID"]);
            Name = dataRow["NAME"].ToString();
            Description = dataRow["DESCRIPTION"].ToString();
        }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
