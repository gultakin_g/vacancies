﻿using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Experiences
{
    public class ExperienceSearch : BaseSearch
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
