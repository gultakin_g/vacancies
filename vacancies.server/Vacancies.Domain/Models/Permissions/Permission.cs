﻿using System;
using System.Data;
using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Permissions
{
    public class Permission : BaseEntity
    {
        public Permission()
        {
        }
        public Permission(DataRow dataRow)
        {
            Id = Convert.ToInt32(dataRow["ID"]);
            Name = Convert.ToString(dataRow["NAME"]);
            Description = Convert.ToString(dataRow["DESCRIPTION"]);
        }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
