﻿using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Permissions
{
    public class PermissionSearch : BaseSearch
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
