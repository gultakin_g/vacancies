﻿using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Salaries
{
    public class SalarySearch : BaseSearch
    {
        public double? Amount { get; set; }
        public string Description { get; set; }
    }
}
