﻿using System;
using System.Data;
using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Salaries
{
    public class Salary : BaseEntity
    {
        public Salary()
        {

        }

        public Salary(DataRow dataRow)
        {
            Id = Convert.ToInt32(dataRow["ID"]);
            Amount = Convert.ToDouble(dataRow["AMOUNT"]);
            Description = Convert.ToString(dataRow["DESCRIPTION"]);
        }
        public double Amount { get; set; }
        public string Description { get; set; }
    }
}
