﻿using System;
using System.Collections.Generic;
using System.Data;
using Vacancies.Domain.Enums;
using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Users
{
    public class User : BaseEntity
    {
        public User()
        {
        }

        public User(DataRow dataRow)
        {
            Id = Convert.ToInt32(dataRow["ID"]);
            if (dataRow.Table.Columns.Contains("IMAGE_URL"))
            {
                ImageUrl = Convert.ToString(dataRow["IMAGE_URL"]);
            }
            FirstName = Convert.ToString(dataRow["FIRST_NAME"]);
            LastName = Convert.ToString(dataRow["LAST_NAME"]);
            MiddleName = Convert.ToString(dataRow["MIDDLE_NAME"]);
            Gender = (Gender)Enum.Parse(typeof(Gender), dataRow["GENDER"]?.ToString());
            BirthDate = Convert.ToDateTime(dataRow["BIRTH_DATE"]);
            Email = Convert.ToString(dataRow["EMAIL"]);
            PhoneNumber = Convert.ToString(dataRow["PHONE_NUMBER"]);
            if (dataRow.Table.Columns.Contains("USER_NAME"))
            {
                UserName = Convert.ToString(dataRow["USER_NAME"]);
            }

            if (dataRow.Table.Columns.Contains("PASSWORD"))
            {
                Password = Convert.ToString(dataRow["PASSWORD"]);
            }
        }

        public string ImageUrl { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public Gender Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public IEnumerable<UserRole> UserRoles { get; set; }
    }
}
