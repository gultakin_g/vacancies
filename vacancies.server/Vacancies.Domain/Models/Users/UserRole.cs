﻿using System;
using System.Data;

namespace Vacancies.Domain.Models.Users
{
    public class UserRole
    {
        public UserRole()
        {
        }

        public UserRole(DataRow dataRow)
        {
            if (dataRow.Table.Columns.Contains("USER_ID"))
                RoleId = Convert.ToInt32(dataRow["USER_ID"]);

            if (dataRow.Table.Columns.Contains("ROLE_ID"))
                RoleId = Convert.ToInt32(dataRow["ROLE_ID"]);
        }
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
