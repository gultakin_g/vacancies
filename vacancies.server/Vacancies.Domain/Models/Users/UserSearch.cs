﻿using System;
using Vacancies.Domain.Enums;
using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Users
{
    public class UserSearch : BaseSearch
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public Gender? Gender { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
    }
}
