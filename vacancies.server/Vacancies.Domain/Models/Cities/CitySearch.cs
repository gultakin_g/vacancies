﻿using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Cities
{
    public class CitySearch : BaseSearch
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
