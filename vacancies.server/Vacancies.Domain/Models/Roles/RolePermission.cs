﻿using System;
using System.Data;

namespace Vacancies.Domain.Models.Roles
{
    public class RolePermission
    {
        public RolePermission()
        {
        }

        public RolePermission(DataRow dataRow)
        {
            if (dataRow.Table.Columns.Contains("ROLE_ID"))
                RoleId = Convert.ToInt32(dataRow["ROLE_ID"]);

            if (dataRow.Table.Columns.Contains("PERMISSION_ID"))
                PermissionId = Convert.ToInt32(dataRow["PERMISSION_ID"]);
        }

        public int RoleId { get; set; }
        public int PermissionId { get; set; }
    }
}
