﻿using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Roles
{
    public class RoleSearch : BaseSearch
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
