﻿using System;
using System.Collections.Generic;
using System.Data;
using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Roles
{
    public class Role : BaseEntity
    {
        public Role()
        {
        }

        public Role(DataRow dataRow)
        {
            Id = Convert.ToInt32(dataRow["ID"]);
            Name = dataRow["NAME"].ToString();
            Description = dataRow["DESCRIPTION"].ToString();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<RolePermission> RolePermissions { get; set; }
    }
}
