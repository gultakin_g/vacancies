﻿using System;
using System.Data;

namespace Vacancies.Domain.Models.Categories
{
    public class CategoryItem
    {
        public CategoryItem()
        {

        }

        public CategoryItem(DataRow dataRow)
        {
            if (dataRow.Table.Columns.Contains("ID"))
                Id = Convert.ToInt32(dataRow["ID"]);

            if (dataRow.Table.Columns.Contains("CATEGORY_ID"))
                CategoryId = Convert.ToInt32(dataRow["CATEGORY_ID"]);

            if (dataRow.Table.Columns.Contains("NAME"))
                Name = Convert.ToString(dataRow["NAME"]);

            if (dataRow.Table.Columns.Contains("DESCRIPTION"))
                Description = Convert.ToString(dataRow["DESCRIPTION"]);
        }
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
