﻿using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Categories
{
    public class CategorySearch : BaseSearch
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
