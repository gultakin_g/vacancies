﻿using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Ages
{
    public class AgeSearch : BaseSearch
    {
        public int? Amount { get; set; }
        public string Description { get; set; }
    }
}
