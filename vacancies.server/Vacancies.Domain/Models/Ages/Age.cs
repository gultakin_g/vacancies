﻿using System;
using System.Data;
using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Ages
{
    public class Age : BaseEntity
    {
        public Age()
        {
        }

        public Age(DataRow dataRow)
        {
            Id = Convert.ToInt32(dataRow["ID"]);
            Amount = Convert.ToInt32(dataRow["AMOUNT"]);
            Description = dataRow["DESCRIPTION"].ToString();
        }

        public int Amount { get; set; }
        public string Description { get; set; }
    }
}
