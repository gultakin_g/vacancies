﻿using Vacancies.Domain.Enums;
using Vacancies.Domain.Models.Bases;

namespace Vacancies.Domain.Models.Resumes
{
    public class ResumeSearch : BaseSearch
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public Gender? Gender { get; set; }
        public string AboutExperience { get; set; }
        public string AboutEducation { get; set; }
        public string MoreInformation { get; set; }
        public string Position { get; set; }
        public string Skills { get; set; }
        public string Email { get; set; }

        public int? AgeId { get; set; }
        public int? EducationId { get; set; }
        public int? ExperienceId { get; set; }
        public int? CategoryItemId { get; set; }
        public int? CityId { get; set; }
        public int? SalaryId { get; set; }
    }
}
