﻿using System;
using System.Data;

namespace Vacancies.Domain.Models.Resumes
{
    public class ResumePhone
    {
        public ResumePhone()
        {
        }

        public ResumePhone(DataRow dataRow)
        {
            if (dataRow.Table.Columns.Contains("RESUME_ID"))
                ResumeId = Convert.ToInt32(dataRow["RESUME_ID"]);

            if (dataRow.Table.Columns.Contains("PHONE_NUMBER"))
                PhoneNumber = Convert.ToString(dataRow["PHONE_NUMBER"]);
        }

        public int ResumeId { get; set; }
        public string PhoneNumber { get; set; }
    }
}
