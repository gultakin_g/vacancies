﻿using System;
using System.Collections.Generic;
using System.Data;
using Vacancies.Domain.Enums;
using Vacancies.Domain.Models.Ages;
using Vacancies.Domain.Models.Bases;
using Vacancies.Domain.Models.Categories;
using Vacancies.Domain.Models.Cities;
using Vacancies.Domain.Models.Educations;
using Vacancies.Domain.Models.Experiences;
using Vacancies.Domain.Models.Salaries;

namespace Vacancies.Domain.Models.Resumes
{
    public class Resume : BaseEntity
    {
        public Resume()
        {
        }

        public Resume(DataRow dataRow)
        {
            if (dataRow.Table.Columns.Contains("CATEGORY_ITEM_ID"))
            {
                CategoryItem = new CategoryItem()
                {
                    Id = Convert.ToInt32(dataRow["CATEGORY_ITEM_ID"])
                };

                if (dataRow.Table.Columns.Contains("CATEGORY_ITEM_NAME"))
                    CategoryItem.Name = dataRow["CATEGORY_ITEM_NAME"].ToString();
            }

            if (dataRow.Table.Columns.Contains("CITY_ID"))
            {
                City = new City()
                {
                    Id = Convert.ToInt32(dataRow["CITY_ID"])
                };

                if (dataRow.Table.Columns.Contains("CITY_NAME"))
                    City.Name = dataRow["CITY_NAME"].ToString();
            }

            if (dataRow.Table.Columns.Contains("SALARY_ID"))
            {
                Salary = new Salary()
                {
                    Id = Convert.ToInt32(dataRow["SALARY_ID"])
                };

                if (dataRow.Table.Columns.Contains("SALARY_AMOUNT"))
                    Salary.Amount = Convert.ToDouble(dataRow["SALARY_AMOUNT"]);
            }

            if (dataRow.Table.Columns.Contains("AGE_ID"))
            {
                Age = new Age()
                {
                    Id = Convert.ToInt32(dataRow["AGE_ID"])
                };

                if (dataRow.Table.Columns.Contains("AGE_AMOUNT"))
                    Age.Amount = Convert.ToInt32(dataRow["AGE_AMOUNT"]);
            }

            if (dataRow.Table.Columns.Contains("EDUCATION_ID"))
            {
                Education = new Education()
                {
                    Id = Convert.ToInt32(dataRow["EDUCATION_ID"])
                };

                if (dataRow.Table.Columns.Contains("EDUCATION_NAME"))
                    Education.Name = dataRow["EDUCATION_NAME"].ToString();
            }

            if (dataRow.Table.Columns.Contains("EXPERIENCE_ID"))
            {
                Experience = new Experience()
                {
                    Id = Convert.ToInt32(dataRow["EXPERIENCE_ID"])
                };

                if (dataRow.Table.Columns.Contains("EXPERIENCE_NAME"))
                    Experience.Name = dataRow["EXPERIENCE_NAME"].ToString();
            }

            Id = Convert.ToInt32(dataRow["ID"]);
            if (dataRow.Table.Columns.Contains("IMAGE_URL"))
            {
                ImageUrl = Convert.ToString(dataRow["IMAGE_URL"]);
            }
            FirstName = Convert.ToString(dataRow["FIRST_NAME"]);
            LastName = Convert.ToString(dataRow["LAST_NAME"]);
            MiddleName = Convert.ToString(dataRow["MIDDLE_NAME"]);
            Gender = (Gender)Enum.Parse(typeof(Gender), dataRow["GENDER"]?.ToString());
            AboutExperience = Convert.ToString(dataRow["ABOUT_EXPERIENCE"]);
            AboutEducation = Convert.ToString(dataRow["ABOUT_EDUCATION"]);
            MoreInformation = Convert.ToString(dataRow["MORE_INFORMATION"]);
            Position = Convert.ToString(dataRow["POSITION"]);
            Skills = Convert.ToString(dataRow["SKILLS"]);
            Email = Convert.ToString(dataRow["EMAIL"]);
            ExpiryDate = Convert.ToDateTime(dataRow["EXPIRY_DATE"]);
        }

        public string ImageUrl { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public Gender Gender { get; set; }
        public string AboutExperience { get; set; }
        public string AboutEducation { get; set; }
        public string MoreInformation { get; set; }
        public string Position { get; set; }
        public string Skills { get; set; }
        public string Email { get; set; }
        public DateTime ExpiryDate { get; set; }

        public Age Age { get; set; }
        public Education Education { get; set; }
        public Experience Experience { get; set; }
        public CategoryItem CategoryItem { get; set; }
        public City City { get; set; }
        public Salary Salary { get; set; }

        public IEnumerable<ResumePhone> ResumePhones { get; set; }
    }
}
