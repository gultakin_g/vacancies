# Vacancies

Vacancies project

Technologies:

 Back-end:
 - ASP.NET Core 3.1 REST API
 - ADO.NET 
 - Swagger

 Front-end:
 - Vue.js / Vuetify
 - Axios

 Database:
 - MS SQL Server