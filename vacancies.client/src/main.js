import Vue from 'vue'
import Vuetify from './vuetify.config';
import App from './App';

Vue.config.productionTip = false;

Vue.use(Vuetify);

new Vue({
  render: h => h(App),
  vuetify: Vuetify
}).$mount('#app')
