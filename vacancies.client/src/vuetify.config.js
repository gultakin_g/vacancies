import Vue from 'vue';
import Vuetify, {
    VApp,
    VAppBar,
    VAppBarNavIcon,
    VAvatar,
    VBtn,
    VCol,
    VContainer,
    VCardActions,
    VCard,
    VCardTitle,
    VCardSubtitle,
    VCardText,
    VContent,
    VDialog,
    VFooter,
    VIcon,
    VImg,
    VList,
    VListGroup,
    VListItem,
    VListItemAction,
    VListItemContent,
    VListItemTitle,
    VNavigationDrawer,
    VRow,
    VSpacer,
    VTextField,
    VToolbarTitle,
    VTooltip
} from 'vuetify/lib';
import {Ripple} from 'vuetify/lib/directives';

Vue.use(Vuetify, {
    components:{
        VApp,
        VAppBar,
        VAppBarNavIcon,
        VAvatar,
        VBtn,
        VCol,
        VContainer,
        VCardActions,
        VCard,
        VCardTitle,
        VCardSubtitle,
        VCardText,
        VContent,
        VDialog,
        VFooter,
        VIcon,
        VImg,
        VList,
        VListGroup,
        VListItem,
        VListItemAction,
        VListItemContent,
        VListItemTitle,
        VNavigationDrawer,
        VRow,
        VSpacer,
        VTextField,
        VToolbarTitle,
        VTooltip
    },
    directives:{
        Ripple
    }
});

const opts = {}

export default new Vuetify(opts);